# zig-brains

A plugin for JetBrains IDEs that adds [Zig](https://ziglang.org) language support. This repository is a successor to the currently-dead project [intellij-zig](https://github.com/ice1000/intellij-zig).

Like its predecessor, this project is also still in the alpha stage; some features are incomplete and there are more planned.

Features:

 - Syntax Highlighting with customizable color settings
 - Limited syntax parsing (e.g. parser does not verify identifiers are valid)
 - Builtin translation from C to Zig using `zig translate-c` (<kbd>Tools > Zig > Translate C File</kbd> or <kbd>Control-Alt-Shift-T</kbd>)
 - Run configuration for simplified access to `zig build` and `zig build run`
 - [Zig Language Server](https://github.com/zigtools/zls) integration
 - Project creation (using `zig init-exe` or `zig init-lib`) and settings for Zig and ZLS executable paths
 - Other basic IDE features like breadcrumbs, spellchecking, folding, commenting, and brace matching
 - Some quick fixes
 - Live templates

 Planned features:

 - Autocompletion
 - More quick fixes, inspections, intention actions, and live templates
 - Optimized PSI structure to make writing annotations, inspections, and other psi-dependent features easier
 - Error recovery in parser