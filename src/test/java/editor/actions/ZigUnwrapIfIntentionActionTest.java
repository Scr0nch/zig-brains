package editor.actions;

import com.intellij.testFramework.fixtures.BasePlatformTestCase;
import testutil.TestUtil;

/**
 * Tests {@link ZigUnwrapIfIntentionAction} in all of the possible situations where it may be used.
 */
public class ZigUnwrapIfIntentionActionTest extends BasePlatformTestCase {

    public void testNonConstantIfStatement() {
        TestUtil.assertNoIntentionAction(myFixture, ZigUnwrapIfIntentionAction.UNWRAP_IF_STATEMENT_TEXT,
                TestUtil.insertIntoComptimeBlock(new String[] {
                        "if (<caret>condition) {",
                        "    // statement 1",
                        "}"
                }));
    }

    public void testNonConstantIfExpression() {
        TestUtil.assertNoIntentionAction(myFixture, ZigUnwrapIfIntentionAction.UNWRAP_IF_EXPRESSION_TEXT,
                TestUtil.insertIntoComptimeBlock(new String[] {
                        "const a = if (<caret>condition) 1;"
                }));
    }

    public void testUnwrapAlwaysTrueIfStatement() {
        testUnwrapAlwaysTrueIfStatement(new String[] {
                "// statement 1",
                "if ( <caret>true ) PP LB",
                "// statement 3"
        }, new String[] {
                "// statement 1",
                "<caret>EX",
                "// statement 3"
        }, false);
    }

    public void testUnwrapAlwaysTrueIfExpression() {
        testUnwrapIfExpression(new String[] {
                "const a = if (<caret>true) 1;"
        }, new String[] {
                "const a = <caret>1;"
        });
    }

    public void testUnwrapAlwaysTrueIfStatementWithElse() {
        testUnwrapAlwaysTrueIfStatement(new String[] {
                "// statement 1",
                "if ( <caret>true ) PP LB else {",
                "    // statement 3",
                "}",
                "// statement 4"
        }, new String[] {
                "// statement 1",
                "<caret>EX",
                "// statement 4"
        }, true);
    }

    public void testUnwrapAlwaysTrueIfExpressionWithElse() {
        testUnwrapIfExpression(new String[] {
                "const a = if (<caret>true) 1 else 0;"
        }, new String[] {
                "const a = <caret>1;"
        });
    }

    public void testUnwrapAlwaysTrueIfStatementWithElseIf() {
        testUnwrapAlwaysTrueIfStatement(new String[] {
                "// statement 1",
                "if ( <caret>true ) PP LB else if (other_condition) {",
                "    // statement 3",
                "}",
                "// statement 4"
        }, new String[] {
                "// statement 1",
                "<caret>EX",
                "// statement 4"
        }, true);
    }

    public void testUnwrapAlwaysTrueIfExpressionWithElseIf() {
        testUnwrapIfExpression(new String[] {
                "const a = if (<caret>true) 1 else if (other_condition) 0;"
        }, new String[] {
                "const a = <caret>1;"
        });
    }

    public void testUnwrapAlwaysTrueIfStatementWithElseIfElse() {
        testUnwrapAlwaysTrueIfStatement(new String[] {
                "// statement 1",
                "if ( <caret>true ) PP LB else if (other_condition) {",
                "    // statement 3",
                "} else {",
                "    // statement 4",
                "}",
                "// statement 5"
        }, new String[] {
                "// statement 1",
                "<caret>EX",
                "// statement 5"
        }, true);
    }

    public void testUnwrapAlwaysTrueIfExpressionWithElseIfElse() {
        testUnwrapIfExpression(new String[] {
                "const a = if (<caret>true) 1 else if (other_condition) 0 else 2;"
        }, new String[] {
                "const a = <caret>1;"
        });
    }

    public void testUnwrapAlwaysTrueIfStatementWithPreviousIf() {
        testUnwrapAlwaysTrueIfStatement(new String[] {
                "// statement 1",
                "if (other_condition) {",
                "    // statement 2",
                "} else if ( <caret>true ) PP LB",
                "// statement 4"
        }, new String[] {
                "// statement 1",
                "if (other_condition) {",
                "    // statement 2",
                "}",
                "<caret>EX",
                "// statement 4"
        }, false);
    }

    public void testUnwrapAlwaysTrueIfExpressionWithPreviousIf() {
        testUnwrapIfExpression(new String[] {
                "const a = if (expression) 1 else if (<caret>true) 0;"
        }, new String[] {
                "const a = if (expression) 1 else <caret>0;"
        });
    }

    public void testUnwrapAlwaysTrueIfStatementWithElseAndPreviousIf() {
        testUnwrapAlwaysTrueIfStatement(new String[] {
                "// statement 1",
                "if (other_condition) {",
                "    // statement 2",
                "} else if ( <caret>true ) PP LB else {",
                "    // statement 4",
                "}",
                "// statement 5"
        }, new String[] {
                "// statement 1",
                "if (other_condition) {",
                "    // statement 2",
                "}",
                "<caret>EX",
                "// statement 5"
        }, true);
    }

    public void testUnwrapAlwaysTrueIfExpressionWithElseAndPreviousIf() {
        testUnwrapIfExpression(new String[] {
                "const a = if (expression) 1 else if (<caret>true) 0 else 2;"
        }, new String[] {
                "const a = if (expression) 1 else <caret>0;"
        });
    }

    public void testUnwrapAlwaysTrueIfStatementWithElseIfAndPreviousIf() {
        testUnwrapAlwaysTrueIfStatement(new String[] {
                "// statement 1",
                "if (other_condition) {",
                "    // statement 2",
                "} else if ( <caret>true ) PP LB else if (other_condition) {",
                "    // statement 4",
                "}",
                "// statement 5"
        }, new String[] {
                "// statement 1",
                "if (other_condition) {",
                "    // statement 2",
                "}",
                "<caret>EX",
                "// statement 5"
        }, true);
    }

    public void testUnwrapAlwaysTrueIfExpressionWithElseIfAndPreviousIf() {
        testUnwrapIfExpression(new String[] {
                "const a = if (expression) 1 else if (<caret>true) 0 else if (other_condition) 2;"
        }, new String[] {
                "const a = if (expression) 1 else <caret>0;"
        });
    }

    public void testUnwrapAlwaysTrueIfStatementWithElseIfElseAndPreviousIf() {
        testUnwrapAlwaysTrueIfStatement(new String[] {
                "// statement 1",
                "if (other_condition) {",
                "    // statement 2",
                "} else if ( <caret>true ) PP LB else if (other_condition) {",
                "    // statement 4",
                "} else {",
                "    // statement 5",
                "}",
                "// statement 6"
        }, new String[] {
                "// statement 1",
                "if (other_condition) {",
                "    // statement 2",
                "}",
                "<caret>EX",
                "// statement 6"
        }, true);
    }

    public void testUnwrapAlwaysTrueIfExpressionWithElseIfElseAndPreviousIf() {
        testUnwrapIfExpression(new String[] {
                "const a = if (expression) 1 else if (<caret>true) 0 else if (other_condition) 2 else 3;"
        }, new String[] {
                "const a = if (expression) 1 else <caret>0;"
        });
    }

    public void testUnwrapAlwaysTrueIfStatementWithTwoPreviousIf() {
        testUnwrapAlwaysTrueIfStatement(new String[] {
                "// 1",
                "if (expr) {",
                "    // 2",
                "} else if (expr) {",
                "    // 3",
                "} else if (<caret>true) PP LB",
                "// 4"
        }, new String[] {
                "// 1",
                "if (expr) {",
                "    // 2",
                "} else if (expr) {",
                "    // 3",
                "}",
                "<caret>EX",
                "// 4"
        }, false);
    }

    public void testUnwrapAlwaysTrueIfExpressionWithTwoPreviousIf() {
        testUnwrapIfExpression(new String[] {
                "const a = if (expr) 1 else if (expr) 0 else if (<caret>true) 2;"
        }, new String[] {
                "const a = if (expr) 1 else if (expr) 0 else <caret>2;"
        });
    }

    public void testUnwrapAlwaysFalseIfStatement() {
        testUnwrapAlwaysFalseIfStatement(new String[] {
                "// statement 1",
                "if ( <caret>false ) PP LB",
                "// statement 3"
        }, new String[] {
                "// statement 1",
                "<caret>// statement 3",
        }, false);
    }

    public void testUnwrapAlwaysFalseIfExpression() {
        TestUtil.assertNoIntentionAction(myFixture, ZigUnwrapIfIntentionAction.UNWRAP_IF_EXPRESSION_TEXT,
                TestUtil.insertIntoComptimeBlock(new String[] {
                        "const a = if(<caret>false) 1;"
                }));
    }

    public void testUnwrapAlwaysFalseIfStatementWithElse() {
        testUnwrapAlwaysFalseIfStatement(new String[] {
                "// statement 1",
                "if ( <caret>false ) PP LB else {",
                "    // statement 3",
                "}",
                "// statement 4"
        }, new String[] {
                "// statement 1",
                "<caret>// statement 3",
                "// statement 4"
        }, true);
    }

    public void testUnwrapAlwaysFalseIfExpressionWithElse() {
        testUnwrapIfExpression(new String[] {
                "const a = if(<caret>false) 1 else 0;"
        }, new String[] {
                "const a = <caret>0;"
        });
    }

    public void testUnwrapAlwaysFalseIfStatementWithElseIf() {
        testUnwrapAlwaysFalseIfStatement(new String[] {
                "// statement 1",
                "if ( <caret>false ) PP LB else if (other_condition) {",
                "    // statement 3",
                "}",
                "// statement 4"
        }, new String[] {
                "// statement 1",
                "<caret>if (other_condition) {",
                "    // statement 3",
                "}",
                "// statement 4"
        }, true);
    }

    public void testUnwrapAlwaysFalseIfExpressionWithElseIf() {
        testUnwrapIfExpression(new String[] {
                "const a = if(<caret>false) 1 else if(other_condition) 0;"
        }, new String[] {
                "const a = <caret>if(other_condition) 0;"
        });
    }

    public void testUnwrapAlwaysFalseIfStatementWithElseIfElse() {
        testUnwrapAlwaysFalseIfStatement(new String[] {
                "// statement 1",
                "if ( <caret>false ) PP LB else if (other_condition) {",
                "    // statement 3",
                "} else {",
                "    // statement 4",
                "}",
                "// statement 5"
        }, new String[] {
                "// statement 1",
                "<caret>if (other_condition) {",
                "    // statement 3",
                "} else {",
                "    // statement 4",
                "}",
                "// statement 5"
        }, true);
    }

    public void testUnwrapAlwaysFalseIfExpressionWithElseIfElse() {
        testUnwrapIfExpression(new String[] {
                "const a = if(<caret>false) 1 else if(other_condition) 0 else 2;"
        }, new String[] {
                "const a = <caret>if(other_condition) 0 else 2;"
        });
    }

    public void testUnwrapAlwaysFalseIfStatementWithPreviousIf() {
        testUnwrapAlwaysFalseIfStatement(new String[] {
                "// statement 1",
                "if (other_condition) {",
                "    // statement 2",
                "} else if ( <caret>false ) PP LB",
                "// statement 4"
        }, new String[] {
                "// statement 1",
                "if (other_condition) {",
                "    // statement 2",
                "}<caret>",
                "// statement 4",
        }, false);
    }

    public void testUnwrapAlwaysFalseIfExpressionWithPreviousIf() {
        testUnwrapIfExpression(new String[] {
                "const a = if (condition) 0 else if(<caret>false) 1;"
        }, new String[] {
                "const a = if (condition) 0<caret>;"
        });
    }

    public void testUnwrapAlwaysFalseIfStatementWithElseAndPreviousIf() {
        testUnwrapAlwaysFalseIfStatement(new String[] {
                "// statement 1",
                "if (other_condition) {",
                "    // statement 2",
                "} else if ( <caret>false ) PP LB else {",
                "    // statement 4",
                "}",
                "// statement 5"
        }, new String[] {
                "// statement 1",
                "if (other_condition) {",
                "    // statement 2",
                "} <caret>else {",
                "    // statement 4",
                "}",
                "// statement 5"
        }, true);
    }

    public void testUnwrapAlwaysFalseIfExpressionWithElseAndPreviousIf() {
        testUnwrapIfExpression(new String[] {
                "const a = if (condition) 0 else if(<caret>false) 1 else 2;"
        }, new String[] {
                "const a = if (condition) 0 else <caret>2;"
        });
    }

    public void testUnwrapAlwaysFalseIfStatementWithElseIfAndPreviousIf() {
        testUnwrapAlwaysFalseIfStatement(new String[] {
                "// statement 1",
                "if (other_condition) {",
                "    // statement 2",
                "} else if ( <caret>false ) PP LB else if (other_condition) {",
                "    // statement 4",
                "}",
                "// statement 5"
        }, new String[] {
                "// statement 1",
                "if (other_condition) {",
                "    // statement 2",
                "} else <caret>if (other_condition) {",
                "    // statement 4",
                "}",
                "// statement 5"
        }, true);
    }

    public void testUnwrapAlwaysFalseIfExpressionWithElseIfAndPreviousIf() {
        testUnwrapIfExpression(new String[] {
                "const a = if (condition) 0 else if(<caret>false) 1 else if(other_condition) 2;"
        }, new String[] {
                "const a = if (condition) 0 else <caret>if(other_condition) 2;"
        });
    }

    public void testUnwrapAlwaysFalseIfStatementWithElseIfElseAndPreviousIf() {
        testUnwrapAlwaysFalseIfStatement(new String[] {
                "// statement 1",
                "if (other_condition) {",
                "    // statement 2",
                "} else if ( <caret>false ) PP LB else if (other_condition) {",
                "    // statement 4",
                "} else {",
                "    // statement 5",
                "}",
                "// statement 6"
        }, new String[] {
                "// statement 1",
                "if (other_condition) {",
                "    // statement 2",
                "} else <caret>if (other_condition) {",
                "    // statement 4",
                "} else {",
                "    // statement 5",
                "}",
                "// statement 6"
        }, true);
    }

    public void testUnwrapAlwaysFalseIfExpressionWithElseIfElseAndPreviousIf() {
        testUnwrapIfExpression(new String[] {
                "const a = if (condition) 0 else if(<caret>false) 1 else if(other_condition) 2 else 3;"
        }, new String[] {
                "const a = if (condition) 0 else <caret>if(other_condition) 2 else 3;"
        });
    }

    public void testUnwrapAlwaysFalseIfStatementWithTwoPreviousIf() {
        testUnwrapAlwaysFalseIfStatement(new String[] {
                "// 1",
                "if (expr) {",
                "    // 2",
                "} else if (expr) {",
                "    // 3",
                "} else if (<caret>false) PP LB",
                "// 4"
        }, new String[] {
                "// 1",
                "if (expr) {",
                "    // 2",
                "} else if (expr) {",
                "    // 3",
                "}<caret>",
                "// 4"
        }, false);
    }

    public void testUnwrapAlwaysFalseIfExpressionWithTwoPreviousIf() {
        testUnwrapIfExpression(new String[] {
                "const a = if (expr) 1 else if (expr) 0 else if (<caret>false) 2;"
        }, new String[] {
                "const a = if (expr) 1 else if (expr) 0<caret>;"
        });
    }

    /**
     * Fuzzes the given original source code lines, in each case unwraps the always-true if statement, and asserts
     * that the result is the same as given expected result. For the fuzzing, all of the instances of {@code PP} will
     * be replaced with a pointer payload and all the occurrences of {@code LB} will be replaced with a labelable block
     * that may contain statements. All possible combinations of these variables are evaluated.
     * <br>
     * The given source code should not include leading indents nor trailing newlines. The given original source code
     * must include a {@code <caret>} tag to set the cursor location of the mock editor. The unwrapping intention
     * action must be at that location for this test to succeed.
     *
     * @param originalSourceCodeLines the annotated original source code lines
     * @param expectedSourceCodeLines the expected source code resulting from unwrapping the if statement contained in
     *                                the given original source code lines
     * @param hasElseBranch           {@code true} if an else branch exists before the always-false branch, {@code
     *                                false} otherwise
     */
    private void testUnwrapAlwaysTrueIfStatement(final String[] originalSourceCodeLines,
                                                 final String[] expectedSourceCodeLines, final boolean hasElseBranch) {
        final String originalSourceCode = TestUtil.insertIntoComptimeBlock(originalSourceCodeLines);
        final String expectedSourceCode = TestUtil.insertIntoComptimeBlock(expectedSourceCodeLines);

        for (int j = 0; j < 2; ++j) {
            final String pointerText = j == 0 ? "|p|" : "";
            final String modifiedOriginalFinal = originalSourceCode.replace("PP", pointerText);
            for (int k = 0; k < 2; ++k) {
                for (int l = 0; l < 2; ++l) {
                    final String modifiedExpected =
                            expectedSourceCode.replace("EX\n    ", l == 0 ? "expr();\n    " : "");
                    for (int m = 0; m < 2; ++m) {
                        String modifiedOriginal = modifiedOriginalFinal;
                        if (k == 0) {
                            // chose whether to include expressions inside a block
                            final String block = "{" + (l == 0 ? "\n        expr();\n    }" : "}");

                            if (m != 0) {
                                // include label on block expression
                                modifiedOriginal = modifiedOriginal.replace("LB", "b: " + block);
                            } else {
                                modifiedOriginal = modifiedOriginal.replace("LB", block);
                            }
                        } else {
                            // include an expression instead of a block
                            if (m == 1 || l == 1) {
                                // only evaluate this case once
                                continue;
                            }

                            final String expression = "expr()" + (hasElseBranch ? "" : ";");
                            modifiedOriginal = modifiedOriginal.replace("LB", expression);
                        }

                        TestUtil.testIntentionAction(myFixture, modifiedOriginal,
                                ZigUnwrapIfIntentionAction.UNWRAP_IF_STATEMENT_TEXT, modifiedExpected);
                    }
                }
            }
        }
    }

    /**
     * Fuzzes the given original source code lines, in each case unwraps the always-false if statement, and asserts
     * that the result is the same as given expected result. For the fuzzing, all of the instances of {@code PP} will
     * be replaced with a pointer payload and all the occurrences of {@code LB} will be replaced with a labelable block
     * that may contain statements. All possible combinations of these variables are evaluated.
     * <br>
     * The given source code should not include leading indents nor trailing newlines. The given original source code
     * must include a {@code <caret>} tag to set the cursor location of the mock editor. The unwrapping intention
     * action must be at that location for this test to succeed.
     *
     * @param originalSourceCodeLines the annotated original source code lines
     * @param expectedSourceCodeLines the expected source code resulting from unwrapping the if statement contained in
     *                                the given original source code lines
     * @param hasElseBranch           {@code true} if an else branch exists before the always-false branch, {@code
     *                                false} otherwise
     */
    private void testUnwrapAlwaysFalseIfStatement(final String[] originalSourceCodeLines,
                                                  final String[] expectedSourceCodeLines, final boolean hasElseBranch) {
        final String originalSourceCode = TestUtil.insertIntoComptimeBlock(originalSourceCodeLines);
        final String expectedSourceCode = TestUtil.insertIntoComptimeBlock(expectedSourceCodeLines);

        // fuzz the variable portions of if statements: the pointer payload, whether the if statement contains a block
        // or an expression, whether the block has a label, and whether the block contains any statements
        for (int i = 0; i < 2; ++i) {
            final String pointerText = i == 0 ? "|p|" : "";
            final String modifiedOriginalFinal = originalSourceCode.replace("PP", pointerText);
            for (int j = 0; j < 2; ++j) {
                for (int k = 0; k < 2; ++k) {
                    for (int l = 0; l < 2; ++l) {
                        String modifiedOriginal = modifiedOriginalFinal;
                        if (l == 0) {
                            // chose whether to include expressions inside a block
                            final String block = "{" + (k == 0 ? "\n        a();\n    }" : "}");

                            if (j != 0) {
                                // include label on block expression
                                modifiedOriginal = modifiedOriginal.replace("LB", "b: " + block);
                            } else {
                                modifiedOriginal = modifiedOriginal.replace("LB", block);
                            }
                        } else {
                            // include an expression instead of a block
                            if (j == 1 || k == 1) {
                                // only evaluate this case once
                                continue;
                            }

                            final String expression = "a()" + (hasElseBranch ? "" : ";");
                            modifiedOriginal = modifiedOriginal.replace("LB", expression);
                        }

                        TestUtil.testIntentionAction(myFixture, modifiedOriginal,
                                ZigUnwrapIfIntentionAction.UNWRAP_IF_STATEMENT_TEXT, expectedSourceCode);
                    }
                }
            }
        }
    }

    /**
     * Asserts that the original source code lines, after applying the unwrap if expression intention action, are
     * equivalent to the given expected source code lines. The given source code should not include leading indents nor
     * trailing newlines. The given original source code must include a {@code <caret>} tag to set the cursor location
     * of the mock editor. The unwrapping intention action must be at that location for this test to succeed.
     *
     * @param originalSourceCodeLines the source code to test
     * @param expectedSourceCodeLines the expected resulting source code after applying the intention action
     */
    private void testUnwrapIfExpression(final String[] originalSourceCodeLines,
                                        final String[] expectedSourceCodeLines) {
        // Expressions are not fuzzed because the pointer payload is the only variable in if expressions and the only
        // case where the associated expression results in a boolean value in when unwrapping a boolean optional
        TestUtil.testIntentionAction(myFixture,
                TestUtil.insertIntoComptimeBlock(originalSourceCodeLines),
                ZigUnwrapIfIntentionAction.UNWRAP_IF_EXPRESSION_TEXT,
                TestUtil.insertIntoComptimeBlock(expectedSourceCodeLines));
    }

}
