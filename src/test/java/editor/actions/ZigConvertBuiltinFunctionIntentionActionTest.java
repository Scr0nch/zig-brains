package editor.actions;

import com.intellij.testFramework.fixtures.BasePlatformTestCase;
import testutil.TestUtil;

public class ZigConvertBuiltinFunctionIntentionActionTest extends BasePlatformTestCase {

    public void testNormalFunctionCall() {
        TestUtil.assertNoIntentionAction(myFixture, "Convert to normal function call",
                TestUtil.insertIntoComptimeBlock(new String[] { "function<caret>Name();" }));
    }

    public void testValidBuiltinFunctionCall() {
        TestUtil.assertNoIntentionAction(myFixture, "Convert to normal function call",
                TestUtil.insertIntoComptimeBlock(new String[] { "@compile<caret>Error(\"\");" }));
    }

    public void testInvalidBuiltinFunctionCall() {
        TestUtil.testIntentionAction(myFixture,
                TestUtil.insertIntoComptimeBlock(new String[] { "@invalid<caret>Builtin();" }),
                "Convert to normal function call",
                TestUtil.insertIntoComptimeBlock(new String[] { "invalid<caret>Builtin();" }));
        TestUtil.testIntentionAction(myFixture,
                TestUtil.insertIntoComptimeBlock(new String[] { "@invalid<caret>Builtin(argument);" }),
                "Convert to normal function call",
                TestUtil.insertIntoComptimeBlock(new String[] { "invalid<caret>Builtin(argument);" }));
    }

}
