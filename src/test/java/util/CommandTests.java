package util;

import java.io.IOException;
import org.hamcrest.*;
import org.junit.*;
import org.junit.rules.ExpectedException;

public class CommandTests {

    private static final String NONEXISTENT_EXECUTABLE_PATH = "./there/is/no/way/this/executable/exists";

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testSuccessfulCommand() throws IOException, InterruptedException {
        final String string = "Hello world!";
        // assume that the echo command is builtin or on PATH
        final Command.Output output = Command.execute(new String[] { "echo", string }, 1000);
        // the command should succeed within the time limit with a standard output matching the string input and no
        // output to standard error
        Assert.assertNotNull(output);
        Assert.assertEquals(string, output.standardOutput.stripTrailing());
        Assert.assertEquals("", output.standardError);
    }

    @Test
    public void testUnsuccessfulCommand() throws IOException, InterruptedException {
        // assume cat is on PATH
        final Command.Output output = Command.execute(new String[] { "cat", NONEXISTENT_EXECUTABLE_PATH }, 1000);
        // the command should fail within the time limit with an empty standard output and an error describing the
        // failure
        Assert.assertNotNull(output);
        Assert.assertEquals("", output.standardOutput);
        Assert.assertEquals("cat: ./there/is/no/way/this/executable/exists: No such file or directory",
                output.standardError.stripTrailing());
    }

    @Test
    public void testNonexistentCommand() throws IOException, InterruptedException {
        // expect an IOException to be thrown because the executable that will be run will not exist
        expectedException.expect(IOException.class);
        expectedException.expectMessage("Cannot run program \"" + NONEXISTENT_EXECUTABLE_PATH + "\"");
        expectedException.expectCause(new BaseMatcher<>() {

            @Override
            public boolean matches(final Object o) {
                return o instanceof IOException && "error=2, No such file or directory".equals(((IOException) o).getMessage());
            }

            @Override
            public void describeTo(final Description description) {
                // FIXME what is this supposed to do?
                description.appendText("Hello world!");
            }

        });

        // IOException will be thrown as a result to this function call
        Command.execute(new String[] { NONEXISTENT_EXECUTABLE_PATH }, 1000);
    }

    @Test
    public void testTimeout() throws IOException, InterruptedException {
        // expect the command to time out before it is able to finish executing
        // use ls recursively from the root directory as a command that should take a while; note that because of the
        // delay between process creation and execution and the actual waitFor() call a simple process may finish
        // executing before it is waited upon
        final Command.Output output = Command.execute(new String[] { "ls", "-R", "/" }, -1);
        Assert.assertNull(output);
    }

}
