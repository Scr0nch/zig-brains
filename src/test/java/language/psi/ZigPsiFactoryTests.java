package language.psi;

import com.intellij.psi.PsiElement;
import com.intellij.testFramework.fixtures.BasePlatformTestCase;
import org.junit.Assert;

public class ZigPsiFactoryTests extends BasePlatformTestCase {

    public void testEmptyFile() {
        final ZigFile zigFile = ZigPsiFactory.createFile(getProject(), "");
        Assert.assertNotNull(zigFile);
        Assert.assertEquals(zigFile.getChildren().length, 0);
    }

    public void testInvalidSyntaxFile() {
        final String symbolName = "INVALID_SYNTAX";
        final ZigFile invalidZigFile = ZigPsiFactory.createFile(getProject(), symbolName);
        Assert.assertNotNull(invalidZigFile);
        final PsiElement zigSymbolImpl = invalidZigFile.getFirstChild();
        Assert.assertNotNull(zigSymbolImpl);
        final PsiElement symbolElement = zigSymbolImpl.getFirstChild();
        Assert.assertNotNull(symbolElement);
        Assert.assertTrue(symbolElement.textMatches(symbolName));
    }

}
