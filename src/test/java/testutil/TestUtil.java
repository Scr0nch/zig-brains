package testutil;

import com.intellij.codeInsight.intention.IntentionAction;
import com.intellij.psi.PsiFile;
import com.intellij.testFramework.PsiTestUtil;
import com.intellij.testFramework.fixtures.CodeInsightTestFixture;
import java.util.List;
import org.junit.Assert;

/**
 * A utility class containing functions to test intention actions and Zig code.
 */
public class TestUtil {

    /**
     * Asserts that the given intention action exists at the cursor position defined in the given original source code
     * and that the given original source code is equivalent to the given expected source code after applying the
     * intention action with the given action hint.
     *
     * @param testFixture        the test fixture to use
     * @param originalSourceCode the original source code to test
     * @param actionHint         the hint of the intention action to apply
     * @param expectedSourceCode the expected resulting code after applying the intention action with the given hint
     */
    public static void testIntentionAction(final CodeInsightTestFixture testFixture, final String originalSourceCode,
                                           final String actionHint, final String expectedSourceCode) {
        final PsiFile file = testFixture.configureByText("dummy_original.zig", originalSourceCode);

        final IntentionAction intentionAction = testFixture.findSingleIntention(actionHint);
        testFixture.launchAction(intentionAction);

        PsiTestUtil.checkFileStructure(file);

        testFixture.checkResult(expectedSourceCode);
    }

    /**
     * Asserts that no intention action with the given action hint exists at the cursor position defined inside the
     * given source code.
     *
     * @param testFixture the test fixture to use
     * @param actionText  the hint of the intention action to search for
     * @param sourceCode  the source code to test
     */
    public static void assertNoIntentionAction(final CodeInsightTestFixture testFixture, String actionText,
                                               final String sourceCode) {
        testFixture.configureByText("dummy.zig", sourceCode);

        final List<IntentionAction> intentionActions = testFixture.getAvailableIntentions();
        for (int i = 0, size = intentionActions.size(); i < size; ++i) {
            Assert.assertNotEquals(actionText, intentionActions.get(i).getText());
        }
    }

    /**
     * Inserts the given block-level source code into a comptime block so that it can be analyzed as a valid Zig file.
     * One level of indentation and newlines will be added and should not be included in the given source code.
     *
     * @param sourceCodeLines the lines of the block-level source code to insert
     * @return a string containing the source code of a valid Zig file that includes the given source code
     */
    public static String insertIntoComptimeBlock(final String[] sourceCodeLines) {
        final StringBuilder builder = new StringBuilder("comptime {\n");

        for (int i = 0, length = sourceCodeLines.length; i < length; ++i) {
            builder.append("    ").append(sourceCodeLines[i]).append('\n');
        }

        return builder.append("}").toString();
    }

    private TestUtil() {}

}
