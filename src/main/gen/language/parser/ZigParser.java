// This is a generated file. Not intended for manual editing.
package language.parser;

import com.intellij.lang.PsiBuilder;
import com.intellij.lang.PsiBuilder.Marker;
import static language.psi.ZigTypes.*;
import static com.intellij.lang.parser.GeneratedParserUtilBase.*;
import com.intellij.psi.tree.IElementType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.tree.TokenSet;
import com.intellij.lang.PsiParser;
import com.intellij.lang.LightPsiParser;

@SuppressWarnings({"SimplifiableIfStatement", "UnusedAssignment"})
public class ZigParser implements PsiParser, LightPsiParser {

  public ASTNode parse(IElementType t, PsiBuilder b) {
    parseLight(t, b);
    return b.getTreeBuilt();
  }

  public void parseLight(IElementType t, PsiBuilder b) {
    boolean r;
    b = adapt_builder_(t, b, this, EXTENDS_SETS_);
    Marker m = enter_section_(b, 0, _COLLAPSE_, null);
    r = parse_root_(t, b);
    exit_section_(b, 0, m, t, r, true, TRUE_CONDITION);
  }

  protected boolean parse_root_(IElementType t, PsiBuilder b) {
    return parse_root_(t, b, 0);
  }

  static boolean parse_root_(IElementType t, PsiBuilder b, int l) {
    return zigFile(b, l + 1);
  }

  public static final TokenSet[] EXTENDS_SETS_ = new TokenSet[] {
    create_token_set_(ADDITION_EXPRESSION, ASSEMBLY_EXPRESSION, ASSIGN_EXPRESSION, BITWISE_EXPRESSION,
      BIT_SHIFT_EXPRESSION, BLOCK_EXPRESSION, BOOLEAN_AND_EXPRESSION, BOOLEAN_OR_EXPRESSION,
      BREAK_EXPRESSION, COMPARE_EXPRESSION, COMPTIME_EXPRESSION, CONTINUE_EXPRESSION,
      CURLY_SUFFIX_EXPRESSION, ERROR_UNION_EXPRESSION, EXPRESSION, FOR_EXPRESSION,
      FOR_TYPE_EXPRESSION, GROUPED_EXPRESSION, IF_EXPRESSION, IF_TYPE_EXPRESSION,
      LABELED_BLOCK_EXPRESSION, LABELED_LOOP_EXPRESSION, LABELED_TYPE_EXPRESSION, LOOP_EXPRESSION,
      LOOP_TYPE_EXPRESSION, MULTIPLY_EXPRESSION, NOSUSPEND_EXPRESSION, PREFIX_EXPRESSION,
      PRIMARY_EXPRESSION, PRIMARY_TYPE_EXPRESSION, RESUME_EXPRESSION, RETURN_EXPRESSION,
      SUFFIX_EXPRESSION, SWITCH_EXPRESSION, TRY_EXPRESSION, TYPE_EXPRESSION,
      WHILE_CONTINUE_EXPRESSION, WHILE_EXPRESSION, WHILE_TYPE_EXPRESSION),
  };

  /* ********************************************************** */
  // ifExpression
  //                               | labeledLoopExpression
  static boolean ControlFlowExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ControlFlowExpression")) return false;
    boolean r;
    r = ifExpression(b, l + 1);
    if (!r) r = labeledLoopExpression(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // ADD_SYMBOL
  //                           | MINUS_SYMBOL // subtraction operator
  //                           | CONCATENATE_ARRAY_SYMBOL
  //                           | ADD_OVERFLOW_SYMBOL
  //                           | MINUS_PERCENT_SYMBOL
  static boolean additionOperation(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "additionOperation")) return false;
    boolean r;
    r = consumeToken(b, ADD_SYMBOL);
    if (!r) r = consumeToken(b, MINUS_SYMBOL);
    if (!r) r = consumeToken(b, CONCATENATE_ARRAY_SYMBOL);
    if (!r) r = consumeToken(b, ADD_OVERFLOW_SYMBOL);
    if (!r) r = consumeToken(b, MINUS_PERCENT_SYMBOL);
    return r;
  }

  /* ********************************************************** */
  // LEFT_BRACKET expression sentinelTerminator? RIGHT_BRACKET
  public static boolean arrayTypeStart(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "arrayTypeStart")) return false;
    if (!nextTokenIs(b, LEFT_BRACKET)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, LEFT_BRACKET);
    r = r && expression(b, l + 1, -1);
    r = r && arrayTypeStart_2(b, l + 1);
    r = r && consumeToken(b, RIGHT_BRACKET);
    exit_section_(b, m, ARRAY_TYPE_START, r);
    return r;
  }

  // sentinelTerminator?
  private static boolean arrayTypeStart_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "arrayTypeStart_2")) return false;
    sentinelTerminator(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // COLON_SYMBOL stringList
  public static boolean assemblyClobbers(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyClobbers")) return false;
    if (!nextTokenIs(b, COLON_SYMBOL)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, COLON_SYMBOL);
    r = r && stringList(b, l + 1);
    exit_section_(b, m, ASSEMBLY_CLOBBERS, r);
    return r;
  }

  /* ********************************************************** */
  // COLON_SYMBOL assemblyInputList assemblyClobbers?
  public static boolean assemblyInput(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyInput")) return false;
    if (!nextTokenIs(b, COLON_SYMBOL)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, COLON_SYMBOL);
    r = r && assemblyInputList(b, l + 1);
    r = r && assemblyInput_2(b, l + 1);
    exit_section_(b, m, ASSEMBLY_INPUT, r);
    return r;
  }

  // assemblyClobbers?
  private static boolean assemblyInput_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyInput_2")) return false;
    assemblyClobbers(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // LEFT_BRACKET IDENTIFIER RIGHT_BRACKET stringLiteral LEFT_PARENTHESIS expression RIGHT_PARENTHESIS
  public static boolean assemblyInputItem(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyInputItem")) return false;
    if (!nextTokenIs(b, LEFT_BRACKET)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, LEFT_BRACKET, IDENTIFIER, RIGHT_BRACKET);
    r = r && stringLiteral(b, l + 1);
    r = r && consumeToken(b, LEFT_PARENTHESIS);
    r = r && expression(b, l + 1, -1);
    r = r && consumeToken(b, RIGHT_PARENTHESIS);
    exit_section_(b, m, ASSEMBLY_INPUT_ITEM, r);
    return r;
  }

  /* ********************************************************** */
  // (assemblyInputItem COMMA_SYMBOL)* assemblyInputItem?
  public static boolean assemblyInputList(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyInputList")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, ASSEMBLY_INPUT_LIST, "<assembly input list>");
    r = assemblyInputList_0(b, l + 1);
    r = r && assemblyInputList_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // (assemblyInputItem COMMA_SYMBOL)*
  private static boolean assemblyInputList_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyInputList_0")) return false;
    while (true) {
      int c = current_position_(b);
      if (!assemblyInputList_0_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "assemblyInputList_0", c)) break;
    }
    return true;
  }

  // assemblyInputItem COMMA_SYMBOL
  private static boolean assemblyInputList_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyInputList_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = assemblyInputItem(b, l + 1);
    r = r && consumeToken(b, COMMA_SYMBOL);
    exit_section_(b, m, null, r);
    return r;
  }

  // assemblyInputItem?
  private static boolean assemblyInputList_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyInputList_1")) return false;
    assemblyInputItem(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // COLON_SYMBOL assemblyOutputList assemblyInput?
  public static boolean assemblyOutput(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyOutput")) return false;
    if (!nextTokenIs(b, COLON_SYMBOL)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, COLON_SYMBOL);
    r = r && assemblyOutputList(b, l + 1);
    r = r && assemblyOutput_2(b, l + 1);
    exit_section_(b, m, ASSEMBLY_OUTPUT, r);
    return r;
  }

  // assemblyInput?
  private static boolean assemblyOutput_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyOutput_2")) return false;
    assemblyInput(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // LEFT_BRACKET IDENTIFIER RIGHT_BRACKET stringLiteral LEFT_PARENTHESIS (MINUS_RIGHT_ARROW typeExpression | IDENTIFIER) RIGHT_PARENTHESIS
  public static boolean assemblyOutputItem(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyOutputItem")) return false;
    if (!nextTokenIs(b, LEFT_BRACKET)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, LEFT_BRACKET, IDENTIFIER, RIGHT_BRACKET);
    r = r && stringLiteral(b, l + 1);
    r = r && consumeToken(b, LEFT_PARENTHESIS);
    r = r && assemblyOutputItem_5(b, l + 1);
    r = r && consumeToken(b, RIGHT_PARENTHESIS);
    exit_section_(b, m, ASSEMBLY_OUTPUT_ITEM, r);
    return r;
  }

  // MINUS_RIGHT_ARROW typeExpression | IDENTIFIER
  private static boolean assemblyOutputItem_5(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyOutputItem_5")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = assemblyOutputItem_5_0(b, l + 1);
    if (!r) r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, null, r);
    return r;
  }

  // MINUS_RIGHT_ARROW typeExpression
  private static boolean assemblyOutputItem_5_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyOutputItem_5_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, MINUS_RIGHT_ARROW);
    r = r && typeExpression(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // (assemblyOutputItem COMMA_SYMBOL)* assemblyOutputItem?
  public static boolean assemblyOutputList(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyOutputList")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, ASSEMBLY_OUTPUT_LIST, "<assembly output list>");
    r = assemblyOutputList_0(b, l + 1);
    r = r && assemblyOutputList_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // (assemblyOutputItem COMMA_SYMBOL)*
  private static boolean assemblyOutputList_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyOutputList_0")) return false;
    while (true) {
      int c = current_position_(b);
      if (!assemblyOutputList_0_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "assemblyOutputList_0", c)) break;
    }
    return true;
  }

  // assemblyOutputItem COMMA_SYMBOL
  private static boolean assemblyOutputList_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyOutputList_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = assemblyOutputItem(b, l + 1);
    r = r && consumeToken(b, COMMA_SYMBOL);
    exit_section_(b, m, null, r);
    return r;
  }

  // assemblyOutputItem?
  private static boolean assemblyOutputList_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyOutputList_1")) return false;
    assemblyOutputItem(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // MULTIPLY_ASSIGN_SYMBOL
  //                         | DIVIDE_ASSIGN_SYMBOL
  //                         | MODULUS_ASSIGN_SYMBOL
  //                         | ADD_ASSIGN_SYMBOL
  //                         | SUBTRACT_ASSIGN_SYMBOL
  //                         | LEFT_BIT_SHIFT_ASSIGN_SYMBOL
  //                         | RIGHT_BIT_SHIFT_ASSIGN_SYMBOL
  //                         | BITWISE_AND_ASSIGN_SYMBOL
  //                         | EXPONENTIATE_ASSIGN_SYMBOL
  //                         | BITWISE_OR_ASSIGN_SYMBOL
  //                         | MULTIPLY_OVERFLOW_ASSIGN_SYMBOL
  //                         | ADD_OVERFLOW_ASSIGN_SYMBOL
  //                         | SUBTRACT_OVERFLOW_ASSIGN_SYMBOL
  //                         | ASSIGN_SYMBOL
  static boolean assignOperation(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assignOperation")) return false;
    boolean r;
    r = consumeToken(b, MULTIPLY_ASSIGN_SYMBOL);
    if (!r) r = consumeToken(b, DIVIDE_ASSIGN_SYMBOL);
    if (!r) r = consumeToken(b, MODULUS_ASSIGN_SYMBOL);
    if (!r) r = consumeToken(b, ADD_ASSIGN_SYMBOL);
    if (!r) r = consumeToken(b, SUBTRACT_ASSIGN_SYMBOL);
    if (!r) r = consumeToken(b, LEFT_BIT_SHIFT_ASSIGN_SYMBOL);
    if (!r) r = consumeToken(b, RIGHT_BIT_SHIFT_ASSIGN_SYMBOL);
    if (!r) r = consumeToken(b, BITWISE_AND_ASSIGN_SYMBOL);
    if (!r) r = consumeToken(b, EXPONENTIATE_ASSIGN_SYMBOL);
    if (!r) r = consumeToken(b, BITWISE_OR_ASSIGN_SYMBOL);
    if (!r) r = consumeToken(b, MULTIPLY_OVERFLOW_ASSIGN_SYMBOL);
    if (!r) r = consumeToken(b, ADD_OVERFLOW_ASSIGN_SYMBOL);
    if (!r) r = consumeToken(b, SUBTRACT_OVERFLOW_ASSIGN_SYMBOL);
    if (!r) r = consumeToken(b, ASSIGN_SYMBOL);
    return r;
  }

  /* ********************************************************** */
  // LEFT_BIT_SHIFT_SYMBOL
  //                           | RIGHT_BIT_SHIFT_SYMBOL
  static boolean bitShiftOperation(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "bitShiftOperation")) return false;
    if (!nextTokenIs(b, "", LEFT_BIT_SHIFT_SYMBOL, RIGHT_BIT_SHIFT_SYMBOL)) return false;
    boolean r;
    r = consumeToken(b, LEFT_BIT_SHIFT_SYMBOL);
    if (!r) r = consumeToken(b, RIGHT_BIT_SHIFT_SYMBOL);
    return r;
  }

  /* ********************************************************** */
  // AMPERSAND_SYMBOL // and
  //                          | CARET_SYMBOL // xor
  //                          | PIPE_SYMBOL // or
  //                          | ORELSE_KEYWORD
  //                          | CATCH_KEYWORD payload?
  static boolean bitwiseOperation(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "bitwiseOperation")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, AMPERSAND_SYMBOL);
    if (!r) r = consumeToken(b, CARET_SYMBOL);
    if (!r) r = consumeToken(b, PIPE_SYMBOL);
    if (!r) r = consumeToken(b, ORELSE_KEYWORD);
    if (!r) r = bitwiseOperation_4(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // CATCH_KEYWORD payload?
  private static boolean bitwiseOperation_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "bitwiseOperation_4")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, CATCH_KEYWORD);
    r = r && bitwiseOperation_4_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // payload?
  private static boolean bitwiseOperation_4_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "bitwiseOperation_4_1")) return false;
    payload(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // labeledBlockExpression
  //                          | expression SEMICOLON_SYMBOL
  public static boolean blockExpressionStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "blockExpressionStatement")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, BLOCK_EXPRESSION_STATEMENT, "<block expression statement>");
    r = labeledBlockExpression(b, l + 1);
    if (!r) r = blockExpressionStatement_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // expression SEMICOLON_SYMBOL
  private static boolean blockExpressionStatement_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "blockExpressionStatement_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = expression(b, l + 1, -1);
    r = r && consumeToken(b, SEMICOLON_SYMBOL);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // IDENTIFIER COLON_SYMBOL
  public static boolean blockLabel(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "blockLabel")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, IDENTIFIER, COLON_SYMBOL);
    exit_section_(b, m, BLOCK_LABEL, r);
    return r;
  }

  /* ********************************************************** */
  // COLON_SYMBOL IDENTIFIER
  public static boolean breakLabel(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "breakLabel")) return false;
    if (!nextTokenIs(b, COLON_SYMBOL)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, COLON_SYMBOL, IDENTIFIER);
    exit_section_(b, m, BREAK_LABEL, r);
    return r;
  }

  /* ********************************************************** */
  // ALIGN_KEYWORD LEFT_PARENTHESIS expression RIGHT_PARENTHESIS
  public static boolean byteAlignment(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "byteAlignment")) return false;
    if (!nextTokenIs(b, ALIGN_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, ALIGN_KEYWORD, LEFT_PARENTHESIS);
    r = r && expression(b, l + 1, -1);
    r = r && consumeToken(b, RIGHT_PARENTHESIS);
    exit_section_(b, m, BYTE_ALIGNMENT, r);
    return r;
  }

  /* ********************************************************** */
  // CALLCONV_KEYWORD LEFT_PARENTHESIS expression RIGHT_PARENTHESIS
  public static boolean callingConvectionSection(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "callingConvectionSection")) return false;
    if (!nextTokenIs(b, CALLCONV_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, CALLCONV_KEYWORD, LEFT_PARENTHESIS);
    r = r && expression(b, l + 1, -1);
    r = r && consumeToken(b, RIGHT_PARENTHESIS);
    exit_section_(b, m, CALLING_CONVECTION_SECTION, r);
    return r;
  }

  /* ********************************************************** */
  // EQUAL_SYMBOL
  //                          | NOT_EQUAL_SYMBOL
  //                          | LESS_THAN_SYMBOL
  //                          | GREATER_THAN_SYMBOL
  //                          | LESS_THAN_OR_EQUAL_SYMBOL
  //                          | GREATER_THAN_OR_EQUAL_SYMBOL
  static boolean compareOperation(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "compareOperation")) return false;
    boolean r;
    r = consumeToken(b, EQUAL_SYMBOL);
    if (!r) r = consumeToken(b, NOT_EQUAL_SYMBOL);
    if (!r) r = consumeToken(b, LESS_THAN_SYMBOL);
    if (!r) r = consumeToken(b, GREATER_THAN_SYMBOL);
    if (!r) r = consumeToken(b, LESS_THAN_OR_EQUAL_SYMBOL);
    if (!r) r = consumeToken(b, GREATER_THAN_OR_EQUAL_SYMBOL);
    return r;
  }

  /* ********************************************************** */
  // (EXTERN_KEYWORD | PACKED_KEYWORD)? containerDeclarationType LEFT_BRACE topLevelDeclarations RIGHT_BRACE
  public static boolean containerDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerDeclaration")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, CONTAINER_DECLARATION, "<container declaration>");
    r = containerDeclaration_0(b, l + 1);
    r = r && containerDeclarationType(b, l + 1);
    r = r && consumeToken(b, LEFT_BRACE);
    r = r && topLevelDeclarations(b, l + 1);
    r = r && consumeToken(b, RIGHT_BRACE);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // (EXTERN_KEYWORD | PACKED_KEYWORD)?
  private static boolean containerDeclaration_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerDeclaration_0")) return false;
    containerDeclaration_0_0(b, l + 1);
    return true;
  }

  // EXTERN_KEYWORD | PACKED_KEYWORD
  private static boolean containerDeclaration_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerDeclaration_0_0")) return false;
    boolean r;
    r = consumeToken(b, EXTERN_KEYWORD);
    if (!r) r = consumeToken(b, PACKED_KEYWORD);
    return r;
  }

  /* ********************************************************** */
  // STRUCT_KEYWORD
  //                          | OPAQUE_KEYWORD
  //                          | ENUM_KEYWORD (LEFT_PARENTHESIS expression RIGHT_PARENTHESIS)?
  //                          | UNION_KEYWORD (LEFT_PARENTHESIS (ENUM_KEYWORD (LEFT_PARENTHESIS expression RIGHT_PARENTHESIS)? | expression) RIGHT_PARENTHESIS)?
  public static boolean containerDeclarationType(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerDeclarationType")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, CONTAINER_DECLARATION_TYPE, "<container declaration type>");
    r = consumeToken(b, STRUCT_KEYWORD);
    if (!r) r = consumeToken(b, OPAQUE_KEYWORD);
    if (!r) r = containerDeclarationType_2(b, l + 1);
    if (!r) r = containerDeclarationType_3(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // ENUM_KEYWORD (LEFT_PARENTHESIS expression RIGHT_PARENTHESIS)?
  private static boolean containerDeclarationType_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerDeclarationType_2")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, ENUM_KEYWORD);
    r = r && containerDeclarationType_2_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (LEFT_PARENTHESIS expression RIGHT_PARENTHESIS)?
  private static boolean containerDeclarationType_2_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerDeclarationType_2_1")) return false;
    containerDeclarationType_2_1_0(b, l + 1);
    return true;
  }

  // LEFT_PARENTHESIS expression RIGHT_PARENTHESIS
  private static boolean containerDeclarationType_2_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerDeclarationType_2_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, LEFT_PARENTHESIS);
    r = r && expression(b, l + 1, -1);
    r = r && consumeToken(b, RIGHT_PARENTHESIS);
    exit_section_(b, m, null, r);
    return r;
  }

  // UNION_KEYWORD (LEFT_PARENTHESIS (ENUM_KEYWORD (LEFT_PARENTHESIS expression RIGHT_PARENTHESIS)? | expression) RIGHT_PARENTHESIS)?
  private static boolean containerDeclarationType_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerDeclarationType_3")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, UNION_KEYWORD);
    r = r && containerDeclarationType_3_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (LEFT_PARENTHESIS (ENUM_KEYWORD (LEFT_PARENTHESIS expression RIGHT_PARENTHESIS)? | expression) RIGHT_PARENTHESIS)?
  private static boolean containerDeclarationType_3_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerDeclarationType_3_1")) return false;
    containerDeclarationType_3_1_0(b, l + 1);
    return true;
  }

  // LEFT_PARENTHESIS (ENUM_KEYWORD (LEFT_PARENTHESIS expression RIGHT_PARENTHESIS)? | expression) RIGHT_PARENTHESIS
  private static boolean containerDeclarationType_3_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerDeclarationType_3_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, LEFT_PARENTHESIS);
    r = r && containerDeclarationType_3_1_0_1(b, l + 1);
    r = r && consumeToken(b, RIGHT_PARENTHESIS);
    exit_section_(b, m, null, r);
    return r;
  }

  // ENUM_KEYWORD (LEFT_PARENTHESIS expression RIGHT_PARENTHESIS)? | expression
  private static boolean containerDeclarationType_3_1_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerDeclarationType_3_1_0_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = containerDeclarationType_3_1_0_1_0(b, l + 1);
    if (!r) r = expression(b, l + 1, -1);
    exit_section_(b, m, null, r);
    return r;
  }

  // ENUM_KEYWORD (LEFT_PARENTHESIS expression RIGHT_PARENTHESIS)?
  private static boolean containerDeclarationType_3_1_0_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerDeclarationType_3_1_0_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, ENUM_KEYWORD);
    r = r && containerDeclarationType_3_1_0_1_0_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (LEFT_PARENTHESIS expression RIGHT_PARENTHESIS)?
  private static boolean containerDeclarationType_3_1_0_1_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerDeclarationType_3_1_0_1_0_1")) return false;
    containerDeclarationType_3_1_0_1_0_1_0(b, l + 1);
    return true;
  }

  // LEFT_PARENTHESIS expression RIGHT_PARENTHESIS
  private static boolean containerDeclarationType_3_1_0_1_0_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerDeclarationType_3_1_0_1_0_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, LEFT_PARENTHESIS);
    r = r && expression(b, l + 1, -1);
    r = r && consumeToken(b, RIGHT_PARENTHESIS);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // DOCUMENTATION_COMMENT? COMPTIME_KEYWORD? IDENTIFIER (COLON_SYMBOL (ANYTYPE_KEYWORD | typeExpression) byteAlignment?)? (ASSIGN_SYMBOL expression)?
  public static boolean containerField(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerField")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, CONTAINER_FIELD, "<container field>");
    r = containerField_0(b, l + 1);
    r = r && containerField_1(b, l + 1);
    r = r && consumeToken(b, IDENTIFIER);
    r = r && containerField_3(b, l + 1);
    r = r && containerField_4(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // DOCUMENTATION_COMMENT?
  private static boolean containerField_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerField_0")) return false;
    consumeToken(b, DOCUMENTATION_COMMENT);
    return true;
  }

  // COMPTIME_KEYWORD?
  private static boolean containerField_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerField_1")) return false;
    consumeToken(b, COMPTIME_KEYWORD);
    return true;
  }

  // (COLON_SYMBOL (ANYTYPE_KEYWORD | typeExpression) byteAlignment?)?
  private static boolean containerField_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerField_3")) return false;
    containerField_3_0(b, l + 1);
    return true;
  }

  // COLON_SYMBOL (ANYTYPE_KEYWORD | typeExpression) byteAlignment?
  private static boolean containerField_3_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerField_3_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, COLON_SYMBOL);
    r = r && containerField_3_0_1(b, l + 1);
    r = r && containerField_3_0_2(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // ANYTYPE_KEYWORD | typeExpression
  private static boolean containerField_3_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerField_3_0_1")) return false;
    boolean r;
    r = consumeToken(b, ANYTYPE_KEYWORD);
    if (!r) r = typeExpression(b, l + 1);
    return r;
  }

  // byteAlignment?
  private static boolean containerField_3_0_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerField_3_0_2")) return false;
    byteAlignment(b, l + 1);
    return true;
  }

  // (ASSIGN_SYMBOL expression)?
  private static boolean containerField_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerField_4")) return false;
    containerField_4_0(b, l + 1);
    return true;
  }

  // ASSIGN_SYMBOL expression
  private static boolean containerField_4_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "containerField_4_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, ASSIGN_SYMBOL);
    r = r && expression(b, l + 1, -1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // typeExpression initializerList?
  public static boolean curlySuffixExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "curlySuffixExpression")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, CURLY_SUFFIX_EXPRESSION, "<curly suffix expression>");
    r = typeExpression(b, l + 1);
    r = r && curlySuffixExpression_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // initializerList?
  private static boolean curlySuffixExpression_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "curlySuffixExpression_1")) return false;
    initializerList(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // ERROR_KEYWORD LEFT_BRACE identifierList RIGHT_BRACE
  public static boolean errorSetDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "errorSetDeclaration")) return false;
    if (!nextTokenIs(b, ERROR_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, ERROR_KEYWORD, LEFT_BRACE);
    r = r && identifierList(b, l + 1);
    r = r && consumeToken(b, RIGHT_BRACE);
    exit_section_(b, m, ERROR_SET_DECLARATION, r);
    return r;
  }

  /* ********************************************************** */
  // suffixExpression (EXCLAMATIONMARK_SYMBOL typeExpression)?
  public static boolean errorUnionExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "errorUnionExpression")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, ERROR_UNION_EXPRESSION, "<error union expression>");
    r = suffixExpression(b, l + 1);
    r = r && errorUnionExpression_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // (EXCLAMATIONMARK_SYMBOL typeExpression)?
  private static boolean errorUnionExpression_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "errorUnionExpression_1")) return false;
    errorUnionExpression_1_0(b, l + 1);
    return true;
  }

  // EXCLAMATIONMARK_SYMBOL typeExpression
  private static boolean errorUnionExpression_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "errorUnionExpression_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenFast(b, EXCLAMATIONMARK_SYMBOL);
    r = r && typeExpression(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // (expression COMMA_SYMBOL)* expression?
  public static boolean expressionList(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expressionList")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EXPRESSION_LIST, "<expression list>");
    r = expressionList_0(b, l + 1);
    r = r && expressionList_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // (expression COMMA_SYMBOL)*
  private static boolean expressionList_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expressionList_0")) return false;
    while (true) {
      int c = current_position_(b);
      if (!expressionList_0_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "expressionList_0", c)) break;
    }
    return true;
  }

  // expression COMMA_SYMBOL
  private static boolean expressionList_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expressionList_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = expression(b, l + 1, -1);
    r = r && consumeToken(b, COMMA_SYMBOL);
    exit_section_(b, m, null, r);
    return r;
  }

  // expression?
  private static boolean expressionList_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expressionList_1")) return false;
    expression(b, l + 1, -1);
    return true;
  }

  /* ********************************************************** */
  // DOT_SYMBOL IDENTIFIER ASSIGN_SYMBOL expression
  public static boolean fieldInitializer(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "fieldInitializer")) return false;
    if (!nextTokenIs(b, DOT_SYMBOL)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, DOT_SYMBOL, IDENTIFIER, ASSIGN_SYMBOL);
    r = r && expression(b, l + 1, -1);
    exit_section_(b, m, FIELD_INITIALIZER, r);
    return r;
  }

  /* ********************************************************** */
  // forPrefix expression (ELSE_KEYWORD expression)?
  public static boolean forExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "forExpression")) return false;
    if (!nextTokenIsFast(b, FOR_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = forPrefix(b, l + 1);
    r = r && expression(b, l + 1, -1);
    r = r && forExpression_2(b, l + 1);
    exit_section_(b, m, FOR_EXPRESSION, r);
    return r;
  }

  // (ELSE_KEYWORD expression)?
  private static boolean forExpression_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "forExpression_2")) return false;
    forExpression_2_0(b, l + 1);
    return true;
  }

  // ELSE_KEYWORD expression
  private static boolean forExpression_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "forExpression_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenFast(b, ELSE_KEYWORD);
    r = r && expression(b, l + 1, -1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // FOR_KEYWORD LEFT_PARENTHESIS expression RIGHT_PARENTHESIS pointerIndexPayload
  public static boolean forPrefix(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "forPrefix")) return false;
    if (!nextTokenIs(b, FOR_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, FOR_KEYWORD, LEFT_PARENTHESIS);
    r = r && expression(b, l + 1, -1);
    r = r && consumeToken(b, RIGHT_PARENTHESIS);
    r = r && pointerIndexPayload(b, l + 1);
    exit_section_(b, m, FOR_PREFIX, r);
    return r;
  }

  /* ********************************************************** */
  // forPrefix labeledBlockExpression (ELSE_KEYWORD statement)?
  //              | forPrefix expression (SEMICOLON_SYMBOL | ELSE_KEYWORD statement)
  public static boolean forStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "forStatement")) return false;
    if (!nextTokenIs(b, FOR_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = forStatement_0(b, l + 1);
    if (!r) r = forStatement_1(b, l + 1);
    exit_section_(b, m, FOR_STATEMENT, r);
    return r;
  }

  // forPrefix labeledBlockExpression (ELSE_KEYWORD statement)?
  private static boolean forStatement_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "forStatement_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = forPrefix(b, l + 1);
    r = r && labeledBlockExpression(b, l + 1);
    r = r && forStatement_0_2(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (ELSE_KEYWORD statement)?
  private static boolean forStatement_0_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "forStatement_0_2")) return false;
    forStatement_0_2_0(b, l + 1);
    return true;
  }

  // ELSE_KEYWORD statement
  private static boolean forStatement_0_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "forStatement_0_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, ELSE_KEYWORD);
    r = r && statement(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // forPrefix expression (SEMICOLON_SYMBOL | ELSE_KEYWORD statement)
  private static boolean forStatement_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "forStatement_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = forPrefix(b, l + 1);
    r = r && expression(b, l + 1, -1);
    r = r && forStatement_1_2(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // SEMICOLON_SYMBOL | ELSE_KEYWORD statement
  private static boolean forStatement_1_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "forStatement_1_2")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, SEMICOLON_SYMBOL);
    if (!r) r = forStatement_1_2_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // ELSE_KEYWORD statement
  private static boolean forStatement_1_2_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "forStatement_1_2_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, ELSE_KEYWORD);
    r = r && statement(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // forPrefix typeExpression (ELSE_KEYWORD typeExpression)?
  public static boolean forTypeExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "forTypeExpression")) return false;
    if (!nextTokenIsFast(b, FOR_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = forPrefix(b, l + 1);
    r = r && typeExpression(b, l + 1);
    r = r && forTypeExpression_2(b, l + 1);
    exit_section_(b, m, FOR_TYPE_EXPRESSION, r);
    return r;
  }

  // (ELSE_KEYWORD typeExpression)?
  private static boolean forTypeExpression_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "forTypeExpression_2")) return false;
    forTypeExpression_2_0(b, l + 1);
    return true;
  }

  // ELSE_KEYWORD typeExpression
  private static boolean forTypeExpression_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "forTypeExpression_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenFast(b, ELSE_KEYWORD);
    r = r && typeExpression(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // LEFT_PARENTHESIS expressionList RIGHT_PARENTHESIS
  public static boolean functionCallArguments(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionCallArguments")) return false;
    if (!nextTokenIs(b, LEFT_PARENTHESIS)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, LEFT_PARENTHESIS);
    r = r && expressionList(b, l + 1);
    r = r && consumeToken(b, RIGHT_PARENTHESIS);
    exit_section_(b, m, FUNCTION_CALL_ARGUMENTS, r);
    return r;
  }

  /* ********************************************************** */
  // (EXPORT_KEYWORD | EXTERN_KEYWORD SINGLE_STRING_LITERAL? | (INLINE_KEYWORD | NOINLINE_KEYWORD))? functionDeclarationPrototype (SEMICOLON_SYMBOL | blockExpression)
  public static boolean functionDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionDeclaration")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, FUNCTION_DECLARATION, "<function declaration>");
    r = functionDeclaration_0(b, l + 1);
    r = r && functionDeclarationPrototype(b, l + 1);
    r = r && functionDeclaration_2(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // (EXPORT_KEYWORD | EXTERN_KEYWORD SINGLE_STRING_LITERAL? | (INLINE_KEYWORD | NOINLINE_KEYWORD))?
  private static boolean functionDeclaration_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionDeclaration_0")) return false;
    functionDeclaration_0_0(b, l + 1);
    return true;
  }

  // EXPORT_KEYWORD | EXTERN_KEYWORD SINGLE_STRING_LITERAL? | (INLINE_KEYWORD | NOINLINE_KEYWORD)
  private static boolean functionDeclaration_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionDeclaration_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EXPORT_KEYWORD);
    if (!r) r = functionDeclaration_0_0_1(b, l + 1);
    if (!r) r = functionDeclaration_0_0_2(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // EXTERN_KEYWORD SINGLE_STRING_LITERAL?
  private static boolean functionDeclaration_0_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionDeclaration_0_0_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EXTERN_KEYWORD);
    r = r && functionDeclaration_0_0_1_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // SINGLE_STRING_LITERAL?
  private static boolean functionDeclaration_0_0_1_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionDeclaration_0_0_1_1")) return false;
    consumeToken(b, SINGLE_STRING_LITERAL);
    return true;
  }

  // INLINE_KEYWORD | NOINLINE_KEYWORD
  private static boolean functionDeclaration_0_0_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionDeclaration_0_0_2")) return false;
    boolean r;
    r = consumeToken(b, INLINE_KEYWORD);
    if (!r) r = consumeToken(b, NOINLINE_KEYWORD);
    return r;
  }

  // SEMICOLON_SYMBOL | blockExpression
  private static boolean functionDeclaration_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionDeclaration_2")) return false;
    boolean r;
    r = consumeToken(b, SEMICOLON_SYMBOL);
    if (!r) r = blockExpression(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // FN_KEYWORD IDENTIFIER functionPrototypeSuffix
  public static boolean functionDeclarationPrototype(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionDeclarationPrototype")) return false;
    if (!nextTokenIs(b, FN_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, FN_KEYWORD, IDENTIFIER);
    r = r && functionPrototypeSuffix(b, l + 1);
    exit_section_(b, m, FUNCTION_DECLARATION_PROTOTYPE, r);
    return r;
  }

  /* ********************************************************** */
  // FN_KEYWORD functionPrototypeSuffix
  public static boolean functionPrototype(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionPrototype")) return false;
    if (!nextTokenIs(b, FN_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, FN_KEYWORD);
    r = r && functionPrototypeSuffix(b, l + 1);
    exit_section_(b, m, FUNCTION_PROTOTYPE, r);
    return r;
  }

  /* ********************************************************** */
  // LEFT_PARENTHESIS parameterDeclarationList RIGHT_PARENTHESIS byteAlignment? linkSection? callingConvectionSection? EXCLAMATIONMARK_SYMBOL? typeExpression
  static boolean functionPrototypeSuffix(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionPrototypeSuffix")) return false;
    if (!nextTokenIs(b, LEFT_PARENTHESIS)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, LEFT_PARENTHESIS);
    r = r && parameterDeclarationList(b, l + 1);
    r = r && consumeToken(b, RIGHT_PARENTHESIS);
    r = r && functionPrototypeSuffix_3(b, l + 1);
    r = r && functionPrototypeSuffix_4(b, l + 1);
    r = r && functionPrototypeSuffix_5(b, l + 1);
    r = r && functionPrototypeSuffix_6(b, l + 1);
    r = r && typeExpression(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // byteAlignment?
  private static boolean functionPrototypeSuffix_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionPrototypeSuffix_3")) return false;
    byteAlignment(b, l + 1);
    return true;
  }

  // linkSection?
  private static boolean functionPrototypeSuffix_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionPrototypeSuffix_4")) return false;
    linkSection(b, l + 1);
    return true;
  }

  // callingConvectionSection?
  private static boolean functionPrototypeSuffix_5(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionPrototypeSuffix_5")) return false;
    callingConvectionSection(b, l + 1);
    return true;
  }

  // EXCLAMATIONMARK_SYMBOL?
  private static boolean functionPrototypeSuffix_6(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "functionPrototypeSuffix_6")) return false;
    consumeToken(b, EXCLAMATIONMARK_SYMBOL);
    return true;
  }

  /* ********************************************************** */
  // LEFT_PARENTHESIS expression RIGHT_PARENTHESIS
  public static boolean groupedExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "groupedExpression")) return false;
    if (!nextTokenIsFast(b, LEFT_PARENTHESIS)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenFast(b, LEFT_PARENTHESIS);
    r = r && expression(b, l + 1, -1);
    r = r && consumeToken(b, RIGHT_PARENTHESIS);
    exit_section_(b, m, GROUPED_EXPRESSION, r);
    return r;
  }

  /* ********************************************************** */
  // (DOCUMENTATION_COMMENT? IDENTIFIER COMMA_SYMBOL)* (DOCUMENTATION_COMMENT? IDENTIFIER)?
  public static boolean identifierList(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "identifierList")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, IDENTIFIER_LIST, "<identifier list>");
    r = identifierList_0(b, l + 1);
    r = r && identifierList_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // (DOCUMENTATION_COMMENT? IDENTIFIER COMMA_SYMBOL)*
  private static boolean identifierList_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "identifierList_0")) return false;
    while (true) {
      int c = current_position_(b);
      if (!identifierList_0_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "identifierList_0", c)) break;
    }
    return true;
  }

  // DOCUMENTATION_COMMENT? IDENTIFIER COMMA_SYMBOL
  private static boolean identifierList_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "identifierList_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = identifierList_0_0_0(b, l + 1);
    r = r && consumeTokens(b, 0, IDENTIFIER, COMMA_SYMBOL);
    exit_section_(b, m, null, r);
    return r;
  }

  // DOCUMENTATION_COMMENT?
  private static boolean identifierList_0_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "identifierList_0_0_0")) return false;
    consumeToken(b, DOCUMENTATION_COMMENT);
    return true;
  }

  // (DOCUMENTATION_COMMENT? IDENTIFIER)?
  private static boolean identifierList_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "identifierList_1")) return false;
    identifierList_1_0(b, l + 1);
    return true;
  }

  // DOCUMENTATION_COMMENT? IDENTIFIER
  private static boolean identifierList_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "identifierList_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = identifierList_1_0_0(b, l + 1);
    r = r && consumeToken(b, IDENTIFIER);
    exit_section_(b, m, null, r);
    return r;
  }

  // DOCUMENTATION_COMMENT?
  private static boolean identifierList_1_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "identifierList_1_0_0")) return false;
    consumeToken(b, DOCUMENTATION_COMMENT);
    return true;
  }

  /* ********************************************************** */
  // ifPrefix expression (ELSE_KEYWORD payload? expression)?
  public static boolean ifExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifExpression")) return false;
    if (!nextTokenIsFast(b, IF_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = ifPrefix(b, l + 1);
    r = r && expression(b, l + 1, -1);
    r = r && ifExpression_2(b, l + 1);
    exit_section_(b, m, IF_EXPRESSION, r);
    return r;
  }

  // (ELSE_KEYWORD payload? expression)?
  private static boolean ifExpression_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifExpression_2")) return false;
    ifExpression_2_0(b, l + 1);
    return true;
  }

  // ELSE_KEYWORD payload? expression
  private static boolean ifExpression_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifExpression_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenFast(b, ELSE_KEYWORD);
    r = r && ifExpression_2_0_1(b, l + 1);
    r = r && expression(b, l + 1, -1);
    exit_section_(b, m, null, r);
    return r;
  }

  // payload?
  private static boolean ifExpression_2_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifExpression_2_0_1")) return false;
    payload(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // IF_KEYWORD LEFT_PARENTHESIS expression RIGHT_PARENTHESIS pointerPayload?
  public static boolean ifPrefix(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifPrefix")) return false;
    if (!nextTokenIs(b, IF_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, IF_KEYWORD, LEFT_PARENTHESIS);
    r = r && expression(b, l + 1, -1);
    r = r && consumeToken(b, RIGHT_PARENTHESIS);
    r = r && ifPrefix_4(b, l + 1);
    exit_section_(b, m, IF_PREFIX, r);
    return r;
  }

  // pointerPayload?
  private static boolean ifPrefix_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifPrefix_4")) return false;
    pointerPayload(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // ifPrefix labeledBlockExpression (ELSE_KEYWORD payload? statement)?
  //             | ifPrefix expression (SEMICOLON_SYMBOL | ELSE_KEYWORD payload? statement)
  public static boolean ifStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifStatement")) return false;
    if (!nextTokenIs(b, IF_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = ifStatement_0(b, l + 1);
    if (!r) r = ifStatement_1(b, l + 1);
    exit_section_(b, m, IF_STATEMENT, r);
    return r;
  }

  // ifPrefix labeledBlockExpression (ELSE_KEYWORD payload? statement)?
  private static boolean ifStatement_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifStatement_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = ifPrefix(b, l + 1);
    r = r && labeledBlockExpression(b, l + 1);
    r = r && ifStatement_0_2(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (ELSE_KEYWORD payload? statement)?
  private static boolean ifStatement_0_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifStatement_0_2")) return false;
    ifStatement_0_2_0(b, l + 1);
    return true;
  }

  // ELSE_KEYWORD payload? statement
  private static boolean ifStatement_0_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifStatement_0_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, ELSE_KEYWORD);
    r = r && ifStatement_0_2_0_1(b, l + 1);
    r = r && statement(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // payload?
  private static boolean ifStatement_0_2_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifStatement_0_2_0_1")) return false;
    payload(b, l + 1);
    return true;
  }

  // ifPrefix expression (SEMICOLON_SYMBOL | ELSE_KEYWORD payload? statement)
  private static boolean ifStatement_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifStatement_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = ifPrefix(b, l + 1);
    r = r && expression(b, l + 1, -1);
    r = r && ifStatement_1_2(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // SEMICOLON_SYMBOL | ELSE_KEYWORD payload? statement
  private static boolean ifStatement_1_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifStatement_1_2")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, SEMICOLON_SYMBOL);
    if (!r) r = ifStatement_1_2_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // ELSE_KEYWORD payload? statement
  private static boolean ifStatement_1_2_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifStatement_1_2_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, ELSE_KEYWORD);
    r = r && ifStatement_1_2_1_1(b, l + 1);
    r = r && statement(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // payload?
  private static boolean ifStatement_1_2_1_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifStatement_1_2_1_1")) return false;
    payload(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // ifPrefix typeExpression (ELSE_KEYWORD payload? typeExpression)?
  public static boolean ifTypeExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifTypeExpression")) return false;
    if (!nextTokenIsFast(b, IF_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = ifPrefix(b, l + 1);
    r = r && typeExpression(b, l + 1);
    r = r && ifTypeExpression_2(b, l + 1);
    exit_section_(b, m, IF_TYPE_EXPRESSION, r);
    return r;
  }

  // (ELSE_KEYWORD payload? typeExpression)?
  private static boolean ifTypeExpression_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifTypeExpression_2")) return false;
    ifTypeExpression_2_0(b, l + 1);
    return true;
  }

  // ELSE_KEYWORD payload? typeExpression
  private static boolean ifTypeExpression_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifTypeExpression_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenFast(b, ELSE_KEYWORD);
    r = r && ifTypeExpression_2_0_1(b, l + 1);
    r = r && typeExpression(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // payload?
  private static boolean ifTypeExpression_2_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifTypeExpression_2_0_1")) return false;
    payload(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // USINGNAMESPACE_KEYWORD? expression SEMICOLON_SYMBOL
  public static boolean importDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "importDeclaration")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, IMPORT_DECLARATION, "<import declaration>");
    r = importDeclaration_0(b, l + 1);
    r = r && expression(b, l + 1, -1);
    r = r && consumeToken(b, SEMICOLON_SYMBOL);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // USINGNAMESPACE_KEYWORD?
  private static boolean importDeclaration_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "importDeclaration_0")) return false;
    consumeToken(b, USINGNAMESPACE_KEYWORD);
    return true;
  }

  /* ********************************************************** */
  // LEFT_BRACE fieldInitializer (COMMA_SYMBOL fieldInitializer)* COMMA_SYMBOL? RIGHT_BRACE 
  //                 | LEFT_BRACE expression (COMMA_SYMBOL expression)* COMMA_SYMBOL? RIGHT_BRACE
  //                 | LEFT_BRACE RIGHT_BRACE
  public static boolean initializerList(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "initializerList")) return false;
    if (!nextTokenIs(b, LEFT_BRACE)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = initializerList_0(b, l + 1);
    if (!r) r = initializerList_1(b, l + 1);
    if (!r) r = parseTokens(b, 0, LEFT_BRACE, RIGHT_BRACE);
    exit_section_(b, m, INITIALIZER_LIST, r);
    return r;
  }

  // LEFT_BRACE fieldInitializer (COMMA_SYMBOL fieldInitializer)* COMMA_SYMBOL? RIGHT_BRACE
  private static boolean initializerList_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "initializerList_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, LEFT_BRACE);
    r = r && fieldInitializer(b, l + 1);
    r = r && initializerList_0_2(b, l + 1);
    r = r && initializerList_0_3(b, l + 1);
    r = r && consumeToken(b, RIGHT_BRACE);
    exit_section_(b, m, null, r);
    return r;
  }

  // (COMMA_SYMBOL fieldInitializer)*
  private static boolean initializerList_0_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "initializerList_0_2")) return false;
    while (true) {
      int c = current_position_(b);
      if (!initializerList_0_2_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "initializerList_0_2", c)) break;
    }
    return true;
  }

  // COMMA_SYMBOL fieldInitializer
  private static boolean initializerList_0_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "initializerList_0_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, COMMA_SYMBOL);
    r = r && fieldInitializer(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // COMMA_SYMBOL?
  private static boolean initializerList_0_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "initializerList_0_3")) return false;
    consumeToken(b, COMMA_SYMBOL);
    return true;
  }

  // LEFT_BRACE expression (COMMA_SYMBOL expression)* COMMA_SYMBOL? RIGHT_BRACE
  private static boolean initializerList_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "initializerList_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, LEFT_BRACE);
    r = r && expression(b, l + 1, -1);
    r = r && initializerList_1_2(b, l + 1);
    r = r && initializerList_1_3(b, l + 1);
    r = r && consumeToken(b, RIGHT_BRACE);
    exit_section_(b, m, null, r);
    return r;
  }

  // (COMMA_SYMBOL expression)*
  private static boolean initializerList_1_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "initializerList_1_2")) return false;
    while (true) {
      int c = current_position_(b);
      if (!initializerList_1_2_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "initializerList_1_2", c)) break;
    }
    return true;
  }

  // COMMA_SYMBOL expression
  private static boolean initializerList_1_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "initializerList_1_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, COMMA_SYMBOL);
    r = r && expression(b, l + 1, -1);
    exit_section_(b, m, null, r);
    return r;
  }

  // COMMA_SYMBOL?
  private static boolean initializerList_1_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "initializerList_1_3")) return false;
    consumeToken(b, COMMA_SYMBOL);
    return true;
  }

  /* ********************************************************** */
  // blockLabel? blockExpression
  public static boolean labeledBlockExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "labeledBlockExpression")) return false;
    if (!nextTokenIsFast(b, IDENTIFIER, LEFT_BRACE)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, LABELED_BLOCK_EXPRESSION, "<labeled block expression>");
    r = labeledBlockExpression_0(b, l + 1);
    r = r && blockExpression(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // blockLabel?
  private static boolean labeledBlockExpression_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "labeledBlockExpression_0")) return false;
    blockLabel(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // blockLabel? (blockExpression | loopStatement)
  public static boolean labeledBlockOrLoopStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "labeledBlockOrLoopStatement")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, LABELED_BLOCK_OR_LOOP_STATEMENT, "<labeled block or loop statement>");
    r = labeledBlockOrLoopStatement_0(b, l + 1);
    r = r && labeledBlockOrLoopStatement_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // blockLabel?
  private static boolean labeledBlockOrLoopStatement_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "labeledBlockOrLoopStatement_0")) return false;
    blockLabel(b, l + 1);
    return true;
  }

  // blockExpression | loopStatement
  private static boolean labeledBlockOrLoopStatement_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "labeledBlockOrLoopStatement_1")) return false;
    boolean r;
    r = blockExpression(b, l + 1);
    if (!r) r = loopStatement(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // blockLabel? loopExpression
  public static boolean labeledLoopExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "labeledLoopExpression")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, LABELED_LOOP_EXPRESSION, "<labeled loop expression>");
    r = labeledLoopExpression_0(b, l + 1);
    r = r && loopExpression(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // blockLabel?
  private static boolean labeledLoopExpression_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "labeledLoopExpression_0")) return false;
    blockLabel(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // blockLabel blockExpression
  //                       | blockLabel? loopTypeExpression
  public static boolean labeledTypeExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "labeledTypeExpression")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, LABELED_TYPE_EXPRESSION, "<labeled type expression>");
    r = labeledTypeExpression_0(b, l + 1);
    if (!r) r = labeledTypeExpression_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // blockLabel blockExpression
  private static boolean labeledTypeExpression_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "labeledTypeExpression_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = blockLabel(b, l + 1);
    r = r && blockExpression(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // blockLabel? loopTypeExpression
  private static boolean labeledTypeExpression_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "labeledTypeExpression_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = labeledTypeExpression_1_0(b, l + 1);
    r = r && loopTypeExpression(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // blockLabel?
  private static boolean labeledTypeExpression_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "labeledTypeExpression_1_0")) return false;
    blockLabel(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // LINKSECTION_KEYWORD LEFT_PARENTHESIS expression RIGHT_PARENTHESIS
  public static boolean linkSection(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "linkSection")) return false;
    if (!nextTokenIs(b, LINKSECTION_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, LINKSECTION_KEYWORD, LEFT_PARENTHESIS);
    r = r && expression(b, l + 1, -1);
    r = r && consumeToken(b, RIGHT_PARENTHESIS);
    exit_section_(b, m, LINK_SECTION, r);
    return r;
  }

  /* ********************************************************** */
  // INLINE_KEYWORD? (forExpression | whileExpression)
  public static boolean loopExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "loopExpression")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, LOOP_EXPRESSION, "<loop expression>");
    r = loopExpression_0(b, l + 1);
    r = r && loopExpression_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // INLINE_KEYWORD?
  private static boolean loopExpression_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "loopExpression_0")) return false;
    consumeTokenFast(b, INLINE_KEYWORD);
    return true;
  }

  // forExpression | whileExpression
  private static boolean loopExpression_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "loopExpression_1")) return false;
    boolean r;
    r = forExpression(b, l + 1);
    if (!r) r = whileExpression(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // INLINE_KEYWORD? (forStatement | whileStatement)
  public static boolean loopStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "loopStatement")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, LOOP_STATEMENT, "<loop statement>");
    r = loopStatement_0(b, l + 1);
    r = r && loopStatement_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // INLINE_KEYWORD?
  private static boolean loopStatement_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "loopStatement_0")) return false;
    consumeToken(b, INLINE_KEYWORD);
    return true;
  }

  // forStatement | whileStatement
  private static boolean loopStatement_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "loopStatement_1")) return false;
    boolean r;
    r = forStatement(b, l + 1);
    if (!r) r = whileStatement(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // INLINE_KEYWORD? (forTypeExpression | whileTypeExpression)
  public static boolean loopTypeExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "loopTypeExpression")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, LOOP_TYPE_EXPRESSION, "<loop type expression>");
    r = loopTypeExpression_0(b, l + 1);
    r = r && loopTypeExpression_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // INLINE_KEYWORD?
  private static boolean loopTypeExpression_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "loopTypeExpression_0")) return false;
    consumeTokenFast(b, INLINE_KEYWORD);
    return true;
  }

  // forTypeExpression | whileTypeExpression
  private static boolean loopTypeExpression_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "loopTypeExpression_1")) return false;
    boolean r;
    r = forTypeExpression(b, l + 1);
    if (!r) r = whileTypeExpression(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // MERGE_ERROR_SET_SYMBOL
  //                           | ASTERISK_SYMBOL
  //                           | DIVIDE_SYMBOL
  //                           | MODULUS_SYMBOL
  //                           | DOUBLE_ASTERISK_SYMBOL // multiply array operator
  //                           | MULTIPLY_OVERFLOW_SYMBOL
  static boolean multiplyOperation(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "multiplyOperation")) return false;
    boolean r;
    r = consumeToken(b, MERGE_ERROR_SET_SYMBOL);
    if (!r) r = consumeToken(b, ASTERISK_SYMBOL);
    if (!r) r = consumeToken(b, DIVIDE_SYMBOL);
    if (!r) r = consumeToken(b, MODULUS_SYMBOL);
    if (!r) r = consumeToken(b, DOUBLE_ASTERISK_SYMBOL);
    if (!r) r = consumeToken(b, MULTIPLY_OVERFLOW_SYMBOL);
    return r;
  }

  /* ********************************************************** */
  // DOCUMENTATION_COMMENT? (NOALIAS_KEYWORD | COMPTIME_KEYWORD)? (IDENTIFIER COLON_SYMBOL)? parameterType
  //                      | RANGE_SYMBOL
  public static boolean parameterDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameterDeclaration")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, PARAMETER_DECLARATION, "<parameter declaration>");
    r = parameterDeclaration_0(b, l + 1);
    if (!r) r = consumeToken(b, RANGE_SYMBOL);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // DOCUMENTATION_COMMENT? (NOALIAS_KEYWORD | COMPTIME_KEYWORD)? (IDENTIFIER COLON_SYMBOL)? parameterType
  private static boolean parameterDeclaration_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameterDeclaration_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = parameterDeclaration_0_0(b, l + 1);
    r = r && parameterDeclaration_0_1(b, l + 1);
    r = r && parameterDeclaration_0_2(b, l + 1);
    r = r && parameterType(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // DOCUMENTATION_COMMENT?
  private static boolean parameterDeclaration_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameterDeclaration_0_0")) return false;
    consumeToken(b, DOCUMENTATION_COMMENT);
    return true;
  }

  // (NOALIAS_KEYWORD | COMPTIME_KEYWORD)?
  private static boolean parameterDeclaration_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameterDeclaration_0_1")) return false;
    parameterDeclaration_0_1_0(b, l + 1);
    return true;
  }

  // NOALIAS_KEYWORD | COMPTIME_KEYWORD
  private static boolean parameterDeclaration_0_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameterDeclaration_0_1_0")) return false;
    boolean r;
    r = consumeToken(b, NOALIAS_KEYWORD);
    if (!r) r = consumeToken(b, COMPTIME_KEYWORD);
    return r;
  }

  // (IDENTIFIER COLON_SYMBOL)?
  private static boolean parameterDeclaration_0_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameterDeclaration_0_2")) return false;
    parameterDeclaration_0_2_0(b, l + 1);
    return true;
  }

  // IDENTIFIER COLON_SYMBOL
  private static boolean parameterDeclaration_0_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameterDeclaration_0_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, IDENTIFIER, COLON_SYMBOL);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // (parameterDeclaration COMMA_SYMBOL)* parameterDeclaration?
  public static boolean parameterDeclarationList(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameterDeclarationList")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, PARAMETER_DECLARATION_LIST, "<parameter declaration list>");
    r = parameterDeclarationList_0(b, l + 1);
    r = r && parameterDeclarationList_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // (parameterDeclaration COMMA_SYMBOL)*
  private static boolean parameterDeclarationList_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameterDeclarationList_0")) return false;
    while (true) {
      int c = current_position_(b);
      if (!parameterDeclarationList_0_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "parameterDeclarationList_0", c)) break;
    }
    return true;
  }

  // parameterDeclaration COMMA_SYMBOL
  private static boolean parameterDeclarationList_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameterDeclarationList_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = parameterDeclaration(b, l + 1);
    r = r && consumeToken(b, COMMA_SYMBOL);
    exit_section_(b, m, null, r);
    return r;
  }

  // parameterDeclaration?
  private static boolean parameterDeclarationList_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameterDeclarationList_1")) return false;
    parameterDeclaration(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // ANYTYPE_KEYWORD
  //               | typeExpression
  public static boolean parameterType(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameterType")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, PARAMETER_TYPE, "<parameter type>");
    r = consumeToken(b, ANYTYPE_KEYWORD);
    if (!r) r = typeExpression(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // PIPE_SYMBOL IDENTIFIER PIPE_SYMBOL
  public static boolean payload(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "payload")) return false;
    if (!nextTokenIs(b, PIPE_SYMBOL)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, PIPE_SYMBOL, IDENTIFIER, PIPE_SYMBOL);
    exit_section_(b, m, PAYLOAD, r);
    return r;
  }

  /* ********************************************************** */
  // PIPE_SYMBOL ASTERISK_SYMBOL? IDENTIFIER (COMMA_SYMBOL IDENTIFIER)? PIPE_SYMBOL
  public static boolean pointerIndexPayload(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "pointerIndexPayload")) return false;
    if (!nextTokenIs(b, PIPE_SYMBOL)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, PIPE_SYMBOL);
    r = r && pointerIndexPayload_1(b, l + 1);
    r = r && consumeToken(b, IDENTIFIER);
    r = r && pointerIndexPayload_3(b, l + 1);
    r = r && consumeToken(b, PIPE_SYMBOL);
    exit_section_(b, m, POINTER_INDEX_PAYLOAD, r);
    return r;
  }

  // ASTERISK_SYMBOL?
  private static boolean pointerIndexPayload_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "pointerIndexPayload_1")) return false;
    consumeToken(b, ASTERISK_SYMBOL);
    return true;
  }

  // (COMMA_SYMBOL IDENTIFIER)?
  private static boolean pointerIndexPayload_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "pointerIndexPayload_3")) return false;
    pointerIndexPayload_3_0(b, l + 1);
    return true;
  }

  // COMMA_SYMBOL IDENTIFIER
  private static boolean pointerIndexPayload_3_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "pointerIndexPayload_3_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, COMMA_SYMBOL, IDENTIFIER);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // PIPE_SYMBOL ASTERISK_SYMBOL? IDENTIFIER PIPE_SYMBOL
  public static boolean pointerPayload(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "pointerPayload")) return false;
    if (!nextTokenIs(b, PIPE_SYMBOL)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, PIPE_SYMBOL);
    r = r && pointerPayload_1(b, l + 1);
    r = r && consumeTokens(b, 0, IDENTIFIER, PIPE_SYMBOL);
    exit_section_(b, m, POINTER_PAYLOAD, r);
    return r;
  }

  // ASTERISK_SYMBOL?
  private static boolean pointerPayload_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "pointerPayload_1")) return false;
    consumeToken(b, ASTERISK_SYMBOL);
    return true;
  }

  /* ********************************************************** */
  // ASTERISK_SYMBOL // pointer type symbol
  //                  | DOUBLE_ASTERISK_SYMBOL // double pointer type symbol
  //                  | unknownLengthPointer
  //                  | C_POINTER_SYMBOL
  public static boolean pointerTypeStart(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "pointerTypeStart")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, POINTER_TYPE_START, "<pointer type start>");
    r = consumeToken(b, ASTERISK_SYMBOL);
    if (!r) r = consumeToken(b, DOUBLE_ASTERISK_SYMBOL);
    if (!r) r = unknownLengthPointer(b, l + 1);
    if (!r) r = consumeToken(b, C_POINTER_SYMBOL);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // EXCLAMATIONMARK_SYMBOL // logical not operator
  //                         | MINUS_SYMBOL // negate operator
  //                         | MINUS_PERCENT_SYMBOL // wrapping negation operator
  //                         | BITWISE_NOT_SYMBOL
  //                         | AMPERSAND_SYMBOL // address-of operator
  //                         | TRY_KEYWORD
  //                         | AWAIT_KEYWORD
  static boolean prefixOperation(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "prefixOperation")) return false;
    boolean r;
    r = consumeToken(b, EXCLAMATIONMARK_SYMBOL);
    if (!r) r = consumeToken(b, MINUS_SYMBOL);
    if (!r) r = consumeToken(b, MINUS_PERCENT_SYMBOL);
    if (!r) r = consumeToken(b, BITWISE_NOT_SYMBOL);
    if (!r) r = consumeToken(b, AMPERSAND_SYMBOL);
    if (!r) r = consumeToken(b, TRY_KEYWORD);
    if (!r) r = consumeToken(b, AWAIT_KEYWORD);
    return r;
  }

  /* ********************************************************** */
  // OPTIONAL_SYMBOL
  //                             | ANYFRAME_KEYWORD MINUS_RIGHT_ARROW
  //                             | sliceTypeStart (byteAlignment | CONST_KEYWORD | VOLATILE_KEYWORD | ALLOWZERO_KEYWORD)*
  //                             | pointerTypeStart (ALIGN_KEYWORD LEFT_PARENTHESIS expression (COLON_SYMBOL INTEGER_LITERAL COLON_SYMBOL INTEGER_LITERAL)? RIGHT_PARENTHESIS | CONST_KEYWORD | VOLATILE_KEYWORD | ALLOWZERO_KEYWORD)*
  //                             | arrayTypeStart
  static boolean prefixTypeOperation(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "prefixTypeOperation")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, OPTIONAL_SYMBOL);
    if (!r) r = parseTokens(b, 0, ANYFRAME_KEYWORD, MINUS_RIGHT_ARROW);
    if (!r) r = prefixTypeOperation_2(b, l + 1);
    if (!r) r = prefixTypeOperation_3(b, l + 1);
    if (!r) r = arrayTypeStart(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // sliceTypeStart (byteAlignment | CONST_KEYWORD | VOLATILE_KEYWORD | ALLOWZERO_KEYWORD)*
  private static boolean prefixTypeOperation_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "prefixTypeOperation_2")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = sliceTypeStart(b, l + 1);
    r = r && prefixTypeOperation_2_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (byteAlignment | CONST_KEYWORD | VOLATILE_KEYWORD | ALLOWZERO_KEYWORD)*
  private static boolean prefixTypeOperation_2_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "prefixTypeOperation_2_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!prefixTypeOperation_2_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "prefixTypeOperation_2_1", c)) break;
    }
    return true;
  }

  // byteAlignment | CONST_KEYWORD | VOLATILE_KEYWORD | ALLOWZERO_KEYWORD
  private static boolean prefixTypeOperation_2_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "prefixTypeOperation_2_1_0")) return false;
    boolean r;
    r = byteAlignment(b, l + 1);
    if (!r) r = consumeToken(b, CONST_KEYWORD);
    if (!r) r = consumeToken(b, VOLATILE_KEYWORD);
    if (!r) r = consumeToken(b, ALLOWZERO_KEYWORD);
    return r;
  }

  // pointerTypeStart (ALIGN_KEYWORD LEFT_PARENTHESIS expression (COLON_SYMBOL INTEGER_LITERAL COLON_SYMBOL INTEGER_LITERAL)? RIGHT_PARENTHESIS | CONST_KEYWORD | VOLATILE_KEYWORD | ALLOWZERO_KEYWORD)*
  private static boolean prefixTypeOperation_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "prefixTypeOperation_3")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = pointerTypeStart(b, l + 1);
    r = r && prefixTypeOperation_3_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (ALIGN_KEYWORD LEFT_PARENTHESIS expression (COLON_SYMBOL INTEGER_LITERAL COLON_SYMBOL INTEGER_LITERAL)? RIGHT_PARENTHESIS | CONST_KEYWORD | VOLATILE_KEYWORD | ALLOWZERO_KEYWORD)*
  private static boolean prefixTypeOperation_3_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "prefixTypeOperation_3_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!prefixTypeOperation_3_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "prefixTypeOperation_3_1", c)) break;
    }
    return true;
  }

  // ALIGN_KEYWORD LEFT_PARENTHESIS expression (COLON_SYMBOL INTEGER_LITERAL COLON_SYMBOL INTEGER_LITERAL)? RIGHT_PARENTHESIS | CONST_KEYWORD | VOLATILE_KEYWORD | ALLOWZERO_KEYWORD
  private static boolean prefixTypeOperation_3_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "prefixTypeOperation_3_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = prefixTypeOperation_3_1_0_0(b, l + 1);
    if (!r) r = consumeToken(b, CONST_KEYWORD);
    if (!r) r = consumeToken(b, VOLATILE_KEYWORD);
    if (!r) r = consumeToken(b, ALLOWZERO_KEYWORD);
    exit_section_(b, m, null, r);
    return r;
  }

  // ALIGN_KEYWORD LEFT_PARENTHESIS expression (COLON_SYMBOL INTEGER_LITERAL COLON_SYMBOL INTEGER_LITERAL)? RIGHT_PARENTHESIS
  private static boolean prefixTypeOperation_3_1_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "prefixTypeOperation_3_1_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, ALIGN_KEYWORD, LEFT_PARENTHESIS);
    r = r && expression(b, l + 1, -1);
    r = r && prefixTypeOperation_3_1_0_0_3(b, l + 1);
    r = r && consumeToken(b, RIGHT_PARENTHESIS);
    exit_section_(b, m, null, r);
    return r;
  }

  // (COLON_SYMBOL INTEGER_LITERAL COLON_SYMBOL INTEGER_LITERAL)?
  private static boolean prefixTypeOperation_3_1_0_0_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "prefixTypeOperation_3_1_0_0_3")) return false;
    prefixTypeOperation_3_1_0_0_3_0(b, l + 1);
    return true;
  }

  // COLON_SYMBOL INTEGER_LITERAL COLON_SYMBOL INTEGER_LITERAL
  private static boolean prefixTypeOperation_3_1_0_0_3_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "prefixTypeOperation_3_1_0_0_3_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, COLON_SYMBOL, INTEGER_LITERAL, COLON_SYMBOL, INTEGER_LITERAL);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // BUILTIN_IDENTIFIER functionCallArguments
  //                       | CHARACTER_LITERAL
  //                       | containerDeclaration
  //                       | DOT_SYMBOL IDENTIFIER
  //                       | DOT_SYMBOL initializerList
  //                       | errorSetDeclaration
  //                       | FLOAT_LITERAL
  //                       | functionPrototype
  //                       | groupedExpression
  //                       | labeledTypeExpression
  //                       | IDENTIFIER
  //                       | ifTypeExpression
  //                       | INTEGER_LITERAL
  //                       | COMPTIME_KEYWORD typeExpression
  //                       | ERROR_KEYWORD DOT_SYMBOL IDENTIFIER
  //                       | FALSE_KEYWORD
  //                       | NULL_KEYWORD
  //                       | ANYFRAME_KEYWORD
  //                       | TRUE_KEYWORD
  //                       | UNDEFINED_KEYWORD
  //                       | UNREACHABLE_KEYWORD
  //                       | stringLiteral
  //                       | switchExpression
  public static boolean primaryTypeExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "primaryTypeExpression")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, PRIMARY_TYPE_EXPRESSION, "<primary type expression>");
    r = primaryTypeExpression_0(b, l + 1);
    if (!r) r = consumeTokenFast(b, CHARACTER_LITERAL);
    if (!r) r = containerDeclaration(b, l + 1);
    if (!r) r = primaryTypeExpression_3(b, l + 1);
    if (!r) r = primaryTypeExpression_4(b, l + 1);
    if (!r) r = errorSetDeclaration(b, l + 1);
    if (!r) r = consumeTokenFast(b, FLOAT_LITERAL);
    if (!r) r = functionPrototype(b, l + 1);
    if (!r) r = groupedExpression(b, l + 1);
    if (!r) r = labeledTypeExpression(b, l + 1);
    if (!r) r = consumeTokenFast(b, IDENTIFIER);
    if (!r) r = ifTypeExpression(b, l + 1);
    if (!r) r = consumeTokenFast(b, INTEGER_LITERAL);
    if (!r) r = primaryTypeExpression_13(b, l + 1);
    if (!r) r = primaryTypeExpression_14(b, l + 1);
    if (!r) r = consumeTokenFast(b, FALSE_KEYWORD);
    if (!r) r = consumeTokenFast(b, NULL_KEYWORD);
    if (!r) r = consumeTokenFast(b, ANYFRAME_KEYWORD);
    if (!r) r = consumeTokenFast(b, TRUE_KEYWORD);
    if (!r) r = consumeTokenFast(b, UNDEFINED_KEYWORD);
    if (!r) r = consumeTokenFast(b, UNREACHABLE_KEYWORD);
    if (!r) r = stringLiteral(b, l + 1);
    if (!r) r = switchExpression(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // BUILTIN_IDENTIFIER functionCallArguments
  private static boolean primaryTypeExpression_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "primaryTypeExpression_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenFast(b, BUILTIN_IDENTIFIER);
    r = r && functionCallArguments(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // DOT_SYMBOL IDENTIFIER
  private static boolean primaryTypeExpression_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "primaryTypeExpression_3")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, DOT_SYMBOL, IDENTIFIER);
    exit_section_(b, m, null, r);
    return r;
  }

  // DOT_SYMBOL initializerList
  private static boolean primaryTypeExpression_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "primaryTypeExpression_4")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenFast(b, DOT_SYMBOL);
    r = r && initializerList(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // COMPTIME_KEYWORD typeExpression
  private static boolean primaryTypeExpression_13(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "primaryTypeExpression_13")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenFast(b, COMPTIME_KEYWORD);
    r = r && typeExpression(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // ERROR_KEYWORD DOT_SYMBOL IDENTIFIER
  private static boolean primaryTypeExpression_14(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "primaryTypeExpression_14")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, ERROR_KEYWORD, DOT_SYMBOL, IDENTIFIER);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // functionDeclaration
  //                                         | topLevelVariableDeclaration
  //                                         | importDeclaration
  static boolean publicizableTopLevelDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "publicizableTopLevelDeclaration")) return false;
    boolean r;
    r = functionDeclaration(b, l + 1);
    if (!r) r = topLevelVariableDeclaration(b, l + 1);
    if (!r) r = importDeclaration(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // COLON_SYMBOL expression
  public static boolean sentinelTerminator(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "sentinelTerminator")) return false;
    if (!nextTokenIs(b, COLON_SYMBOL)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, COLON_SYMBOL);
    r = r && expression(b, l + 1, -1);
    exit_section_(b, m, SENTINEL_TERMINATOR, r);
    return r;
  }

  /* ********************************************************** */
  // LEFT_BRACKET sentinelTerminator? RIGHT_BRACKET
  public static boolean sliceTypeStart(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "sliceTypeStart")) return false;
    if (!nextTokenIs(b, LEFT_BRACKET)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, LEFT_BRACKET);
    r = r && sliceTypeStart_1(b, l + 1);
    r = r && consumeToken(b, RIGHT_BRACKET);
    exit_section_(b, m, SLICE_TYPE_START, r);
    return r;
  }

  // sentinelTerminator?
  private static boolean sliceTypeStart_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "sliceTypeStart_1")) return false;
    sentinelTerminator(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // COMPTIME_KEYWORD? variableDeclaration
  //           | (COMPTIME_KEYWORD | NOSUSPEND_KEYWORD | DEFER_KEYWORD) blockExpressionStatement
  //           | ERRDEFER_KEYWORD payload? blockExpressionStatement
  //           | SUSPEND_KEYWORD blockExpressionStatement
  //           | ifStatement
  //           | labeledBlockOrLoopStatement
  //           | switchExpression // no trailing semicolon required; the expression is also a statement
  //           | expression SEMICOLON_SYMBOL
  public static boolean statement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "statement")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, STATEMENT, "<statement>");
    r = statement_0(b, l + 1);
    if (!r) r = statement_1(b, l + 1);
    if (!r) r = statement_2(b, l + 1);
    if (!r) r = statement_3(b, l + 1);
    if (!r) r = ifStatement(b, l + 1);
    if (!r) r = labeledBlockOrLoopStatement(b, l + 1);
    if (!r) r = switchExpression(b, l + 1);
    if (!r) r = statement_7(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // COMPTIME_KEYWORD? variableDeclaration
  private static boolean statement_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "statement_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = statement_0_0(b, l + 1);
    r = r && variableDeclaration(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // COMPTIME_KEYWORD?
  private static boolean statement_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "statement_0_0")) return false;
    consumeToken(b, COMPTIME_KEYWORD);
    return true;
  }

  // (COMPTIME_KEYWORD | NOSUSPEND_KEYWORD | DEFER_KEYWORD) blockExpressionStatement
  private static boolean statement_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "statement_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = statement_1_0(b, l + 1);
    r = r && blockExpressionStatement(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // COMPTIME_KEYWORD | NOSUSPEND_KEYWORD | DEFER_KEYWORD
  private static boolean statement_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "statement_1_0")) return false;
    boolean r;
    r = consumeToken(b, COMPTIME_KEYWORD);
    if (!r) r = consumeToken(b, NOSUSPEND_KEYWORD);
    if (!r) r = consumeToken(b, DEFER_KEYWORD);
    return r;
  }

  // ERRDEFER_KEYWORD payload? blockExpressionStatement
  private static boolean statement_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "statement_2")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, ERRDEFER_KEYWORD);
    r = r && statement_2_1(b, l + 1);
    r = r && blockExpressionStatement(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // payload?
  private static boolean statement_2_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "statement_2_1")) return false;
    payload(b, l + 1);
    return true;
  }

  // SUSPEND_KEYWORD blockExpressionStatement
  private static boolean statement_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "statement_3")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, SUSPEND_KEYWORD);
    r = r && blockExpressionStatement(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // expression SEMICOLON_SYMBOL
  private static boolean statement_7(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "statement_7")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = expression(b, l + 1, -1);
    r = r && consumeToken(b, SEMICOLON_SYMBOL);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // (stringLiteral COMMA_SYMBOL)* stringLiteral?
  public static boolean stringList(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "stringList")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, STRING_LIST, "<string list>");
    r = stringList_0(b, l + 1);
    r = r && stringList_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // (stringLiteral COMMA_SYMBOL)*
  private static boolean stringList_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "stringList_0")) return false;
    while (true) {
      int c = current_position_(b);
      if (!stringList_0_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "stringList_0", c)) break;
    }
    return true;
  }

  // stringLiteral COMMA_SYMBOL
  private static boolean stringList_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "stringList_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = stringLiteral(b, l + 1);
    r = r && consumeToken(b, COMMA_SYMBOL);
    exit_section_(b, m, null, r);
    return r;
  }

  // stringLiteral?
  private static boolean stringList_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "stringList_1")) return false;
    stringLiteral(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // SINGLE_STRING_LITERAL | LINE_STRING_LITERAL
  public static boolean stringLiteral(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "stringLiteral")) return false;
    if (!nextTokenIs(b, "<string literal>", LINE_STRING_LITERAL, SINGLE_STRING_LITERAL)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, STRING_LITERAL, "<string literal>");
    r = consumeToken(b, SINGLE_STRING_LITERAL);
    if (!r) r = consumeToken(b, LINE_STRING_LITERAL);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // ASYNC_KEYWORD primaryTypeExpression suffixOperation* functionCallArguments
  //                  | primaryTypeExpression (suffixOperation | functionCallArguments)*
  public static boolean suffixExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "suffixExpression")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, SUFFIX_EXPRESSION, "<suffix expression>");
    r = suffixExpression_0(b, l + 1);
    if (!r) r = suffixExpression_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // ASYNC_KEYWORD primaryTypeExpression suffixOperation* functionCallArguments
  private static boolean suffixExpression_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "suffixExpression_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenFast(b, ASYNC_KEYWORD);
    r = r && primaryTypeExpression(b, l + 1);
    r = r && suffixExpression_0_2(b, l + 1);
    r = r && functionCallArguments(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // suffixOperation*
  private static boolean suffixExpression_0_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "suffixExpression_0_2")) return false;
    while (true) {
      int c = current_position_(b);
      if (!suffixOperation(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "suffixExpression_0_2", c)) break;
    }
    return true;
  }

  // primaryTypeExpression (suffixOperation | functionCallArguments)*
  private static boolean suffixExpression_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "suffixExpression_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = primaryTypeExpression(b, l + 1);
    r = r && suffixExpression_1_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (suffixOperation | functionCallArguments)*
  private static boolean suffixExpression_1_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "suffixExpression_1_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!suffixExpression_1_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "suffixExpression_1_1", c)) break;
    }
    return true;
  }

  // suffixOperation | functionCallArguments
  private static boolean suffixExpression_1_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "suffixExpression_1_1_0")) return false;
    boolean r;
    r = suffixOperation(b, l + 1);
    if (!r) r = functionCallArguments(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // LEFT_BRACKET expression (SLICE_SYMBOL expression? sentinelTerminator?)? RIGHT_BRACKET
  //                         | DOT_SYMBOL IDENTIFIER
  //                         | DEREFERENCE_POINTER_SYMBOL
  //                         | UNWRAP_OPTIONAL_SYMBOL
  static boolean suffixOperation(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "suffixOperation")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = suffixOperation_0(b, l + 1);
    if (!r) r = parseTokens(b, 0, DOT_SYMBOL, IDENTIFIER);
    if (!r) r = consumeToken(b, DEREFERENCE_POINTER_SYMBOL);
    if (!r) r = consumeToken(b, UNWRAP_OPTIONAL_SYMBOL);
    exit_section_(b, m, null, r);
    return r;
  }

  // LEFT_BRACKET expression (SLICE_SYMBOL expression? sentinelTerminator?)? RIGHT_BRACKET
  private static boolean suffixOperation_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "suffixOperation_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, LEFT_BRACKET);
    r = r && expression(b, l + 1, -1);
    r = r && suffixOperation_0_2(b, l + 1);
    r = r && consumeToken(b, RIGHT_BRACKET);
    exit_section_(b, m, null, r);
    return r;
  }

  // (SLICE_SYMBOL expression? sentinelTerminator?)?
  private static boolean suffixOperation_0_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "suffixOperation_0_2")) return false;
    suffixOperation_0_2_0(b, l + 1);
    return true;
  }

  // SLICE_SYMBOL expression? sentinelTerminator?
  private static boolean suffixOperation_0_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "suffixOperation_0_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, SLICE_SYMBOL);
    r = r && suffixOperation_0_2_0_1(b, l + 1);
    r = r && suffixOperation_0_2_0_2(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // expression?
  private static boolean suffixOperation_0_2_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "suffixOperation_0_2_0_1")) return false;
    expression(b, l + 1, -1);
    return true;
  }

  // sentinelTerminator?
  private static boolean suffixOperation_0_2_0_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "suffixOperation_0_2_0_2")) return false;
    sentinelTerminator(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // switchItem (COMMA_SYMBOL switchItem)* COMMA_SYMBOL?
  //            | ELSE_KEYWORD
  public static boolean switchCase(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchCase")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, SWITCH_CASE, "<switch case>");
    r = switchCase_0(b, l + 1);
    if (!r) r = consumeToken(b, ELSE_KEYWORD);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // switchItem (COMMA_SYMBOL switchItem)* COMMA_SYMBOL?
  private static boolean switchCase_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchCase_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = switchItem(b, l + 1);
    r = r && switchCase_0_1(b, l + 1);
    r = r && switchCase_0_2(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (COMMA_SYMBOL switchItem)*
  private static boolean switchCase_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchCase_0_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!switchCase_0_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "switchCase_0_1", c)) break;
    }
    return true;
  }

  // COMMA_SYMBOL switchItem
  private static boolean switchCase_0_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchCase_0_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, COMMA_SYMBOL);
    r = r && switchItem(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // COMMA_SYMBOL?
  private static boolean switchCase_0_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchCase_0_2")) return false;
    consumeToken(b, COMMA_SYMBOL);
    return true;
  }

  /* ********************************************************** */
  // SWITCH_KEYWORD LEFT_PARENTHESIS expression RIGHT_PARENTHESIS LEFT_BRACE switchProngList RIGHT_BRACE
  public static boolean switchExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchExpression")) return false;
    if (!nextTokenIsFast(b, SWITCH_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, SWITCH_KEYWORD, LEFT_PARENTHESIS);
    r = r && expression(b, l + 1, -1);
    r = r && consumeTokens(b, 0, RIGHT_PARENTHESIS, LEFT_BRACE);
    r = r && switchProngList(b, l + 1);
    r = r && consumeToken(b, RIGHT_BRACE);
    exit_section_(b, m, SWITCH_EXPRESSION, r);
    return r;
  }

  /* ********************************************************** */
  // expression (RANGE_SYMBOL expression)?
  public static boolean switchItem(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchItem")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, SWITCH_ITEM, "<switch item>");
    r = expression(b, l + 1, -1);
    r = r && switchItem_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // (RANGE_SYMBOL expression)?
  private static boolean switchItem_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchItem_1")) return false;
    switchItem_1_0(b, l + 1);
    return true;
  }

  // RANGE_SYMBOL expression
  private static boolean switchItem_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchItem_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, RANGE_SYMBOL);
    r = r && expression(b, l + 1, -1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // switchCase SWITCH_PRONG_SYMBOL pointerPayload? expression
  public static boolean switchProng(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchProng")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, SWITCH_PRONG, "<switch prong>");
    r = switchCase(b, l + 1);
    r = r && consumeToken(b, SWITCH_PRONG_SYMBOL);
    r = r && switchProng_2(b, l + 1);
    r = r && expression(b, l + 1, -1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // pointerPayload?
  private static boolean switchProng_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchProng_2")) return false;
    pointerPayload(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // (switchProng COMMA_SYMBOL)* switchProng?
  public static boolean switchProngList(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchProngList")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, SWITCH_PRONG_LIST, "<switch prong list>");
    r = switchProngList_0(b, l + 1);
    r = r && switchProngList_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // (switchProng COMMA_SYMBOL)*
  private static boolean switchProngList_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchProngList_0")) return false;
    while (true) {
      int c = current_position_(b);
      if (!switchProngList_0_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "switchProngList_0", c)) break;
    }
    return true;
  }

  // switchProng COMMA_SYMBOL
  private static boolean switchProngList_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchProngList_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = switchProng(b, l + 1);
    r = r && consumeToken(b, COMMA_SYMBOL);
    exit_section_(b, m, null, r);
    return r;
  }

  // switchProng?
  private static boolean switchProngList_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchProngList_1")) return false;
    switchProng(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // DOCUMENTATION_COMMENT? TEST_KEYWORD SINGLE_STRING_LITERAL? blockExpression
  public static boolean testDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "testDeclaration")) return false;
    if (!nextTokenIs(b, "<test declaration>", DOCUMENTATION_COMMENT, TEST_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, TEST_DECLARATION, "<test declaration>");
    r = testDeclaration_0(b, l + 1);
    r = r && consumeToken(b, TEST_KEYWORD);
    r = r && testDeclaration_2(b, l + 1);
    r = r && blockExpression(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // DOCUMENTATION_COMMENT?
  private static boolean testDeclaration_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "testDeclaration_0")) return false;
    consumeToken(b, DOCUMENTATION_COMMENT);
    return true;
  }

  // SINGLE_STRING_LITERAL?
  private static boolean testDeclaration_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "testDeclaration_2")) return false;
    consumeToken(b, SINGLE_STRING_LITERAL);
    return true;
  }

  /* ********************************************************** */
  // DOCUMENTATION_COMMENT? COMPTIME_KEYWORD blockExpression
  public static boolean topLevelComptimeBlock(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "topLevelComptimeBlock")) return false;
    if (!nextTokenIs(b, "<top level comptime block>", COMPTIME_KEYWORD, DOCUMENTATION_COMMENT)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, TOP_LEVEL_COMPTIME_BLOCK, "<top level comptime block>");
    r = topLevelComptimeBlock_0(b, l + 1);
    r = r && consumeToken(b, COMPTIME_KEYWORD);
    r = r && blockExpression(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // DOCUMENTATION_COMMENT?
  private static boolean topLevelComptimeBlock_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "topLevelComptimeBlock_0")) return false;
    consumeToken(b, DOCUMENTATION_COMMENT);
    return true;
  }

  /* ********************************************************** */
  // testDeclaration
  //                             | topLevelComptimeBlock
  //                             | DOCUMENTATION_COMMENT? PUB_KEYWORD? publicizableTopLevelDeclaration
  static boolean topLevelDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "topLevelDeclaration")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = testDeclaration(b, l + 1);
    if (!r) r = topLevelComptimeBlock(b, l + 1);
    if (!r) r = topLevelDeclaration_2(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // DOCUMENTATION_COMMENT? PUB_KEYWORD? publicizableTopLevelDeclaration
  private static boolean topLevelDeclaration_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "topLevelDeclaration_2")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = topLevelDeclaration_2_0(b, l + 1);
    r = r && topLevelDeclaration_2_1(b, l + 1);
    r = r && publicizableTopLevelDeclaration(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // DOCUMENTATION_COMMENT?
  private static boolean topLevelDeclaration_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "topLevelDeclaration_2_0")) return false;
    consumeToken(b, DOCUMENTATION_COMMENT);
    return true;
  }

  // PUB_KEYWORD?
  private static boolean topLevelDeclaration_2_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "topLevelDeclaration_2_1")) return false;
    consumeToken(b, PUB_KEYWORD);
    return true;
  }

  /* ********************************************************** */
  // TOP_LEVEL_DOCUMENTATION_COMMENT? topLevelDeclaration* (containerField COMMA_SYMBOL)* containerField? topLevelDeclaration*
  static boolean topLevelDeclarations(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "topLevelDeclarations")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = topLevelDeclarations_0(b, l + 1);
    r = r && topLevelDeclarations_1(b, l + 1);
    r = r && topLevelDeclarations_2(b, l + 1);
    r = r && topLevelDeclarations_3(b, l + 1);
    r = r && topLevelDeclarations_4(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // TOP_LEVEL_DOCUMENTATION_COMMENT?
  private static boolean topLevelDeclarations_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "topLevelDeclarations_0")) return false;
    consumeToken(b, TOP_LEVEL_DOCUMENTATION_COMMENT);
    return true;
  }

  // topLevelDeclaration*
  private static boolean topLevelDeclarations_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "topLevelDeclarations_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!topLevelDeclaration(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "topLevelDeclarations_1", c)) break;
    }
    return true;
  }

  // (containerField COMMA_SYMBOL)*
  private static boolean topLevelDeclarations_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "topLevelDeclarations_2")) return false;
    while (true) {
      int c = current_position_(b);
      if (!topLevelDeclarations_2_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "topLevelDeclarations_2", c)) break;
    }
    return true;
  }

  // containerField COMMA_SYMBOL
  private static boolean topLevelDeclarations_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "topLevelDeclarations_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = containerField(b, l + 1);
    r = r && consumeToken(b, COMMA_SYMBOL);
    exit_section_(b, m, null, r);
    return r;
  }

  // containerField?
  private static boolean topLevelDeclarations_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "topLevelDeclarations_3")) return false;
    containerField(b, l + 1);
    return true;
  }

  // topLevelDeclaration*
  private static boolean topLevelDeclarations_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "topLevelDeclarations_4")) return false;
    while (true) {
      int c = current_position_(b);
      if (!topLevelDeclaration(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "topLevelDeclarations_4", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // (EXPORT_KEYWORD | EXTERN_KEYWORD SINGLE_STRING_LITERAL?)? THREADLOCAL_KEYWORD? variableDeclaration
  public static boolean topLevelVariableDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "topLevelVariableDeclaration")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, TOP_LEVEL_VARIABLE_DECLARATION, "<top level variable declaration>");
    r = topLevelVariableDeclaration_0(b, l + 1);
    r = r && topLevelVariableDeclaration_1(b, l + 1);
    r = r && variableDeclaration(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // (EXPORT_KEYWORD | EXTERN_KEYWORD SINGLE_STRING_LITERAL?)?
  private static boolean topLevelVariableDeclaration_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "topLevelVariableDeclaration_0")) return false;
    topLevelVariableDeclaration_0_0(b, l + 1);
    return true;
  }

  // EXPORT_KEYWORD | EXTERN_KEYWORD SINGLE_STRING_LITERAL?
  private static boolean topLevelVariableDeclaration_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "topLevelVariableDeclaration_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EXPORT_KEYWORD);
    if (!r) r = topLevelVariableDeclaration_0_0_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // EXTERN_KEYWORD SINGLE_STRING_LITERAL?
  private static boolean topLevelVariableDeclaration_0_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "topLevelVariableDeclaration_0_0_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EXTERN_KEYWORD);
    r = r && topLevelVariableDeclaration_0_0_1_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // SINGLE_STRING_LITERAL?
  private static boolean topLevelVariableDeclaration_0_0_1_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "topLevelVariableDeclaration_0_0_1_1")) return false;
    consumeToken(b, SINGLE_STRING_LITERAL);
    return true;
  }

  // THREADLOCAL_KEYWORD?
  private static boolean topLevelVariableDeclaration_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "topLevelVariableDeclaration_1")) return false;
    consumeToken(b, THREADLOCAL_KEYWORD);
    return true;
  }

  /* ********************************************************** */
  // prefixTypeOperation* errorUnionExpression
  public static boolean typeExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "typeExpression")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, TYPE_EXPRESSION, "<type expression>");
    r = typeExpression_0(b, l + 1);
    r = r && errorUnionExpression(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // prefixTypeOperation*
  private static boolean typeExpression_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "typeExpression_0")) return false;
    while (true) {
      int c = current_position_(b);
      if (!prefixTypeOperation(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "typeExpression_0", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // LEFT_BRACKET ASTERISK_SYMBOL sentinelTerminator? RIGHT_BRACKET
  public static boolean unknownLengthPointer(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "unknownLengthPointer")) return false;
    if (!nextTokenIs(b, LEFT_BRACKET)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, LEFT_BRACKET, ASTERISK_SYMBOL);
    r = r && unknownLengthPointer_2(b, l + 1);
    r = r && consumeToken(b, RIGHT_BRACKET);
    exit_section_(b, m, UNKNOWN_LENGTH_POINTER, r);
    return r;
  }

  // sentinelTerminator?
  private static boolean unknownLengthPointer_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "unknownLengthPointer_2")) return false;
    sentinelTerminator(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // (CONST_KEYWORD | VAR_KEYWORD) IDENTIFIER (COLON_SYMBOL typeExpression)? byteAlignment? linkSection? (ASSIGN_SYMBOL expression)? SEMICOLON_SYMBOL
  public static boolean variableDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variableDeclaration")) return false;
    if (!nextTokenIs(b, "<variable declaration>", CONST_KEYWORD, VAR_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, VARIABLE_DECLARATION, "<variable declaration>");
    r = variableDeclaration_0(b, l + 1);
    r = r && consumeToken(b, IDENTIFIER);
    r = r && variableDeclaration_2(b, l + 1);
    r = r && variableDeclaration_3(b, l + 1);
    r = r && variableDeclaration_4(b, l + 1);
    r = r && variableDeclaration_5(b, l + 1);
    r = r && consumeToken(b, SEMICOLON_SYMBOL);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // CONST_KEYWORD | VAR_KEYWORD
  private static boolean variableDeclaration_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variableDeclaration_0")) return false;
    boolean r;
    r = consumeToken(b, CONST_KEYWORD);
    if (!r) r = consumeToken(b, VAR_KEYWORD);
    return r;
  }

  // (COLON_SYMBOL typeExpression)?
  private static boolean variableDeclaration_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variableDeclaration_2")) return false;
    variableDeclaration_2_0(b, l + 1);
    return true;
  }

  // COLON_SYMBOL typeExpression
  private static boolean variableDeclaration_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variableDeclaration_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, COLON_SYMBOL);
    r = r && typeExpression(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // byteAlignment?
  private static boolean variableDeclaration_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variableDeclaration_3")) return false;
    byteAlignment(b, l + 1);
    return true;
  }

  // linkSection?
  private static boolean variableDeclaration_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variableDeclaration_4")) return false;
    linkSection(b, l + 1);
    return true;
  }

  // (ASSIGN_SYMBOL expression)?
  private static boolean variableDeclaration_5(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variableDeclaration_5")) return false;
    variableDeclaration_5_0(b, l + 1);
    return true;
  }

  // ASSIGN_SYMBOL expression
  private static boolean variableDeclaration_5_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variableDeclaration_5_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, ASSIGN_SYMBOL);
    r = r && expression(b, l + 1, -1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // COLON_SYMBOL LEFT_PARENTHESIS expression RIGHT_PARENTHESIS
  public static boolean whileContinueExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whileContinueExpression")) return false;
    if (!nextTokenIsFast(b, COLON_SYMBOL)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, COLON_SYMBOL, LEFT_PARENTHESIS);
    r = r && expression(b, l + 1, -1);
    r = r && consumeToken(b, RIGHT_PARENTHESIS);
    exit_section_(b, m, WHILE_CONTINUE_EXPRESSION, r);
    return r;
  }

  /* ********************************************************** */
  // whilePrefix expression (ELSE_KEYWORD payload? expression)?
  public static boolean whileExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whileExpression")) return false;
    if (!nextTokenIsFast(b, WHILE_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = whilePrefix(b, l + 1);
    r = r && expression(b, l + 1, -1);
    r = r && whileExpression_2(b, l + 1);
    exit_section_(b, m, WHILE_EXPRESSION, r);
    return r;
  }

  // (ELSE_KEYWORD payload? expression)?
  private static boolean whileExpression_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whileExpression_2")) return false;
    whileExpression_2_0(b, l + 1);
    return true;
  }

  // ELSE_KEYWORD payload? expression
  private static boolean whileExpression_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whileExpression_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenFast(b, ELSE_KEYWORD);
    r = r && whileExpression_2_0_1(b, l + 1);
    r = r && expression(b, l + 1, -1);
    exit_section_(b, m, null, r);
    return r;
  }

  // payload?
  private static boolean whileExpression_2_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whileExpression_2_0_1")) return false;
    payload(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // WHILE_KEYWORD LEFT_PARENTHESIS expression RIGHT_PARENTHESIS pointerPayload? whileContinueExpression?
  public static boolean whilePrefix(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whilePrefix")) return false;
    if (!nextTokenIs(b, WHILE_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, WHILE_KEYWORD, LEFT_PARENTHESIS);
    r = r && expression(b, l + 1, -1);
    r = r && consumeToken(b, RIGHT_PARENTHESIS);
    r = r && whilePrefix_4(b, l + 1);
    r = r && whilePrefix_5(b, l + 1);
    exit_section_(b, m, WHILE_PREFIX, r);
    return r;
  }

  // pointerPayload?
  private static boolean whilePrefix_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whilePrefix_4")) return false;
    pointerPayload(b, l + 1);
    return true;
  }

  // whileContinueExpression?
  private static boolean whilePrefix_5(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whilePrefix_5")) return false;
    whileContinueExpression(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // whilePrefix labeledBlockExpression (ELSE_KEYWORD payload? statement)?
  //                | whilePrefix expression (SEMICOLON_SYMBOL | ELSE_KEYWORD payload? statement)
  public static boolean whileStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whileStatement")) return false;
    if (!nextTokenIs(b, WHILE_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = whileStatement_0(b, l + 1);
    if (!r) r = whileStatement_1(b, l + 1);
    exit_section_(b, m, WHILE_STATEMENT, r);
    return r;
  }

  // whilePrefix labeledBlockExpression (ELSE_KEYWORD payload? statement)?
  private static boolean whileStatement_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whileStatement_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = whilePrefix(b, l + 1);
    r = r && labeledBlockExpression(b, l + 1);
    r = r && whileStatement_0_2(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (ELSE_KEYWORD payload? statement)?
  private static boolean whileStatement_0_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whileStatement_0_2")) return false;
    whileStatement_0_2_0(b, l + 1);
    return true;
  }

  // ELSE_KEYWORD payload? statement
  private static boolean whileStatement_0_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whileStatement_0_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, ELSE_KEYWORD);
    r = r && whileStatement_0_2_0_1(b, l + 1);
    r = r && statement(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // payload?
  private static boolean whileStatement_0_2_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whileStatement_0_2_0_1")) return false;
    payload(b, l + 1);
    return true;
  }

  // whilePrefix expression (SEMICOLON_SYMBOL | ELSE_KEYWORD payload? statement)
  private static boolean whileStatement_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whileStatement_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = whilePrefix(b, l + 1);
    r = r && expression(b, l + 1, -1);
    r = r && whileStatement_1_2(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // SEMICOLON_SYMBOL | ELSE_KEYWORD payload? statement
  private static boolean whileStatement_1_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whileStatement_1_2")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, SEMICOLON_SYMBOL);
    if (!r) r = whileStatement_1_2_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // ELSE_KEYWORD payload? statement
  private static boolean whileStatement_1_2_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whileStatement_1_2_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, ELSE_KEYWORD);
    r = r && whileStatement_1_2_1_1(b, l + 1);
    r = r && statement(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // payload?
  private static boolean whileStatement_1_2_1_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whileStatement_1_2_1_1")) return false;
    payload(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // whilePrefix typeExpression (ELSE_KEYWORD payload? typeExpression)?
  public static boolean whileTypeExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whileTypeExpression")) return false;
    if (!nextTokenIsFast(b, WHILE_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = whilePrefix(b, l + 1);
    r = r && typeExpression(b, l + 1);
    r = r && whileTypeExpression_2(b, l + 1);
    exit_section_(b, m, WHILE_TYPE_EXPRESSION, r);
    return r;
  }

  // (ELSE_KEYWORD payload? typeExpression)?
  private static boolean whileTypeExpression_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whileTypeExpression_2")) return false;
    whileTypeExpression_2_0(b, l + 1);
    return true;
  }

  // ELSE_KEYWORD payload? typeExpression
  private static boolean whileTypeExpression_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whileTypeExpression_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenFast(b, ELSE_KEYWORD);
    r = r && whileTypeExpression_2_0_1(b, l + 1);
    r = r && typeExpression(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // payload?
  private static boolean whileTypeExpression_2_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whileTypeExpression_2_0_1")) return false;
    payload(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // topLevelDeclarations
  static boolean zigFile(PsiBuilder b, int l) {
    return topLevelDeclarations(b, l + 1);
  }

  /* ********************************************************** */
  // Expression root: expression
  // Operator priority table:
  // 0: PREFIX(assemblyExpression)
  // 1: N_ARY(assignExpression) N_ARY(booleanOrExpression) N_ARY(booleanAndExpression) POSTFIX(compareExpression)
  //    N_ARY(bitwiseExpression) N_ARY(bitShiftExpression) N_ARY(additionExpression) N_ARY(multiplyExpression)
  // 2: PREFIX(tryExpression) ATOM(breakExpression) PREFIX(comptimeExpression) PREFIX(nosuspendExpression)
  //    ATOM(continueExpression) PREFIX(resumeExpression) ATOM(returnExpression)
  // 3: PREFIX(prefixExpression)
  // 4: ATOM(blockExpression)
  // 5: ATOM(primaryExpression)
  public static boolean expression(PsiBuilder b, int l, int g) {
    if (!recursion_guard_(b, l, "expression")) return false;
    addVariant(b, "<expression>");
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, "<expression>");
    r = assemblyExpression(b, l + 1);
    if (!r) r = tryExpression(b, l + 1);
    if (!r) r = breakExpression(b, l + 1);
    if (!r) r = comptimeExpression(b, l + 1);
    if (!r) r = nosuspendExpression(b, l + 1);
    if (!r) r = continueExpression(b, l + 1);
    if (!r) r = resumeExpression(b, l + 1);
    if (!r) r = returnExpression(b, l + 1);
    if (!r) r = prefixExpression(b, l + 1);
    if (!r) r = blockExpression(b, l + 1);
    if (!r) r = primaryExpression(b, l + 1);
    p = r;
    r = r && expression_0(b, l + 1, g);
    exit_section_(b, l, m, null, r, p, null);
    return r || p;
  }

  public static boolean expression_0(PsiBuilder b, int l, int g) {
    if (!recursion_guard_(b, l, "expression_0")) return false;
    boolean r = true;
    while (true) {
      Marker m = enter_section_(b, l, _LEFT_, null);
      if (g < 1 && assignOperation(b, l + 1)) {
        int c = current_position_(b);
        while (true) {
          r = report_error_(b, expression(b, l, 1));
          if (!assignOperation(b, l + 1)) break;
          if (!empty_element_parsed_guard_(b, "assignExpression", c)) break;
          c = current_position_(b);
        }
        exit_section_(b, l, m, ASSIGN_EXPRESSION, r, true, null);
      }
      else if (g < 1 && consumeTokenSmart(b, OR_KEYWORD)) {
        while (true) {
          r = report_error_(b, expression(b, l, 1));
          if (!consumeTokenSmart(b, OR_KEYWORD)) break;
        }
        exit_section_(b, l, m, BOOLEAN_OR_EXPRESSION, r, true, null);
      }
      else if (g < 1 && consumeTokenSmart(b, AND_KEYWORD)) {
        while (true) {
          r = report_error_(b, expression(b, l, 1));
          if (!consumeTokenSmart(b, AND_KEYWORD)) break;
        }
        exit_section_(b, l, m, BOOLEAN_AND_EXPRESSION, r, true, null);
      }
      else if (g < 1 && compareExpression_0(b, l + 1)) {
        r = true;
        exit_section_(b, l, m, COMPARE_EXPRESSION, r, true, null);
      }
      else if (g < 1 && bitwiseOperation(b, l + 1)) {
        int c = current_position_(b);
        while (true) {
          r = report_error_(b, expression(b, l, 1));
          if (!bitwiseOperation(b, l + 1)) break;
          if (!empty_element_parsed_guard_(b, "bitwiseExpression", c)) break;
          c = current_position_(b);
        }
        exit_section_(b, l, m, BITWISE_EXPRESSION, r, true, null);
      }
      else if (g < 1 && bitShiftOperation(b, l + 1)) {
        int c = current_position_(b);
        while (true) {
          r = report_error_(b, expression(b, l, 1));
          if (!bitShiftOperation(b, l + 1)) break;
          if (!empty_element_parsed_guard_(b, "bitShiftExpression", c)) break;
          c = current_position_(b);
        }
        exit_section_(b, l, m, BIT_SHIFT_EXPRESSION, r, true, null);
      }
      else if (g < 1 && additionOperation(b, l + 1)) {
        int c = current_position_(b);
        while (true) {
          r = report_error_(b, expression(b, l, 1));
          if (!additionOperation(b, l + 1)) break;
          if (!empty_element_parsed_guard_(b, "additionExpression", c)) break;
          c = current_position_(b);
        }
        exit_section_(b, l, m, ADDITION_EXPRESSION, r, true, null);
      }
      else if (g < 1 && multiplyOperation(b, l + 1)) {
        int c = current_position_(b);
        while (true) {
          r = report_error_(b, expression(b, l, 1));
          if (!multiplyOperation(b, l + 1)) break;
          if (!empty_element_parsed_guard_(b, "multiplyExpression", c)) break;
          c = current_position_(b);
        }
        exit_section_(b, l, m, MULTIPLY_EXPRESSION, r, true, null);
      }
      else {
        exit_section_(b, l, m, null, false, false, null);
        break;
      }
    }
    return r;
  }

  public static boolean assemblyExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyExpression")) return false;
    if (!nextTokenIsSmart(b, ASSEMBLY_KEYWORD)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, null);
    r = assemblyExpression_0(b, l + 1);
    p = r;
    r = p && expression(b, l, 0);
    r = p && report_error_(b, assemblyExpression_1(b, l + 1)) && r;
    exit_section_(b, l, m, ASSEMBLY_EXPRESSION, r, p, null);
    return r || p;
  }

  // ASSEMBLY_KEYWORD VOLATILE_KEYWORD? LEFT_PARENTHESIS
  private static boolean assemblyExpression_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyExpression_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenSmart(b, ASSEMBLY_KEYWORD);
    r = r && assemblyExpression_0_1(b, l + 1);
    r = r && consumeToken(b, LEFT_PARENTHESIS);
    exit_section_(b, m, null, r);
    return r;
  }

  // VOLATILE_KEYWORD?
  private static boolean assemblyExpression_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyExpression_0_1")) return false;
    consumeTokenSmart(b, VOLATILE_KEYWORD);
    return true;
  }

  // assemblyOutput? RIGHT_PARENTHESIS
  private static boolean assemblyExpression_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyExpression_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = assemblyExpression_1_0(b, l + 1);
    r = r && consumeToken(b, RIGHT_PARENTHESIS);
    exit_section_(b, m, null, r);
    return r;
  }

  // assemblyOutput?
  private static boolean assemblyExpression_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assemblyExpression_1_0")) return false;
    assemblyOutput(b, l + 1);
    return true;
  }

  // compareOperation expression
  private static boolean compareExpression_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "compareExpression_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = compareOperation(b, l + 1);
    r = r && expression(b, l + 1, -1);
    exit_section_(b, m, null, r);
    return r;
  }

  public static boolean tryExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "tryExpression")) return false;
    if (!nextTokenIsSmart(b, TRY_KEYWORD)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, null);
    r = consumeTokenSmart(b, TRY_KEYWORD);
    p = r;
    r = p && expression(b, l, 2);
    exit_section_(b, l, m, TRY_EXPRESSION, r, p, null);
    return r || p;
  }

  // BREAK_KEYWORD breakLabel? expression?
  public static boolean breakExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "breakExpression")) return false;
    if (!nextTokenIsSmart(b, BREAK_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenSmart(b, BREAK_KEYWORD);
    r = r && breakExpression_1(b, l + 1);
    r = r && breakExpression_2(b, l + 1);
    exit_section_(b, m, BREAK_EXPRESSION, r);
    return r;
  }

  // breakLabel?
  private static boolean breakExpression_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "breakExpression_1")) return false;
    breakLabel(b, l + 1);
    return true;
  }

  // expression?
  private static boolean breakExpression_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "breakExpression_2")) return false;
    expression(b, l + 1, -1);
    return true;
  }

  public static boolean comptimeExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "comptimeExpression")) return false;
    if (!nextTokenIsSmart(b, COMPTIME_KEYWORD)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, null);
    r = consumeTokenSmart(b, COMPTIME_KEYWORD);
    p = r;
    r = p && expression(b, l, 2);
    exit_section_(b, l, m, COMPTIME_EXPRESSION, r, p, null);
    return r || p;
  }

  public static boolean nosuspendExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "nosuspendExpression")) return false;
    if (!nextTokenIsSmart(b, NOSUSPEND_KEYWORD)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, null);
    r = consumeTokenSmart(b, NOSUSPEND_KEYWORD);
    p = r;
    r = p && expression(b, l, 2);
    exit_section_(b, l, m, NOSUSPEND_EXPRESSION, r, p, null);
    return r || p;
  }

  // CONTINUE_KEYWORD breakLabel?
  public static boolean continueExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "continueExpression")) return false;
    if (!nextTokenIsSmart(b, CONTINUE_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenSmart(b, CONTINUE_KEYWORD);
    r = r && continueExpression_1(b, l + 1);
    exit_section_(b, m, CONTINUE_EXPRESSION, r);
    return r;
  }

  // breakLabel?
  private static boolean continueExpression_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "continueExpression_1")) return false;
    breakLabel(b, l + 1);
    return true;
  }

  public static boolean resumeExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "resumeExpression")) return false;
    if (!nextTokenIsSmart(b, RESUME_KEYWORD)) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, null);
    r = consumeTokenSmart(b, RESUME_KEYWORD);
    p = r;
    r = p && expression(b, l, 2);
    exit_section_(b, l, m, RESUME_EXPRESSION, r, p, null);
    return r || p;
  }

  // RETURN_KEYWORD expression?
  public static boolean returnExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "returnExpression")) return false;
    if (!nextTokenIsSmart(b, RETURN_KEYWORD)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenSmart(b, RETURN_KEYWORD);
    r = r && returnExpression_1(b, l + 1);
    exit_section_(b, m, RETURN_EXPRESSION, r);
    return r;
  }

  // expression?
  private static boolean returnExpression_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "returnExpression_1")) return false;
    expression(b, l + 1, -1);
    return true;
  }

  public static boolean prefixExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "prefixExpression")) return false;
    boolean r, p;
    Marker m = enter_section_(b, l, _NONE_, null);
    r = prefixExpression_0(b, l + 1);
    p = r;
    r = p && expression(b, l, 3);
    exit_section_(b, l, m, PREFIX_EXPRESSION, r, p, null);
    return r || p;
  }

  // prefixOperation+
  private static boolean prefixExpression_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "prefixExpression_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = prefixOperation(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!prefixOperation(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "prefixExpression_0", c)) break;
    }
    exit_section_(b, m, null, r);
    return r;
  }

  // LEFT_BRACE statement* RIGHT_BRACE
  public static boolean blockExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "blockExpression")) return false;
    if (!nextTokenIsSmart(b, LEFT_BRACE)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokenSmart(b, LEFT_BRACE);
    r = r && blockExpression_1(b, l + 1);
    r = r && consumeToken(b, RIGHT_BRACE);
    exit_section_(b, m, BLOCK_EXPRESSION, r);
    return r;
  }

  // statement*
  private static boolean blockExpression_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "blockExpression_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!statement(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "blockExpression_1", c)) break;
    }
    return true;
  }

  // ControlFlowExpression
  //                   | curlySuffixExpression
  public static boolean primaryExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "primaryExpression")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, PRIMARY_EXPRESSION, "<primary expression>");
    r = ControlFlowExpression(b, l + 1);
    if (!r) r = curlySuffixExpression(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

}
