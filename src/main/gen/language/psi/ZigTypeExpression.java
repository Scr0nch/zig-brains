// This is a generated file. Not intended for manual editing.
package language.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface ZigTypeExpression extends ZigExpression {

  @NotNull
  List<ZigArrayTypeStart> getArrayTypeStartList();

  @NotNull
  List<ZigByteAlignment> getByteAlignmentList();

  @NotNull
  List<ZigExpression> getExpressionList();

  @NotNull
  List<ZigPointerTypeStart> getPointerTypeStartList();

  @NotNull
  List<ZigSliceTypeStart> getSliceTypeStartList();

}
