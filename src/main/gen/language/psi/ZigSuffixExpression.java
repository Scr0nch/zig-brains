// This is a generated file. Not intended for manual editing.
package language.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface ZigSuffixExpression extends ZigExpression {

  @NotNull
  List<ZigExpression> getExpressionList();

  @NotNull
  List<ZigFunctionCallArguments> getFunctionCallArgumentsList();

  @NotNull
  List<ZigSentinelTerminator> getSentinelTerminatorList();

}
