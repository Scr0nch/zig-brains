// This is a generated file. Not intended for manual editing.
package language.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface ZigContainerDeclaration extends PsiElement {

  @NotNull
  ZigContainerDeclarationType getContainerDeclarationType();

  @NotNull
  List<ZigContainerField> getContainerFieldList();

  @NotNull
  List<ZigFunctionDeclaration> getFunctionDeclarationList();

  @NotNull
  List<ZigImportDeclaration> getImportDeclarationList();

  @NotNull
  List<ZigTestDeclaration> getTestDeclarationList();

  @NotNull
  List<ZigTopLevelComptimeBlock> getTopLevelComptimeBlockList();

  @NotNull
  List<ZigTopLevelVariableDeclaration> getTopLevelVariableDeclarationList();

}
