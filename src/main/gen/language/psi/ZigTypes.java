// This is a generated file. Not intended for manual editing.
package language.psi;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import language.psi.impl.*;

public interface ZigTypes {

  IElementType ADDITION_EXPRESSION = new ZigElementType("ADDITION_EXPRESSION");
  IElementType ARRAY_TYPE_START = new ZigElementType("ARRAY_TYPE_START");
  IElementType ASSEMBLY_CLOBBERS = new ZigElementType("ASSEMBLY_CLOBBERS");
  IElementType ASSEMBLY_EXPRESSION = new ZigElementType("ASSEMBLY_EXPRESSION");
  IElementType ASSEMBLY_INPUT = new ZigElementType("ASSEMBLY_INPUT");
  IElementType ASSEMBLY_INPUT_ITEM = new ZigElementType("ASSEMBLY_INPUT_ITEM");
  IElementType ASSEMBLY_INPUT_LIST = new ZigElementType("ASSEMBLY_INPUT_LIST");
  IElementType ASSEMBLY_OUTPUT = new ZigElementType("ASSEMBLY_OUTPUT");
  IElementType ASSEMBLY_OUTPUT_ITEM = new ZigElementType("ASSEMBLY_OUTPUT_ITEM");
  IElementType ASSEMBLY_OUTPUT_LIST = new ZigElementType("ASSEMBLY_OUTPUT_LIST");
  IElementType ASSIGN_EXPRESSION = new ZigElementType("ASSIGN_EXPRESSION");
  IElementType BITWISE_EXPRESSION = new ZigElementType("BITWISE_EXPRESSION");
  IElementType BIT_SHIFT_EXPRESSION = new ZigElementType("BIT_SHIFT_EXPRESSION");
  IElementType BLOCK_EXPRESSION = new ZigElementType("BLOCK_EXPRESSION");
  IElementType BLOCK_EXPRESSION_STATEMENT = new ZigElementType("BLOCK_EXPRESSION_STATEMENT");
  IElementType BLOCK_LABEL = new ZigElementType("BLOCK_LABEL");
  IElementType BOOLEAN_AND_EXPRESSION = new ZigElementType("BOOLEAN_AND_EXPRESSION");
  IElementType BOOLEAN_OR_EXPRESSION = new ZigElementType("BOOLEAN_OR_EXPRESSION");
  IElementType BREAK_EXPRESSION = new ZigElementType("BREAK_EXPRESSION");
  IElementType BREAK_LABEL = new ZigElementType("BREAK_LABEL");
  IElementType BYTE_ALIGNMENT = new ZigElementType("BYTE_ALIGNMENT");
  IElementType CALLING_CONVECTION_SECTION = new ZigElementType("CALLING_CONVECTION_SECTION");
  IElementType COMPARE_EXPRESSION = new ZigElementType("COMPARE_EXPRESSION");
  IElementType COMPTIME_EXPRESSION = new ZigElementType("COMPTIME_EXPRESSION");
  IElementType CONTAINER_DECLARATION = new ZigElementType("CONTAINER_DECLARATION");
  IElementType CONTAINER_DECLARATION_TYPE = new ZigElementType("CONTAINER_DECLARATION_TYPE");
  IElementType CONTAINER_FIELD = new ZigElementType("CONTAINER_FIELD");
  IElementType CONTINUE_EXPRESSION = new ZigElementType("CONTINUE_EXPRESSION");
  IElementType CURLY_SUFFIX_EXPRESSION = new ZigElementType("CURLY_SUFFIX_EXPRESSION");
  IElementType ERROR_SET_DECLARATION = new ZigElementType("ERROR_SET_DECLARATION");
  IElementType ERROR_UNION_EXPRESSION = new ZigElementType("ERROR_UNION_EXPRESSION");
  IElementType EXPRESSION = new ZigElementType("EXPRESSION");
  IElementType EXPRESSION_LIST = new ZigElementType("EXPRESSION_LIST");
  IElementType FIELD_INITIALIZER = new ZigElementType("FIELD_INITIALIZER");
  IElementType FOR_EXPRESSION = new ZigElementType("FOR_EXPRESSION");
  IElementType FOR_PREFIX = new ZigElementType("FOR_PREFIX");
  IElementType FOR_STATEMENT = new ZigElementType("FOR_STATEMENT");
  IElementType FOR_TYPE_EXPRESSION = new ZigElementType("FOR_TYPE_EXPRESSION");
  IElementType FUNCTION_CALL_ARGUMENTS = new ZigElementType("FUNCTION_CALL_ARGUMENTS");
  IElementType FUNCTION_DECLARATION = new ZigElementType("FUNCTION_DECLARATION");
  IElementType FUNCTION_DECLARATION_PROTOTYPE = new ZigElementType("FUNCTION_DECLARATION_PROTOTYPE");
  IElementType FUNCTION_PROTOTYPE = new ZigElementType("FUNCTION_PROTOTYPE");
  IElementType GROUPED_EXPRESSION = new ZigElementType("GROUPED_EXPRESSION");
  IElementType IDENTIFIER_LIST = new ZigElementType("IDENTIFIER_LIST");
  IElementType IF_EXPRESSION = new ZigElementType("IF_EXPRESSION");
  IElementType IF_PREFIX = new ZigElementType("IF_PREFIX");
  IElementType IF_STATEMENT = new ZigElementType("IF_STATEMENT");
  IElementType IF_TYPE_EXPRESSION = new ZigElementType("IF_TYPE_EXPRESSION");
  IElementType IMPORT_DECLARATION = new ZigElementType("IMPORT_DECLARATION");
  IElementType INITIALIZER_LIST = new ZigElementType("INITIALIZER_LIST");
  IElementType LABELED_BLOCK_EXPRESSION = new ZigElementType("LABELED_BLOCK_EXPRESSION");
  IElementType LABELED_BLOCK_OR_LOOP_STATEMENT = new ZigElementType("LABELED_BLOCK_OR_LOOP_STATEMENT");
  IElementType LABELED_LOOP_EXPRESSION = new ZigElementType("LABELED_LOOP_EXPRESSION");
  IElementType LABELED_TYPE_EXPRESSION = new ZigElementType("LABELED_TYPE_EXPRESSION");
  IElementType LINK_SECTION = new ZigElementType("LINK_SECTION");
  IElementType LOOP_EXPRESSION = new ZigElementType("LOOP_EXPRESSION");
  IElementType LOOP_STATEMENT = new ZigElementType("LOOP_STATEMENT");
  IElementType LOOP_TYPE_EXPRESSION = new ZigElementType("LOOP_TYPE_EXPRESSION");
  IElementType MULTIPLY_EXPRESSION = new ZigElementType("MULTIPLY_EXPRESSION");
  IElementType NOSUSPEND_EXPRESSION = new ZigElementType("NOSUSPEND_EXPRESSION");
  IElementType PARAMETER_DECLARATION = new ZigElementType("PARAMETER_DECLARATION");
  IElementType PARAMETER_DECLARATION_LIST = new ZigElementType("PARAMETER_DECLARATION_LIST");
  IElementType PARAMETER_TYPE = new ZigElementType("PARAMETER_TYPE");
  IElementType PAYLOAD = new ZigElementType("PAYLOAD");
  IElementType POINTER_INDEX_PAYLOAD = new ZigElementType("POINTER_INDEX_PAYLOAD");
  IElementType POINTER_PAYLOAD = new ZigElementType("POINTER_PAYLOAD");
  IElementType POINTER_TYPE_START = new ZigElementType("POINTER_TYPE_START");
  IElementType PREFIX_EXPRESSION = new ZigElementType("PREFIX_EXPRESSION");
  IElementType PRIMARY_EXPRESSION = new ZigElementType("PRIMARY_EXPRESSION");
  IElementType PRIMARY_TYPE_EXPRESSION = new ZigElementType("PRIMARY_TYPE_EXPRESSION");
  IElementType RESUME_EXPRESSION = new ZigElementType("RESUME_EXPRESSION");
  IElementType RETURN_EXPRESSION = new ZigElementType("RETURN_EXPRESSION");
  IElementType SENTINEL_TERMINATOR = new ZigElementType("SENTINEL_TERMINATOR");
  IElementType SLICE_TYPE_START = new ZigElementType("SLICE_TYPE_START");
  IElementType STATEMENT = new ZigElementType("STATEMENT");
  IElementType STRING_LIST = new ZigElementType("STRING_LIST");
  IElementType STRING_LITERAL = new ZigElementType("STRING_LITERAL");
  IElementType SUFFIX_EXPRESSION = new ZigElementType("SUFFIX_EXPRESSION");
  IElementType SWITCH_CASE = new ZigElementType("SWITCH_CASE");
  IElementType SWITCH_EXPRESSION = new ZigElementType("SWITCH_EXPRESSION");
  IElementType SWITCH_ITEM = new ZigElementType("SWITCH_ITEM");
  IElementType SWITCH_PRONG = new ZigElementType("SWITCH_PRONG");
  IElementType SWITCH_PRONG_LIST = new ZigElementType("SWITCH_PRONG_LIST");
  IElementType TEST_DECLARATION = new ZigElementType("TEST_DECLARATION");
  IElementType TOP_LEVEL_COMPTIME_BLOCK = new ZigElementType("TOP_LEVEL_COMPTIME_BLOCK");
  IElementType TOP_LEVEL_VARIABLE_DECLARATION = new ZigElementType("TOP_LEVEL_VARIABLE_DECLARATION");
  IElementType TRY_EXPRESSION = new ZigElementType("TRY_EXPRESSION");
  IElementType TYPE_EXPRESSION = new ZigElementType("TYPE_EXPRESSION");
  IElementType UNKNOWN_LENGTH_POINTER = new ZigElementType("UNKNOWN_LENGTH_POINTER");
  IElementType VARIABLE_DECLARATION = new ZigElementType("VARIABLE_DECLARATION");
  IElementType WHILE_CONTINUE_EXPRESSION = new ZigElementType("WHILE_CONTINUE_EXPRESSION");
  IElementType WHILE_EXPRESSION = new ZigElementType("WHILE_EXPRESSION");
  IElementType WHILE_PREFIX = new ZigElementType("WHILE_PREFIX");
  IElementType WHILE_STATEMENT = new ZigElementType("WHILE_STATEMENT");
  IElementType WHILE_TYPE_EXPRESSION = new ZigElementType("WHILE_TYPE_EXPRESSION");

  IElementType ADD_ASSIGN_SYMBOL = new ZigTokenType("ADD_ASSIGN_SYMBOL");
  IElementType ADD_OVERFLOW_ASSIGN_SYMBOL = new ZigTokenType("ADD_OVERFLOW_ASSIGN_SYMBOL");
  IElementType ADD_OVERFLOW_SYMBOL = new ZigTokenType("ADD_OVERFLOW_SYMBOL");
  IElementType ADD_SYMBOL = new ZigTokenType("ADD_SYMBOL");
  IElementType ALIGN_KEYWORD = new ZigTokenType("ALIGN_KEYWORD");
  IElementType ALLOWZERO_KEYWORD = new ZigTokenType("ALLOWZERO_KEYWORD");
  IElementType AMPERSAND_SYMBOL = new ZigTokenType("AMPERSAND_SYMBOL");
  IElementType AND_KEYWORD = new ZigTokenType("AND_KEYWORD");
  IElementType ANYFRAME_KEYWORD = new ZigTokenType("ANYFRAME_KEYWORD");
  IElementType ANYTYPE_KEYWORD = new ZigTokenType("ANYTYPE_KEYWORD");
  IElementType ASSEMBLY_KEYWORD = new ZigTokenType("ASSEMBLY_KEYWORD");
  IElementType ASSIGN_SYMBOL = new ZigTokenType("ASSIGN_SYMBOL");
  IElementType ASTERISK_SYMBOL = new ZigTokenType("ASTERISK_SYMBOL");
  IElementType ASYNC_KEYWORD = new ZigTokenType("ASYNC_KEYWORD");
  IElementType AWAIT_KEYWORD = new ZigTokenType("AWAIT_KEYWORD");
  IElementType BITWISE_AND_ASSIGN_SYMBOL = new ZigTokenType("BITWISE_AND_ASSIGN_SYMBOL");
  IElementType BITWISE_NOT_SYMBOL = new ZigTokenType("BITWISE_NOT_SYMBOL");
  IElementType BITWISE_OR_ASSIGN_SYMBOL = new ZigTokenType("BITWISE_OR_ASSIGN_SYMBOL");
  IElementType BREAK_KEYWORD = new ZigTokenType("BREAK_KEYWORD");
  IElementType BUILTIN_IDENTIFIER = new ZigTokenType("BUILTIN_IDENTIFIER");
  IElementType CALLCONV_KEYWORD = new ZigTokenType("CALLCONV_KEYWORD");
  IElementType CARET_SYMBOL = new ZigTokenType("CARET_SYMBOL");
  IElementType CATCH_KEYWORD = new ZigTokenType("CATCH_KEYWORD");
  IElementType CHARACTER_LITERAL = new ZigTokenType("CHARACTER_LITERAL");
  IElementType COLON_SYMBOL = new ZigTokenType("COLON_SYMBOL");
  IElementType COMMA_SYMBOL = new ZigTokenType("COMMA_SYMBOL");
  IElementType COMPTIME_KEYWORD = new ZigTokenType("COMPTIME_KEYWORD");
  IElementType CONCATENATE_ARRAY_SYMBOL = new ZigTokenType("CONCATENATE_ARRAY_SYMBOL");
  IElementType CONST_KEYWORD = new ZigTokenType("CONST_KEYWORD");
  IElementType CONTINUE_KEYWORD = new ZigTokenType("CONTINUE_KEYWORD");
  IElementType C_POINTER_SYMBOL = new ZigTokenType("C_POINTER_SYMBOL");
  IElementType DEFER_KEYWORD = new ZigTokenType("DEFER_KEYWORD");
  IElementType DEREFERENCE_POINTER_SYMBOL = new ZigTokenType("DEREFERENCE_POINTER_SYMBOL");
  IElementType DIVIDE_ASSIGN_SYMBOL = new ZigTokenType("DIVIDE_ASSIGN_SYMBOL");
  IElementType DIVIDE_SYMBOL = new ZigTokenType("DIVIDE_SYMBOL");
  IElementType DOCUMENTATION_COMMENT = new ZigTokenType("DOCUMENTATION_COMMENT");
  IElementType DOT_SYMBOL = new ZigTokenType("DOT_SYMBOL");
  IElementType DOUBLE_ASTERISK_SYMBOL = new ZigTokenType("DOUBLE_ASTERISK_SYMBOL");
  IElementType ELSE_KEYWORD = new ZigTokenType("ELSE_KEYWORD");
  IElementType ENUM_KEYWORD = new ZigTokenType("ENUM_KEYWORD");
  IElementType EQUAL_SYMBOL = new ZigTokenType("EQUAL_SYMBOL");
  IElementType ERRDEFER_KEYWORD = new ZigTokenType("ERRDEFER_KEYWORD");
  IElementType ERROR_KEYWORD = new ZigTokenType("ERROR_KEYWORD");
  IElementType EXCLAMATIONMARK_SYMBOL = new ZigTokenType("EXCLAMATIONMARK_SYMBOL");
  IElementType EXPONENTIATE_ASSIGN_SYMBOL = new ZigTokenType("EXPONENTIATE_ASSIGN_SYMBOL");
  IElementType EXPORT_KEYWORD = new ZigTokenType("EXPORT_KEYWORD");
  IElementType EXTERN_KEYWORD = new ZigTokenType("EXTERN_KEYWORD");
  IElementType FALSE_KEYWORD = new ZigTokenType("FALSE_KEYWORD");
  IElementType FLOAT_LITERAL = new ZigTokenType("FLOAT_LITERAL");
  IElementType FN_KEYWORD = new ZigTokenType("FN_KEYWORD");
  IElementType FOR_KEYWORD = new ZigTokenType("FOR_KEYWORD");
  IElementType GREATER_THAN_OR_EQUAL_SYMBOL = new ZigTokenType("GREATER_THAN_OR_EQUAL_SYMBOL");
  IElementType GREATER_THAN_SYMBOL = new ZigTokenType("GREATER_THAN_SYMBOL");
  IElementType IDENTIFIER = new ZigTokenType("IDENTIFIER");
  IElementType IF_KEYWORD = new ZigTokenType("IF_KEYWORD");
  IElementType INLINE_KEYWORD = new ZigTokenType("INLINE_KEYWORD");
  IElementType INTEGER_LITERAL = new ZigTokenType("INTEGER_LITERAL");
  IElementType LEFT_BIT_SHIFT_ASSIGN_SYMBOL = new ZigTokenType("LEFT_BIT_SHIFT_ASSIGN_SYMBOL");
  IElementType LEFT_BIT_SHIFT_SYMBOL = new ZigTokenType("LEFT_BIT_SHIFT_SYMBOL");
  IElementType LEFT_BRACE = new ZigTokenType("LEFT_BRACE");
  IElementType LEFT_BRACKET = new ZigTokenType("LEFT_BRACKET");
  IElementType LEFT_PARENTHESIS = new ZigTokenType("LEFT_PARENTHESIS");
  IElementType LESS_THAN_OR_EQUAL_SYMBOL = new ZigTokenType("LESS_THAN_OR_EQUAL_SYMBOL");
  IElementType LESS_THAN_SYMBOL = new ZigTokenType("LESS_THAN_SYMBOL");
  IElementType LINE_STRING_LITERAL = new ZigTokenType("LINE_STRING_LITERAL");
  IElementType LINKSECTION_KEYWORD = new ZigTokenType("LINKSECTION_KEYWORD");
  IElementType MERGE_ERROR_SET_SYMBOL = new ZigTokenType("MERGE_ERROR_SET_SYMBOL");
  IElementType MINUS_PERCENT_SYMBOL = new ZigTokenType("MINUS_PERCENT_SYMBOL");
  IElementType MINUS_RIGHT_ARROW = new ZigTokenType("MINUS_RIGHT_ARROW");
  IElementType MINUS_SYMBOL = new ZigTokenType("MINUS_SYMBOL");
  IElementType MODULUS_ASSIGN_SYMBOL = new ZigTokenType("MODULUS_ASSIGN_SYMBOL");
  IElementType MODULUS_SYMBOL = new ZigTokenType("MODULUS_SYMBOL");
  IElementType MULTIPLY_ASSIGN_SYMBOL = new ZigTokenType("MULTIPLY_ASSIGN_SYMBOL");
  IElementType MULTIPLY_OVERFLOW_ASSIGN_SYMBOL = new ZigTokenType("MULTIPLY_OVERFLOW_ASSIGN_SYMBOL");
  IElementType MULTIPLY_OVERFLOW_SYMBOL = new ZigTokenType("MULTIPLY_OVERFLOW_SYMBOL");
  IElementType NOALIAS_KEYWORD = new ZigTokenType("NOALIAS_KEYWORD");
  IElementType NOINLINE_KEYWORD = new ZigTokenType("NOINLINE_KEYWORD");
  IElementType NOSUSPEND_KEYWORD = new ZigTokenType("NOSUSPEND_KEYWORD");
  IElementType NOT_EQUAL_SYMBOL = new ZigTokenType("NOT_EQUAL_SYMBOL");
  IElementType NULL_KEYWORD = new ZigTokenType("NULL_KEYWORD");
  IElementType OPAQUE_KEYWORD = new ZigTokenType("OPAQUE_KEYWORD");
  IElementType OPTIONAL_SYMBOL = new ZigTokenType("OPTIONAL_SYMBOL");
  IElementType ORELSE_KEYWORD = new ZigTokenType("ORELSE_KEYWORD");
  IElementType OR_KEYWORD = new ZigTokenType("OR_KEYWORD");
  IElementType PACKED_KEYWORD = new ZigTokenType("PACKED_KEYWORD");
  IElementType PIPE_SYMBOL = new ZigTokenType("PIPE_SYMBOL");
  IElementType PUB_KEYWORD = new ZigTokenType("PUB_KEYWORD");
  IElementType RANGE_SYMBOL = new ZigTokenType("RANGE_SYMBOL");
  IElementType RESUME_KEYWORD = new ZigTokenType("RESUME_KEYWORD");
  IElementType RETURN_KEYWORD = new ZigTokenType("RETURN_KEYWORD");
  IElementType RIGHT_BIT_SHIFT_ASSIGN_SYMBOL = new ZigTokenType("RIGHT_BIT_SHIFT_ASSIGN_SYMBOL");
  IElementType RIGHT_BIT_SHIFT_SYMBOL = new ZigTokenType("RIGHT_BIT_SHIFT_SYMBOL");
  IElementType RIGHT_BRACE = new ZigTokenType("RIGHT_BRACE");
  IElementType RIGHT_BRACKET = new ZigTokenType("RIGHT_BRACKET");
  IElementType RIGHT_PARENTHESIS = new ZigTokenType("RIGHT_PARENTHESIS");
  IElementType SEMICOLON_SYMBOL = new ZigTokenType("SEMICOLON_SYMBOL");
  IElementType SINGLE_STRING_LITERAL = new ZigTokenType("SINGLE_STRING_LITERAL");
  IElementType SLICE_SYMBOL = new ZigTokenType("SLICE_SYMBOL");
  IElementType STRUCT_KEYWORD = new ZigTokenType("STRUCT_KEYWORD");
  IElementType SUBTRACT_ASSIGN_SYMBOL = new ZigTokenType("SUBTRACT_ASSIGN_SYMBOL");
  IElementType SUBTRACT_OVERFLOW_ASSIGN_SYMBOL = new ZigTokenType("SUBTRACT_OVERFLOW_ASSIGN_SYMBOL");
  IElementType SUSPEND_KEYWORD = new ZigTokenType("SUSPEND_KEYWORD");
  IElementType SWITCH_KEYWORD = new ZigTokenType("SWITCH_KEYWORD");
  IElementType SWITCH_PRONG_SYMBOL = new ZigTokenType("SWITCH_PRONG_SYMBOL");
  IElementType TEST_KEYWORD = new ZigTokenType("TEST_KEYWORD");
  IElementType THREADLOCAL_KEYWORD = new ZigTokenType("THREADLOCAL_KEYWORD");
  IElementType TOP_LEVEL_DOCUMENTATION_COMMENT = new ZigTokenType("TOP_LEVEL_DOCUMENTATION_COMMENT");
  IElementType TRUE_KEYWORD = new ZigTokenType("TRUE_KEYWORD");
  IElementType TRY_KEYWORD = new ZigTokenType("TRY_KEYWORD");
  IElementType UNDEFINED_KEYWORD = new ZigTokenType("UNDEFINED_KEYWORD");
  IElementType UNION_KEYWORD = new ZigTokenType("UNION_KEYWORD");
  IElementType UNREACHABLE_KEYWORD = new ZigTokenType("UNREACHABLE_KEYWORD");
  IElementType UNWRAP_OPTIONAL_SYMBOL = new ZigTokenType("UNWRAP_OPTIONAL_SYMBOL");
  IElementType USINGNAMESPACE_KEYWORD = new ZigTokenType("USINGNAMESPACE_KEYWORD");
  IElementType VAR_KEYWORD = new ZigTokenType("VAR_KEYWORD");
  IElementType VOLATILE_KEYWORD = new ZigTokenType("VOLATILE_KEYWORD");
  IElementType WHILE_KEYWORD = new ZigTokenType("WHILE_KEYWORD");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
      if (type == ADDITION_EXPRESSION) {
        return new ZigAdditionExpressionImpl(node);
      }
      else if (type == ARRAY_TYPE_START) {
        return new ZigArrayTypeStartImpl(node);
      }
      else if (type == ASSEMBLY_CLOBBERS) {
        return new ZigAssemblyClobbersImpl(node);
      }
      else if (type == ASSEMBLY_EXPRESSION) {
        return new ZigAssemblyExpressionImpl(node);
      }
      else if (type == ASSEMBLY_INPUT) {
        return new ZigAssemblyInputImpl(node);
      }
      else if (type == ASSEMBLY_INPUT_ITEM) {
        return new ZigAssemblyInputItemImpl(node);
      }
      else if (type == ASSEMBLY_INPUT_LIST) {
        return new ZigAssemblyInputListImpl(node);
      }
      else if (type == ASSEMBLY_OUTPUT) {
        return new ZigAssemblyOutputImpl(node);
      }
      else if (type == ASSEMBLY_OUTPUT_ITEM) {
        return new ZigAssemblyOutputItemImpl(node);
      }
      else if (type == ASSEMBLY_OUTPUT_LIST) {
        return new ZigAssemblyOutputListImpl(node);
      }
      else if (type == ASSIGN_EXPRESSION) {
        return new ZigAssignExpressionImpl(node);
      }
      else if (type == BITWISE_EXPRESSION) {
        return new ZigBitwiseExpressionImpl(node);
      }
      else if (type == BIT_SHIFT_EXPRESSION) {
        return new ZigBitShiftExpressionImpl(node);
      }
      else if (type == BLOCK_EXPRESSION) {
        return new ZigBlockExpressionImpl(node);
      }
      else if (type == BLOCK_EXPRESSION_STATEMENT) {
        return new ZigBlockExpressionStatementImpl(node);
      }
      else if (type == BLOCK_LABEL) {
        return new ZigBlockLabelImpl(node);
      }
      else if (type == BOOLEAN_AND_EXPRESSION) {
        return new ZigBooleanAndExpressionImpl(node);
      }
      else if (type == BOOLEAN_OR_EXPRESSION) {
        return new ZigBooleanOrExpressionImpl(node);
      }
      else if (type == BREAK_EXPRESSION) {
        return new ZigBreakExpressionImpl(node);
      }
      else if (type == BREAK_LABEL) {
        return new ZigBreakLabelImpl(node);
      }
      else if (type == BYTE_ALIGNMENT) {
        return new ZigByteAlignmentImpl(node);
      }
      else if (type == CALLING_CONVECTION_SECTION) {
        return new ZigCallingConvectionSectionImpl(node);
      }
      else if (type == COMPARE_EXPRESSION) {
        return new ZigCompareExpressionImpl(node);
      }
      else if (type == COMPTIME_EXPRESSION) {
        return new ZigComptimeExpressionImpl(node);
      }
      else if (type == CONTAINER_DECLARATION) {
        return new ZigContainerDeclarationImpl(node);
      }
      else if (type == CONTAINER_DECLARATION_TYPE) {
        return new ZigContainerDeclarationTypeImpl(node);
      }
      else if (type == CONTAINER_FIELD) {
        return new ZigContainerFieldImpl(node);
      }
      else if (type == CONTINUE_EXPRESSION) {
        return new ZigContinueExpressionImpl(node);
      }
      else if (type == CURLY_SUFFIX_EXPRESSION) {
        return new ZigCurlySuffixExpressionImpl(node);
      }
      else if (type == ERROR_SET_DECLARATION) {
        return new ZigErrorSetDeclarationImpl(node);
      }
      else if (type == ERROR_UNION_EXPRESSION) {
        return new ZigErrorUnionExpressionImpl(node);
      }
      else if (type == EXPRESSION_LIST) {
        return new ZigExpressionListImpl(node);
      }
      else if (type == FIELD_INITIALIZER) {
        return new ZigFieldInitializerImpl(node);
      }
      else if (type == FOR_EXPRESSION) {
        return new ZigForExpressionImpl(node);
      }
      else if (type == FOR_PREFIX) {
        return new ZigForPrefixImpl(node);
      }
      else if (type == FOR_STATEMENT) {
        return new ZigForStatementImpl(node);
      }
      else if (type == FOR_TYPE_EXPRESSION) {
        return new ZigForTypeExpressionImpl(node);
      }
      else if (type == FUNCTION_CALL_ARGUMENTS) {
        return new ZigFunctionCallArgumentsImpl(node);
      }
      else if (type == FUNCTION_DECLARATION) {
        return new ZigFunctionDeclarationImpl(node);
      }
      else if (type == FUNCTION_DECLARATION_PROTOTYPE) {
        return new ZigFunctionDeclarationPrototypeImpl(node);
      }
      else if (type == FUNCTION_PROTOTYPE) {
        return new ZigFunctionPrototypeImpl(node);
      }
      else if (type == GROUPED_EXPRESSION) {
        return new ZigGroupedExpressionImpl(node);
      }
      else if (type == IDENTIFIER_LIST) {
        return new ZigIdentifierListImpl(node);
      }
      else if (type == IF_EXPRESSION) {
        return new ZigIfExpressionImpl(node);
      }
      else if (type == IF_PREFIX) {
        return new ZigIfPrefixImpl(node);
      }
      else if (type == IF_STATEMENT) {
        return new ZigIfStatementImpl(node);
      }
      else if (type == IF_TYPE_EXPRESSION) {
        return new ZigIfTypeExpressionImpl(node);
      }
      else if (type == IMPORT_DECLARATION) {
        return new ZigImportDeclarationImpl(node);
      }
      else if (type == INITIALIZER_LIST) {
        return new ZigInitializerListImpl(node);
      }
      else if (type == LABELED_BLOCK_EXPRESSION) {
        return new ZigLabeledBlockExpressionImpl(node);
      }
      else if (type == LABELED_BLOCK_OR_LOOP_STATEMENT) {
        return new ZigLabeledBlockOrLoopStatementImpl(node);
      }
      else if (type == LABELED_LOOP_EXPRESSION) {
        return new ZigLabeledLoopExpressionImpl(node);
      }
      else if (type == LABELED_TYPE_EXPRESSION) {
        return new ZigLabeledTypeExpressionImpl(node);
      }
      else if (type == LINK_SECTION) {
        return new ZigLinkSectionImpl(node);
      }
      else if (type == LOOP_EXPRESSION) {
        return new ZigLoopExpressionImpl(node);
      }
      else if (type == LOOP_STATEMENT) {
        return new ZigLoopStatementImpl(node);
      }
      else if (type == LOOP_TYPE_EXPRESSION) {
        return new ZigLoopTypeExpressionImpl(node);
      }
      else if (type == MULTIPLY_EXPRESSION) {
        return new ZigMultiplyExpressionImpl(node);
      }
      else if (type == NOSUSPEND_EXPRESSION) {
        return new ZigNosuspendExpressionImpl(node);
      }
      else if (type == PARAMETER_DECLARATION) {
        return new ZigParameterDeclarationImpl(node);
      }
      else if (type == PARAMETER_DECLARATION_LIST) {
        return new ZigParameterDeclarationListImpl(node);
      }
      else if (type == PARAMETER_TYPE) {
        return new ZigParameterTypeImpl(node);
      }
      else if (type == PAYLOAD) {
        return new ZigPayloadImpl(node);
      }
      else if (type == POINTER_INDEX_PAYLOAD) {
        return new ZigPointerIndexPayloadImpl(node);
      }
      else if (type == POINTER_PAYLOAD) {
        return new ZigPointerPayloadImpl(node);
      }
      else if (type == POINTER_TYPE_START) {
        return new ZigPointerTypeStartImpl(node);
      }
      else if (type == PREFIX_EXPRESSION) {
        return new ZigPrefixExpressionImpl(node);
      }
      else if (type == PRIMARY_TYPE_EXPRESSION) {
        return new ZigPrimaryTypeExpressionImpl(node);
      }
      else if (type == RESUME_EXPRESSION) {
        return new ZigResumeExpressionImpl(node);
      }
      else if (type == RETURN_EXPRESSION) {
        return new ZigReturnExpressionImpl(node);
      }
      else if (type == SENTINEL_TERMINATOR) {
        return new ZigSentinelTerminatorImpl(node);
      }
      else if (type == SLICE_TYPE_START) {
        return new ZigSliceTypeStartImpl(node);
      }
      else if (type == STATEMENT) {
        return new ZigStatementImpl(node);
      }
      else if (type == STRING_LIST) {
        return new ZigStringListImpl(node);
      }
      else if (type == STRING_LITERAL) {
        return new ZigStringLiteralImpl(node);
      }
      else if (type == SUFFIX_EXPRESSION) {
        return new ZigSuffixExpressionImpl(node);
      }
      else if (type == SWITCH_CASE) {
        return new ZigSwitchCaseImpl(node);
      }
      else if (type == SWITCH_EXPRESSION) {
        return new ZigSwitchExpressionImpl(node);
      }
      else if (type == SWITCH_ITEM) {
        return new ZigSwitchItemImpl(node);
      }
      else if (type == SWITCH_PRONG) {
        return new ZigSwitchProngImpl(node);
      }
      else if (type == SWITCH_PRONG_LIST) {
        return new ZigSwitchProngListImpl(node);
      }
      else if (type == TEST_DECLARATION) {
        return new ZigTestDeclarationImpl(node);
      }
      else if (type == TOP_LEVEL_COMPTIME_BLOCK) {
        return new ZigTopLevelComptimeBlockImpl(node);
      }
      else if (type == TOP_LEVEL_VARIABLE_DECLARATION) {
        return new ZigTopLevelVariableDeclarationImpl(node);
      }
      else if (type == TRY_EXPRESSION) {
        return new ZigTryExpressionImpl(node);
      }
      else if (type == TYPE_EXPRESSION) {
        return new ZigTypeExpressionImpl(node);
      }
      else if (type == UNKNOWN_LENGTH_POINTER) {
        return new ZigUnknownLengthPointerImpl(node);
      }
      else if (type == VARIABLE_DECLARATION) {
        return new ZigVariableDeclarationImpl(node);
      }
      else if (type == WHILE_CONTINUE_EXPRESSION) {
        return new ZigWhileContinueExpressionImpl(node);
      }
      else if (type == WHILE_EXPRESSION) {
        return new ZigWhileExpressionImpl(node);
      }
      else if (type == WHILE_PREFIX) {
        return new ZigWhilePrefixImpl(node);
      }
      else if (type == WHILE_STATEMENT) {
        return new ZigWhileStatementImpl(node);
      }
      else if (type == WHILE_TYPE_EXPRESSION) {
        return new ZigWhileTypeExpressionImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}
