// This is a generated file. Not intended for manual editing.
package language.psi;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;

public class ZigVisitor extends PsiElementVisitor {

  public void visitAdditionExpression(@NotNull ZigAdditionExpression o) {
    visitExpression(o);
  }

  public void visitArrayTypeStart(@NotNull ZigArrayTypeStart o) {
    visitPsiElement(o);
  }

  public void visitAssemblyClobbers(@NotNull ZigAssemblyClobbers o) {
    visitPsiElement(o);
  }

  public void visitAssemblyExpression(@NotNull ZigAssemblyExpression o) {
    visitExpression(o);
  }

  public void visitAssemblyInput(@NotNull ZigAssemblyInput o) {
    visitPsiElement(o);
  }

  public void visitAssemblyInputItem(@NotNull ZigAssemblyInputItem o) {
    visitPsiElement(o);
  }

  public void visitAssemblyInputList(@NotNull ZigAssemblyInputList o) {
    visitPsiElement(o);
  }

  public void visitAssemblyOutput(@NotNull ZigAssemblyOutput o) {
    visitPsiElement(o);
  }

  public void visitAssemblyOutputItem(@NotNull ZigAssemblyOutputItem o) {
    visitPsiElement(o);
  }

  public void visitAssemblyOutputList(@NotNull ZigAssemblyOutputList o) {
    visitPsiElement(o);
  }

  public void visitAssignExpression(@NotNull ZigAssignExpression o) {
    visitExpression(o);
  }

  public void visitBitShiftExpression(@NotNull ZigBitShiftExpression o) {
    visitExpression(o);
  }

  public void visitBitwiseExpression(@NotNull ZigBitwiseExpression o) {
    visitExpression(o);
  }

  public void visitBlockExpression(@NotNull ZigBlockExpression o) {
    visitExpression(o);
  }

  public void visitBlockExpressionStatement(@NotNull ZigBlockExpressionStatement o) {
    visitPsiElement(o);
  }

  public void visitBlockLabel(@NotNull ZigBlockLabel o) {
    visitPsiElement(o);
  }

  public void visitBooleanAndExpression(@NotNull ZigBooleanAndExpression o) {
    visitExpression(o);
  }

  public void visitBooleanOrExpression(@NotNull ZigBooleanOrExpression o) {
    visitExpression(o);
  }

  public void visitBreakExpression(@NotNull ZigBreakExpression o) {
    visitExpression(o);
  }

  public void visitBreakLabel(@NotNull ZigBreakLabel o) {
    visitPsiElement(o);
  }

  public void visitByteAlignment(@NotNull ZigByteAlignment o) {
    visitPsiElement(o);
  }

  public void visitCallingConvectionSection(@NotNull ZigCallingConvectionSection o) {
    visitPsiElement(o);
  }

  public void visitCompareExpression(@NotNull ZigCompareExpression o) {
    visitExpression(o);
  }

  public void visitComptimeExpression(@NotNull ZigComptimeExpression o) {
    visitExpression(o);
  }

  public void visitContainerDeclaration(@NotNull ZigContainerDeclaration o) {
    visitPsiElement(o);
  }

  public void visitContainerDeclarationType(@NotNull ZigContainerDeclarationType o) {
    visitPsiElement(o);
  }

  public void visitContainerField(@NotNull ZigContainerField o) {
    visitPsiElement(o);
  }

  public void visitContinueExpression(@NotNull ZigContinueExpression o) {
    visitExpression(o);
  }

  public void visitCurlySuffixExpression(@NotNull ZigCurlySuffixExpression o) {
    visitExpression(o);
  }

  public void visitErrorSetDeclaration(@NotNull ZigErrorSetDeclaration o) {
    visitPsiElement(o);
  }

  public void visitErrorUnionExpression(@NotNull ZigErrorUnionExpression o) {
    visitExpression(o);
  }

  public void visitExpression(@NotNull ZigExpression o) {
    visitPsiElement(o);
  }

  public void visitExpressionList(@NotNull ZigExpressionList o) {
    visitPsiElement(o);
  }

  public void visitFieldInitializer(@NotNull ZigFieldInitializer o) {
    visitPsiElement(o);
  }

  public void visitForExpression(@NotNull ZigForExpression o) {
    visitExpression(o);
  }

  public void visitForPrefix(@NotNull ZigForPrefix o) {
    visitPsiElement(o);
  }

  public void visitForStatement(@NotNull ZigForStatement o) {
    visitPsiElement(o);
  }

  public void visitForTypeExpression(@NotNull ZigForTypeExpression o) {
    visitExpression(o);
  }

  public void visitFunctionCallArguments(@NotNull ZigFunctionCallArguments o) {
    visitPsiElement(o);
  }

  public void visitFunctionDeclaration(@NotNull ZigFunctionDeclaration o) {
    visitPsiElement(o);
  }

  public void visitFunctionDeclarationPrototype(@NotNull ZigFunctionDeclarationPrototype o) {
    visitPsiElement(o);
  }

  public void visitFunctionPrototype(@NotNull ZigFunctionPrototype o) {
    visitPsiElement(o);
  }

  public void visitGroupedExpression(@NotNull ZigGroupedExpression o) {
    visitExpression(o);
  }

  public void visitIdentifierList(@NotNull ZigIdentifierList o) {
    visitPsiElement(o);
  }

  public void visitIfExpression(@NotNull ZigIfExpression o) {
    visitExpression(o);
  }

  public void visitIfPrefix(@NotNull ZigIfPrefix o) {
    visitPsiElement(o);
  }

  public void visitIfStatement(@NotNull ZigIfStatement o) {
    visitPsiElement(o);
  }

  public void visitIfTypeExpression(@NotNull ZigIfTypeExpression o) {
    visitExpression(o);
  }

  public void visitImportDeclaration(@NotNull ZigImportDeclaration o) {
    visitPsiElement(o);
  }

  public void visitInitializerList(@NotNull ZigInitializerList o) {
    visitPsiElement(o);
  }

  public void visitLabeledBlockExpression(@NotNull ZigLabeledBlockExpression o) {
    visitExpression(o);
  }

  public void visitLabeledBlockOrLoopStatement(@NotNull ZigLabeledBlockOrLoopStatement o) {
    visitPsiElement(o);
  }

  public void visitLabeledLoopExpression(@NotNull ZigLabeledLoopExpression o) {
    visitExpression(o);
  }

  public void visitLabeledTypeExpression(@NotNull ZigLabeledTypeExpression o) {
    visitExpression(o);
  }

  public void visitLinkSection(@NotNull ZigLinkSection o) {
    visitPsiElement(o);
  }

  public void visitLoopExpression(@NotNull ZigLoopExpression o) {
    visitExpression(o);
  }

  public void visitLoopStatement(@NotNull ZigLoopStatement o) {
    visitPsiElement(o);
  }

  public void visitLoopTypeExpression(@NotNull ZigLoopTypeExpression o) {
    visitExpression(o);
  }

  public void visitMultiplyExpression(@NotNull ZigMultiplyExpression o) {
    visitExpression(o);
  }

  public void visitNosuspendExpression(@NotNull ZigNosuspendExpression o) {
    visitExpression(o);
  }

  public void visitParameterDeclaration(@NotNull ZigParameterDeclaration o) {
    visitPsiElement(o);
  }

  public void visitParameterDeclarationList(@NotNull ZigParameterDeclarationList o) {
    visitPsiElement(o);
  }

  public void visitParameterType(@NotNull ZigParameterType o) {
    visitPsiElement(o);
  }

  public void visitPayload(@NotNull ZigPayload o) {
    visitPsiElement(o);
  }

  public void visitPointerIndexPayload(@NotNull ZigPointerIndexPayload o) {
    visitPsiElement(o);
  }

  public void visitPointerPayload(@NotNull ZigPointerPayload o) {
    visitPsiElement(o);
  }

  public void visitPointerTypeStart(@NotNull ZigPointerTypeStart o) {
    visitPsiElement(o);
  }

  public void visitPrefixExpression(@NotNull ZigPrefixExpression o) {
    visitExpression(o);
  }

  public void visitPrimaryExpression(@NotNull ZigPrimaryExpression o) {
    visitExpression(o);
  }

  public void visitPrimaryTypeExpression(@NotNull ZigPrimaryTypeExpression o) {
    visitExpression(o);
  }

  public void visitResumeExpression(@NotNull ZigResumeExpression o) {
    visitExpression(o);
  }

  public void visitReturnExpression(@NotNull ZigReturnExpression o) {
    visitExpression(o);
  }

  public void visitSentinelTerminator(@NotNull ZigSentinelTerminator o) {
    visitPsiElement(o);
  }

  public void visitSliceTypeStart(@NotNull ZigSliceTypeStart o) {
    visitPsiElement(o);
  }

  public void visitStatement(@NotNull ZigStatement o) {
    visitPsiElement(o);
  }

  public void visitStringList(@NotNull ZigStringList o) {
    visitPsiElement(o);
  }

  public void visitStringLiteral(@NotNull ZigStringLiteral o) {
    visitPsiElement(o);
  }

  public void visitSuffixExpression(@NotNull ZigSuffixExpression o) {
    visitExpression(o);
  }

  public void visitSwitchCase(@NotNull ZigSwitchCase o) {
    visitPsiElement(o);
  }

  public void visitSwitchExpression(@NotNull ZigSwitchExpression o) {
    visitExpression(o);
  }

  public void visitSwitchItem(@NotNull ZigSwitchItem o) {
    visitPsiElement(o);
  }

  public void visitSwitchProng(@NotNull ZigSwitchProng o) {
    visitPsiElement(o);
  }

  public void visitSwitchProngList(@NotNull ZigSwitchProngList o) {
    visitPsiElement(o);
  }

  public void visitTestDeclaration(@NotNull ZigTestDeclaration o) {
    visitPsiElement(o);
  }

  public void visitTopLevelComptimeBlock(@NotNull ZigTopLevelComptimeBlock o) {
    visitPsiElement(o);
  }

  public void visitTopLevelVariableDeclaration(@NotNull ZigTopLevelVariableDeclaration o) {
    visitPsiElement(o);
  }

  public void visitTryExpression(@NotNull ZigTryExpression o) {
    visitExpression(o);
  }

  public void visitTypeExpression(@NotNull ZigTypeExpression o) {
    visitExpression(o);
  }

  public void visitUnknownLengthPointer(@NotNull ZigUnknownLengthPointer o) {
    visitPsiElement(o);
  }

  public void visitVariableDeclaration(@NotNull ZigVariableDeclaration o) {
    visitPsiElement(o);
  }

  public void visitWhileContinueExpression(@NotNull ZigWhileContinueExpression o) {
    visitExpression(o);
  }

  public void visitWhileExpression(@NotNull ZigWhileExpression o) {
    visitExpression(o);
  }

  public void visitWhilePrefix(@NotNull ZigWhilePrefix o) {
    visitPsiElement(o);
  }

  public void visitWhileStatement(@NotNull ZigWhileStatement o) {
    visitPsiElement(o);
  }

  public void visitWhileTypeExpression(@NotNull ZigWhileTypeExpression o) {
    visitExpression(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}
