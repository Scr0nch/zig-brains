// This is a generated file. Not intended for manual editing.
package language.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface ZigFunctionDeclarationPrototype extends PsiElement {

  @Nullable
  ZigByteAlignment getByteAlignment();

  @Nullable
  ZigCallingConvectionSection getCallingConvectionSection();

  @NotNull
  ZigExpression getExpression();

  @Nullable
  ZigLinkSection getLinkSection();

  @NotNull
  ZigParameterDeclarationList getParameterDeclarationList();

}
