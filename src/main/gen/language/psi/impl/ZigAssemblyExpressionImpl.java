// This is a generated file. Not intended for manual editing.
package language.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static language.psi.ZigTypes.*;
import language.psi.*;

public class ZigAssemblyExpressionImpl extends ZigExpressionImpl implements ZigAssemblyExpression {

  public ZigAssemblyExpressionImpl(@NotNull ASTNode node) {
    super(node);
  }

  @Override
  public void accept(@NotNull ZigVisitor visitor) {
    visitor.visitAssemblyExpression(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ZigVisitor) accept((ZigVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public ZigAssemblyOutput getAssemblyOutput() {
    return findChildByClass(ZigAssemblyOutput.class);
  }

  @Override
  @Nullable
  public ZigExpression getExpression() {
    return findChildByClass(ZigExpression.class);
  }

}
