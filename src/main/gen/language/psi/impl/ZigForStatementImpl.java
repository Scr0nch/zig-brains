// This is a generated file. Not intended for manual editing.
package language.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static language.psi.ZigTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import language.psi.*;

public class ZigForStatementImpl extends ASTWrapperPsiElement implements ZigForStatement {

  public ZigForStatementImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ZigVisitor visitor) {
    visitor.visitForStatement(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ZigVisitor) accept((ZigVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public ZigExpression getExpression() {
    return findNotNullChildByClass(ZigExpression.class);
  }

  @Override
  @NotNull
  public ZigForPrefix getForPrefix() {
    return findNotNullChildByClass(ZigForPrefix.class);
  }

  @Override
  @Nullable
  public ZigStatement getStatement() {
    return findChildByClass(ZigStatement.class);
  }

}
