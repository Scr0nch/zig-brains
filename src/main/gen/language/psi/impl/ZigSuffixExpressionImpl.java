// This is a generated file. Not intended for manual editing.
package language.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static language.psi.ZigTypes.*;
import language.psi.*;

public class ZigSuffixExpressionImpl extends ZigExpressionImpl implements ZigSuffixExpression {

  public ZigSuffixExpressionImpl(@NotNull ASTNode node) {
    super(node);
  }

  @Override
  public void accept(@NotNull ZigVisitor visitor) {
    visitor.visitSuffixExpression(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ZigVisitor) accept((ZigVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<ZigExpression> getExpressionList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, ZigExpression.class);
  }

  @Override
  @NotNull
  public List<ZigFunctionCallArguments> getFunctionCallArgumentsList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, ZigFunctionCallArguments.class);
  }

  @Override
  @NotNull
  public List<ZigSentinelTerminator> getSentinelTerminatorList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, ZigSentinelTerminator.class);
  }

}
