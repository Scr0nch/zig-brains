// This is a generated file. Not intended for manual editing.
package language.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static language.psi.ZigTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import language.psi.*;

public class ZigStatementImpl extends ASTWrapperPsiElement implements ZigStatement {

  public ZigStatementImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ZigVisitor visitor) {
    visitor.visitStatement(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ZigVisitor) accept((ZigVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public ZigBlockExpressionStatement getBlockExpressionStatement() {
    return findChildByClass(ZigBlockExpressionStatement.class);
  }

  @Override
  @Nullable
  public ZigExpression getExpression() {
    return findChildByClass(ZigExpression.class);
  }

  @Override
  @Nullable
  public ZigIfStatement getIfStatement() {
    return findChildByClass(ZigIfStatement.class);
  }

  @Override
  @Nullable
  public ZigLabeledBlockOrLoopStatement getLabeledBlockOrLoopStatement() {
    return findChildByClass(ZigLabeledBlockOrLoopStatement.class);
  }

  @Override
  @Nullable
  public ZigPayload getPayload() {
    return findChildByClass(ZigPayload.class);
  }

  @Override
  @Nullable
  public ZigVariableDeclaration getVariableDeclaration() {
    return findChildByClass(ZigVariableDeclaration.class);
  }

}
