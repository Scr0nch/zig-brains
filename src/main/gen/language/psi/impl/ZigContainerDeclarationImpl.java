// This is a generated file. Not intended for manual editing.
package language.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static language.psi.ZigTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import language.psi.*;

public class ZigContainerDeclarationImpl extends ASTWrapperPsiElement implements ZigContainerDeclaration {

  public ZigContainerDeclarationImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ZigVisitor visitor) {
    visitor.visitContainerDeclaration(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ZigVisitor) accept((ZigVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public ZigContainerDeclarationType getContainerDeclarationType() {
    return findNotNullChildByClass(ZigContainerDeclarationType.class);
  }

  @Override
  @NotNull
  public List<ZigContainerField> getContainerFieldList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, ZigContainerField.class);
  }

  @Override
  @NotNull
  public List<ZigFunctionDeclaration> getFunctionDeclarationList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, ZigFunctionDeclaration.class);
  }

  @Override
  @NotNull
  public List<ZigImportDeclaration> getImportDeclarationList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, ZigImportDeclaration.class);
  }

  @Override
  @NotNull
  public List<ZigTestDeclaration> getTestDeclarationList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, ZigTestDeclaration.class);
  }

  @Override
  @NotNull
  public List<ZigTopLevelComptimeBlock> getTopLevelComptimeBlockList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, ZigTopLevelComptimeBlock.class);
  }

  @Override
  @NotNull
  public List<ZigTopLevelVariableDeclaration> getTopLevelVariableDeclarationList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, ZigTopLevelVariableDeclaration.class);
  }

}
