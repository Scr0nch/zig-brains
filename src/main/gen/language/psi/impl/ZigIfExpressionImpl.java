// This is a generated file. Not intended for manual editing.
package language.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static language.psi.ZigTypes.*;
import language.psi.*;

public class ZigIfExpressionImpl extends ZigExpressionImpl implements ZigIfExpression {

  public ZigIfExpressionImpl(@NotNull ASTNode node) {
    super(node);
  }

  @Override
  public void accept(@NotNull ZigVisitor visitor) {
    visitor.visitIfExpression(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ZigVisitor) accept((ZigVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<ZigExpression> getExpressionList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, ZigExpression.class);
  }

  @Override
  @NotNull
  public ZigIfPrefix getIfPrefix() {
    return findNotNullChildByClass(ZigIfPrefix.class);
  }

  @Override
  @Nullable
  public ZigPayload getPayload() {
    return findChildByClass(ZigPayload.class);
  }

}
