// This is a generated file. Not intended for manual editing.
package language.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static language.psi.ZigTypes.*;
import language.psi.*;

public class ZigPrimaryTypeExpressionImpl extends ZigExpressionImpl implements ZigPrimaryTypeExpression {

  public ZigPrimaryTypeExpressionImpl(@NotNull ASTNode node) {
    super(node);
  }

  @Override
  public void accept(@NotNull ZigVisitor visitor) {
    visitor.visitPrimaryTypeExpression(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ZigVisitor) accept((ZigVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public ZigContainerDeclaration getContainerDeclaration() {
    return findChildByClass(ZigContainerDeclaration.class);
  }

  @Override
  @Nullable
  public ZigErrorSetDeclaration getErrorSetDeclaration() {
    return findChildByClass(ZigErrorSetDeclaration.class);
  }

  @Override
  @Nullable
  public ZigExpression getExpression() {
    return findChildByClass(ZigExpression.class);
  }

  @Override
  @Nullable
  public ZigFunctionCallArguments getFunctionCallArguments() {
    return findChildByClass(ZigFunctionCallArguments.class);
  }

  @Override
  @Nullable
  public ZigFunctionPrototype getFunctionPrototype() {
    return findChildByClass(ZigFunctionPrototype.class);
  }

  @Override
  @Nullable
  public ZigInitializerList getInitializerList() {
    return findChildByClass(ZigInitializerList.class);
  }

  @Override
  @Nullable
  public ZigStringLiteral getStringLiteral() {
    return findChildByClass(ZigStringLiteral.class);
  }

}
