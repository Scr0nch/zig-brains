// This is a generated file. Not intended for manual editing.
package language.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static language.psi.ZigTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import language.psi.*;

public class ZigFunctionPrototypeImpl extends ASTWrapperPsiElement implements ZigFunctionPrototype {

  public ZigFunctionPrototypeImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ZigVisitor visitor) {
    visitor.visitFunctionPrototype(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ZigVisitor) accept((ZigVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public ZigByteAlignment getByteAlignment() {
    return findChildByClass(ZigByteAlignment.class);
  }

  @Override
  @Nullable
  public ZigCallingConvectionSection getCallingConvectionSection() {
    return findChildByClass(ZigCallingConvectionSection.class);
  }

  @Override
  @NotNull
  public ZigExpression getExpression() {
    return findNotNullChildByClass(ZigExpression.class);
  }

  @Override
  @Nullable
  public ZigLinkSection getLinkSection() {
    return findChildByClass(ZigLinkSection.class);
  }

  @Override
  @NotNull
  public ZigParameterDeclarationList getParameterDeclarationList() {
    return findNotNullChildByClass(ZigParameterDeclarationList.class);
  }

}
