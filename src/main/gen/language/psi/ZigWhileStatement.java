// This is a generated file. Not intended for manual editing.
package language.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface ZigWhileStatement extends PsiElement {

  @NotNull
  ZigExpression getExpression();

  @Nullable
  ZigPayload getPayload();

  @Nullable
  ZigStatement getStatement();

  @NotNull
  ZigWhilePrefix getWhilePrefix();

}
