// This is a generated file. Not intended for manual editing.
package language.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface ZigPrimaryTypeExpression extends ZigExpression {

  @Nullable
  ZigContainerDeclaration getContainerDeclaration();

  @Nullable
  ZigErrorSetDeclaration getErrorSetDeclaration();

  @Nullable
  ZigExpression getExpression();

  @Nullable
  ZigFunctionCallArguments getFunctionCallArguments();

  @Nullable
  ZigFunctionPrototype getFunctionPrototype();

  @Nullable
  ZigInitializerList getInitializerList();

  @Nullable
  ZigStringLiteral getStringLiteral();

}
