// This is a generated file. Not intended for manual editing.
package language.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface ZigStatement extends PsiElement {

  @Nullable
  ZigBlockExpressionStatement getBlockExpressionStatement();

  @Nullable
  ZigExpression getExpression();

  @Nullable
  ZigIfStatement getIfStatement();

  @Nullable
  ZigLabeledBlockOrLoopStatement getLabeledBlockOrLoopStatement();

  @Nullable
  ZigPayload getPayload();

  @Nullable
  ZigVariableDeclaration getVariableDeclaration();

}
