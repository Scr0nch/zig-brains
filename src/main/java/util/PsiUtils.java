package util;

import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import language.psi.*;
import org.jetbrains.annotations.NotNull;

// Note: this class is named PsiUtils because it would otherwise clash with {@link com.intellij.psi.util.PsiUtil}.

/**
 * A utility class to help with navigating a psi tree.
 */
public class PsiUtils {

    /**
     * Gets the next sibling of the given element and if it is a whitespace element, gets the next sibling of the
     * whitespace element.
     *
     * @param element the element before the non-whitespace sibling to find
     * @return the next non-whitespace sibling of the given element
     */
    public static PsiElement getNextNonWhitespaceSibling(final PsiElement element) {
        final PsiElement nextSiblingOrWhitespace = element.getNextSibling();
        return getNextSiblingIfWhitespace(nextSiblingOrWhitespace);
    }

    /**
     * Gets the previous sibling of the given element and if it is a whitespace element, gets the previous sibling of
     * the whitespace element.
     *
     * @param element the element after the non-whitespace sibling to find
     * @return the previous non-whitespace sibling of the given element
     */
    public static PsiElement getPreviousNonWhitespaceSibling(final PsiElement element) {
        final PsiElement previousSiblingOrWhitespace = element.getPrevSibling();
        return previousSiblingOrWhitespace instanceof PsiWhiteSpace ? previousSiblingOrWhitespace.getPrevSibling() :
                previousSiblingOrWhitespace;
    }

    /**
     * Gets the next sibling of the given element if it is a whitespace element.
     *
     * @param element the element to either get the next sibling of or return
     * @return the given element if it is not whitespace, the element's next sibling otherwise
     */
    public static PsiElement getNextSiblingIfWhitespace(final PsiElement element) {
        return element instanceof PsiWhiteSpace ? element.getNextSibling() : element;
    }

    @NotNull
    public static PsiElement createStatement(final Project project, final ZigExpression expression) {
        if (expression instanceof ZigLabeledBlockExpression || expression instanceof ZigBlockExpression
                || expression instanceof ZigSwitchExpression || expression instanceof ZigIfExpression
                || expression instanceof ZigLabeledLoopExpression || expression instanceof ZigLoopExpression
                || expression instanceof ZigWhileExpression || expression instanceof ZigForExpression) {
            // only these expressions are statements without a semicolon
            return expression;
        }

        return ZigPsiFactory.createStatement(project, expression.getText() + ";");
    }

}
