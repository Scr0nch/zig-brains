package util;

import com.intellij.ide.IdeBundle;
import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.TextFieldWithBrowseButton;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.*;

/**
 * A utility class that helps with UI-related functionality.
 */
public class UIUtils {

    /**
     * Creates a text field with an attached browse button that will open a file chooser and allow the user to pick a
     * single executable file. The path of the file will be inserted into the text field and is required to be
     * non-empty.
     *
     * @param project       the current project or {@code null}
     * @param textFieldName the name of the text field's contents. Will be used in an error message if the path is
     *                      invalid
     * @return a text field with an attached browse button with the given name
     */
    public static TextFieldWithBrowseButton createNonEmptyTextFieldWithBrowseButton(final @Nullable Project project,
                                                                                    final @NotNull String textFieldName) {
        // Note: this method does not implement the necessary functionality even though the parameters are passed
        // unchanged to the private implementation because the textFieldName should not be null when specified
        return createTextFieldWithBrowseButton(project, textFieldName);
    }

    /**
     * Creates a text field with an attached browse button that will open a file chooser and allow the user to pick a
     * single executable file. The path of the file will be inserted into the text field.
     *
     * @param project the current project or {@code null}
     * @return a text field with an attached browse button
     */
    public static TextFieldWithBrowseButton createTextFieldWithBrowseButton(final @Nullable Project project) {
        return createTextFieldWithBrowseButton(project, null);
    }

    private static TextFieldWithBrowseButton createTextFieldWithBrowseButton(final @Nullable Project project,
                                                                             final @Nullable String textFieldName) {
        final TextFieldWithBrowseButton textField = new TextFieldWithBrowseButton();

        final FileChooserDescriptor fileChooserDescriptor = new FileChooserDescriptor(true, false, false, false,
                false, false) {

            @Override
            public boolean isFileSelectable(final VirtualFile file) {
                final String path = file.getPath();
                return (textFieldName == null && path.isBlank())
                        || Command.getErrorMessageIfNotExecutableFile(path, true, textFieldName) == null;
            }

        };
        textField.addBrowseFolderListener(IdeBundle.message("title.browse.icon"),
                IdeBundle.message("prompt.browse.icon.for.selected.action"), project, fileChooserDescriptor);

        return textField;
    }

}
