package util;

import java.io.*;
import java.nio.file.*;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import org.jetbrains.annotations.NotNull;

/**
 * A utility class that helps with executing other processes/commands.
 */
public class Command {

    /**
     * A simple immutable data class that stores the standard output and standard error output from a process.
     */
    public static class Output {

        private Output(final String standardOutput, final String standardError) {
            this.standardOutput = standardOutput;
            this.standardError = standardError;
        }

        /**
         * The standard output of a process or an empty string if none was generated
         */
        public final String standardOutput;

        /**
         * The standard error of a process or an empty string if none was generated
         */
        public final String standardError;

    }

    /**
     * Convenience method for {@link Command#execute(String[], long, File)} that uses the current working directory of
     * the current process for the working directory of the child process.
     *
     * @param command             the command to execute
     * @param timeoutMilliseconds the maximum amount of time the process can execute for
     * @return an {@link Output} instance with the standard input and error output of the command or {@code null} if the
     * command failed to complete within the time limit.
     * @throws IOException          if an I/O error occurs
     * @throws InterruptedException if this thread is interrupted while waiting for the command to complete.
     */
    @Nullable
    public static Output execute(@NotNull final String[] command, final long timeoutMilliseconds) throws IOException,
            InterruptedException {
        return execute(command, timeoutMilliseconds, null);
    }

    /**
     * Executes the given command with the given time limit. The command is an array of a command string split by
     * whitespace. This function will block until either the process completes or the time limit expires. If the process
     * does not complete execution within the given time limit, it will be destroyed. Note: even at small, zero, or
     * negative time limits, the process may still complete execution if it is fast because of the delay between the
     * starting of the process and the waiting for the completion of the process.
     *
     * @param command             the command to execute
     * @param timeoutMilliseconds the maximum amount of time the process can execute for
     * @param directory           the working directory that the command should be run in or {@code null} for the
     *                            current working directory of this process
     * @return an {@link Output} instance with the standard input and error output of the command or {@code null} if the
     * command failed to complete within the time limit.
     * @throws IOException          if an I/O error occurs
     * @throws InterruptedException if this thread is interrupted while waiting for the command to complete.
     */
    @Nullable
    public static Output execute(@NotNull final String[] command, final long timeoutMilliseconds,
                                 @Nullable final File directory) throws IOException, InterruptedException {
        final Process process = new ProcessBuilder().command(command).directory(directory).start();
        if (!process.waitFor(timeoutMilliseconds, TimeUnit.MILLISECONDS)) {
            // the process has not exited within the time limit
            process.destroy();
            return null;
        }

        final Output output = new Output(new String(process.getInputStream().readAllBytes()),
                new String(process.getErrorStream().readAllBytes()));

        process.destroy();

        return output;
    }

    /**
     * Returns the path of the command with the given name or an empty string if an executable with the given name does
     * not exist or it cannot be found within 100 milliseconds.
     *
     * @param commandName the name of the command to find
     * @return the path of the command with the given name or an empty string if it cannot be found
     */
    @NotNull
    public static String getCommandLocationOrEmpty(final @NotNull String commandName) {
        try {
            final Output output = execute(new String[] { "which", commandName }, 100);
            if (output != null && !output.standardOutput.endsWith("not found")) {
                // Note: the trim call is necessary, trailing whitespace is possible which will cause other path-related
                // operations to fail
                return output.standardOutput.trim();
            }
        } catch (final IOException ignored) {} catch (final InterruptedException e) {
            // log interrupted exceptions as they should not occur within the given timeout
            e.printStackTrace();
        }

        return "";
    }

    /**
     * Creates a error message using the given file name if the given file path does not point to an executable file
     *
     * @param filePath         the path to the file to validate
     * @param isEmptyPathValid {@code true} if an empty path is valid, {@code false} otherwise
     * @param fileName         a user-friendly name for the file to use in error messages
     * @return an error message describing what is wrong about the given file path, {@code null} if the given file path
     * points to a valid executable file
     */
    @Nullable
    public static String getErrorMessageIfNotExecutableFile(final String filePath, final boolean isEmptyPathValid,
                                                            final String fileName) {
        if (isEmptyPathValid && filePath.isBlank()) {
            return null;
        }

        try {
            final Path zlsExecutablePath = Paths.get(filePath);

            if (!Files.exists(zlsExecutablePath)) {
                return "Non-existent location given for " + fileName + "!";
            }
            if (!Files.isExecutable(zlsExecutablePath) || Files.isDirectory(zlsExecutablePath)) {
                return "Non-executable file given for " + fileName + "!";
            }
        } catch (final InvalidPathException ignored) {
            return "Invalid path given for " + fileName + "!";
        }

        return null;
    }

    private Command() {}

}
