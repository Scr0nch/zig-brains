package editor;

import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.*;
import java.util.Map;
import javax.swing.Icon;
import org.jetbrains.annotations.*;

/**
 * A {@code ColorSettingsPage} implementation that gives the user the option to customize all of the syntax highlighting
 * categories highlighted by a {@link ZigSyntaxHighlighter}.
 */
public class ZigColorSettingsPage implements ColorSettingsPage {

    /**
     * A list of attribute descriptions witch attach a display name to all of the highlightable tokens from the {@link
     * ZigSyntaxHighlighter}. This list somewhat matches the order of the constants declared in the aforementioned
     * class.
     */
    private static final AttributesDescriptor[] ATTRIBUTES_DESCRIPTORS = new AttributesDescriptor[] {
            new AttributesDescriptor("Identifier", ZigSyntaxHighlighter.IDENTIFIER),
            new AttributesDescriptor("Builtin identifier", ZigSyntaxHighlighter.BUILTIN_IDENTIFIER),
            new AttributesDescriptor("Number", ZigSyntaxHighlighter.NUMBER),
            new AttributesDescriptor("Keyword", ZigSyntaxHighlighter.KEYWORD),
            new AttributesDescriptor("String//Text", ZigSyntaxHighlighter.STRING),
            new AttributesDescriptor("String//Bad character", ZigSyntaxHighlighter.BAD_CHARACTER),
            new AttributesDescriptor("Comments//Top level documentation comment",
                    ZigSyntaxHighlighter.TOP_LEVEL_DOCUMENTATION_COMMENT),
            new AttributesDescriptor("Comments//Documentation comment", ZigSyntaxHighlighter.DOCUMENTATION_COMMENT),
            new AttributesDescriptor("Comments//Line comment", ZigSyntaxHighlighter.LINE_COMMENT),
            new AttributesDescriptor("Braces and operators//Operator sign", ZigSyntaxHighlighter.OPERATOR_SIGN),
            new AttributesDescriptor("Braces and operators//Braces", ZigSyntaxHighlighter.BRACES),
            new AttributesDescriptor("Braces and operators//Dot", ZigSyntaxHighlighter.DOT),
            new AttributesDescriptor("Braces and operators//Semicolon", ZigSyntaxHighlighter.SEMICOLON),
            new AttributesDescriptor("Braces and operators//Comma", ZigSyntaxHighlighter.COMMA),
            new AttributesDescriptor("Braces and operators//Parenthesis", ZigSyntaxHighlighter.PARENTHESIS),
            new AttributesDescriptor("Braces and operators//Brackets", ZigSyntaxHighlighter.BRACKETS),
            new AttributesDescriptor("Functions//Function declaration", ZigSyntaxHighlighter.FUNCTION_DECLARATION),
            new AttributesDescriptor("Functions//Function call", ZigSyntaxHighlighter.FUNCTION_CALL),
            new AttributesDescriptor("Variables//Global variable", ZigSyntaxHighlighter.GLOBAL_VARIABLE),
            new AttributesDescriptor("Variables//Local constant", ZigSyntaxHighlighter.LOCAL_CONSTANT),
            new AttributesDescriptor("Variables//Local variable", ZigSyntaxHighlighter.LOCAL_VARIABLE),
            new AttributesDescriptor("Variables//Parameter", ZigSyntaxHighlighter.PARAMETER),
            new AttributesDescriptor("Structures//Structure declaration", ZigSyntaxHighlighter.STRUCTURE),
            new AttributesDescriptor("Structures//Constant", ZigSyntaxHighlighter.STRUCTURE_CONSTANT),
            new AttributesDescriptor("Structures//Field", ZigSyntaxHighlighter.STRUCTURE_FIELD),
    };

    private static final Map<String, TextAttributesKey> ADDITIONAL_HIGHLIGHTING_TAG_TO_DESCRIPTION_MAP = Map.of(
            "BUILTIN_IDENTIFIER", ZigSyntaxHighlighter.BUILTIN_IDENTIFIER,
            "GLOBAL_VARIABLE", ZigSyntaxHighlighter.GLOBAL_VARIABLE,
            "STRUCTURE", ZigSyntaxHighlighter.STRUCTURE,
            "STRUCTURE_VARIABLE", ZigSyntaxHighlighter.STRUCTURE_FIELD,
            "STRUCTURE_CONSTANT", ZigSyntaxHighlighter.STRUCTURE_CONSTANT,
            "FUNCTION_DECLARATION", ZigSyntaxHighlighter.FUNCTION_DECLARATION,
            "PARAMETER", ZigSyntaxHighlighter.PARAMETER,
            "FUNCTION_CALL", ZigSyntaxHighlighter.FUNCTION_CALL,
            "LOCAL_VARIABLE", ZigSyntaxHighlighter.LOCAL_VARIABLE,
            "LOCAL_CONSTANT", ZigSyntaxHighlighter.LOCAL_CONSTANT
    );

    @Override
    public Icon getIcon() {
        return ZigIcons.LANGUAGE_ICON_WITH_NAME;
    }

    @Override
    public @NotNull SyntaxHighlighter getHighlighter() {
        return new ZigSyntaxHighlighter();
    }

    @Override
    public @NotNull String getDemoText() {
        return "const <GLOBAL_VARIABLE>std</GLOBAL_VARIABLE> = <BUILTIN_IDENTIFIER>@import</BUILTIN_IDENTIFIER>" +
                "(\"std\");\n\n"
                + "//! top level documentation comment\n\n"
                + "// global variables\n"
                + "var <GLOBAL_VARIABLE>y</GLOBAL_VARIABLE> = <GLOBAL_VARIABLE>x</GLOBAL_VARIABLE> + 2;\n"
                + "/// documentation comment\n"
                + "const <GLOBAL_VARIABLE>x</GLOBAL_VARIABLE> = 0xFF;\n\n"
                + "const <STRUCTURE>Structure</STRUCTURE> = struct {\n"
                + "    var <STRUCTURE_VARIABLE>structure_variable</STRUCTURE_VARIABLE>: u8\n"
                + "    const <STRUCTURE_CONSTANT>structure_constant</STRUCTURE_CONSTANT>: i32 = 2;\n"
                + "};\n\n"
                + "const <GLOBAL_VARIABLE>Enumeration</GLOBAL_VARIABLE> = enum {\n"
                + "    One,\n"
                + "    Two,\n"
                + "    Three,\n"
                + "};\n\n"
                + "const <GLOBAL_VARIABLE>Union</GLOBAL_VARIABLE> = union {\n"
                + "    Int: i32,\n"
                + "    Float: f32,\n"
                + "};\n\n"
                + "pub fn <FUNCTION_DECLARATION>main</FUNCTION_DECLARATION>(<PARAMETER>parameter</PARAMETER>: i32) " +
                "!void {\n"
                + "    // comment\n"
                + "    std.debug.<FUNCTION_CALL>warn</FUNCTION_CALL>(\"Hello world!\", .{});\n\n"
                + "    // integer constant with type specifier\n"
                + "    const <LOCAL_CONSTANT>integer</LOCAL_CONSTANT>: i32 = 100_000 + 1 / 1 * 1 ^ 0b11110000 - " +
                "0o755;\n\n"
                + "    // floats\n"
                + "    const <LOCAL_CONSTANT>float</LOCAL_CONSTANT>: f32 = 123.0 + 123.0E+77 + 0x103.70P-5 + " +
                "0x1234_5678.9ABC_CDEFp-10;\n\n"
                + "    // builtin function number cast\n"
                + "    const <LOCAL_CONSTANT>float_to_int</LOCAL_CONSTANT>: i32 = @floatToInt(i32, float);\n\n"
                + "    // boolean variable without type specifier\n"
                + "    var <LOCAL_VARIABLE>boolean</LOCAL_VARIABLE> = !(true and false) or true;\n\n"
                + "    // optional\n"
                + "    var <LOCAL_VARIABLE>optional</LOCAL_VARIABLE>: ?[]const u8 = undefined;\n"
                + "    <LOCAL_VARIABLE>optional</LOCAL_VARIABLE> = null;\n"
                + "    <LOCAL_VARIABLE>optional</LOCAL_VARIABLE> = \"foo\";\n"
                + "    if (<LOCAL_VARIABLE>optional</LOCAL_VARIABLE> != null) block_name: {\n"
                + "        // do something with optional\n"
                + "    }\n"
                + "    if (<LOCAL_VARIABLE>optional</LOCAL_VARIABLE>) |<LOCAL_CONSTANT>not_optional</LOCAL_CONSTANT>|" +
                " {\n"
                + "        // do something with not_optional\n"
                + "    }\n\n"
                + "    // error union\n"
                + "    var <LOCAL_VARIABLE>number_or_error</LOCAL_VARIABLE> = anyerror!i32 = error.DivideByZero;\n"
                + "    <LOCAL_VARIABLE>number_or_error</LOCAL_VARIABLE> = 100_000;\n\n"
                + "    // characters\n"
                + "    const <LOCAL_CONSTANT>character</LOCAL_CONSTANT>: u8 = 'e'; // \\x65, \\u{1f4a9}\n\n"
                + "    // multiline string\n"
                + "    comptime const <LOCAL_CONSTANT>multiline_string</LOCAL_CONSTANT> =\n"
                + "        \\\\ first line\n"
                + "        \\\\ second line\n"
                + "    ;\n"
                + "}";
    }

    @Override
    public @Nullable Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
        return ADDITIONAL_HIGHLIGHTING_TAG_TO_DESCRIPTION_MAP;
    }

    @NotNull
    @Override
    public AttributesDescriptor @NotNull [] getAttributeDescriptors() {
        return ATTRIBUTES_DESCRIPTORS;
    }

    @NotNull
    @Override
    public ColorDescriptor @NotNull [] getColorDescriptors() {
        return ColorDescriptor.EMPTY_ARRAY;
    }

    @Override
    public @NotNull @Nls(capitalization = Nls.Capitalization.Title) String getDisplayName() {
        return "Zig";
    }

}
