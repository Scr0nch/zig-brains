package editor;

import com.intellij.notification.*;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.*;

public class Notifier {

    private static final String ZLS_INFO_NOTIFICATION_GROUP_ID = "ZLS Information Messages";
    private static final String ZIG_WARNING_NOTIFICATION_GROUP_ID = "Zig Plugin Warnings";
    private static final String ZIG_ERROR_NOTIFICATION_GROUP_ID = "Zig Plugin Errors";

    private static final NotificationGroup ZLS_INFO_NOTIFICATION_GROUP =
            NotificationGroupManager.getInstance().getNotificationGroup(ZLS_INFO_NOTIFICATION_GROUP_ID);
    private static final NotificationGroup WARNING_NOTIFICATION_GROUP =
            NotificationGroupManager.getInstance().getNotificationGroup(ZIG_WARNING_NOTIFICATION_GROUP_ID);
    private static final NotificationGroup ERROR_NOTIFICATION_GROUP =
            NotificationGroupManager.getInstance().getNotificationGroup(ZIG_ERROR_NOTIFICATION_GROUP_ID);

    public static void displayZLSInfoMessage(@Nullable final Project project, @NotNull final String content) {
        ZLS_INFO_NOTIFICATION_GROUP.createNotification(content, NotificationType.INFORMATION)
                .setIcon(ZigIcons.LANGUAGE_ICON).notify(project);
    }

    public static void displayWarning(@Nullable final Project project, @NotNull final String content) {
        WARNING_NOTIFICATION_GROUP.createNotification(content, NotificationType.WARNING).setIcon(ZigIcons.LANGUAGE_ICON)
                .notify(project);
    }

    public static void displayError(@Nullable final Project project, @NotNull final String content) {
        ERROR_NOTIFICATION_GROUP.createNotification(content, NotificationType.ERROR).setIcon(ZigIcons.LANGUAGE_ICON)
                .notify(project);
    }

    private Notifier() {}

}
