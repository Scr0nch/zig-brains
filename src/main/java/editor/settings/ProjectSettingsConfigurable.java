package editor.settings;

import com.intellij.openapi.options.*;
import com.intellij.openapi.project.Project;
import editor.settings.ProjectSettingsStateComponent.ProjectSettingsState;
import javax.swing.JComponent;
import org.jetbrains.annotations.*;

public class ProjectSettingsConfigurable implements SearchableConfigurable {

    private final Project project;
    private ProjectSettingsUIComponent uiComponent;

    public ProjectSettingsConfigurable(final Project project) {
        this.project = project;
    }

    @Override
    public @NotNull String getId() {
        return "editor.settings.ProjectSettingsConfigurable";
    }

    @Nls(capitalization = Nls.Capitalization.Title)
    @Override
    public String getDisplayName() {
        return "Zig";
    }

    @Override
    public @Nullable JComponent createComponent() {
        uiComponent = new ProjectSettingsUIComponent(project);
        return uiComponent.getRootComponent();
    }

    @Override
    public boolean isModified() {
        final ProjectSettingsState projectSettingsState = ProjectSettingsStateComponent.getInstance(project).getState();
        return !(uiComponent.getZigExecutableLocation().equals(projectSettingsState.zigExecutableLocation)
                && uiComponent.getZLSExecutableLocation().equals(projectSettingsState.zlsExecutableLocation)
                && uiComponent.isZLSInfoMessagesSuppressed() == projectSettingsState.isInfoMessagesSuppressed);
    }

    @Override
    public void apply() throws ConfigurationException {
        final ProjectSettingsStateComponent projectSettingsStateComponent =
                ProjectSettingsStateComponent.getInstance(project);
        final ProjectSettingsState projectSettingsState = projectSettingsStateComponent.getState();

        projectSettingsState.zigExecutableLocation = uiComponent.getZigExecutableLocation();
        projectSettingsState.isInfoMessagesSuppressed = uiComponent.isZLSInfoMessagesSuppressed();

        final String newZLSExecutableLocation = uiComponent.getZLSExecutableLocation();
        if (newZLSExecutableLocation.equals(projectSettingsState.zlsExecutableLocation)) {
            return;
        }
        projectSettingsState.zlsExecutableLocation = newZLSExecutableLocation;
        projectSettingsStateComponent.applyZLSExecutableLocationChange();
    }

    @Override
    public void reset() {
        final ProjectSettingsState projectSettingsState = ProjectSettingsStateComponent.getInstance(project).getState();
        uiComponent.setZigExecutableLocationText(projectSettingsState.zigExecutableLocation);
        uiComponent.setZLSExecutableLocationText(projectSettingsState.zlsExecutableLocation);
        uiComponent.setZLSInfoMessagesSuppressed(projectSettingsState.isInfoMessagesSuppressed);
    }

    @Override
    public void disposeUIResources() {
        uiComponent = null;
    }

    @Override
    public JComponent getPreferredFocusedComponent() {
        return uiComponent == null ? null : uiComponent.getPreferredFocusedComponent();
    }

}
