package editor.settings;

import com.intellij.openapi.components.*;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.project.Project;
import editor.languageserver.ZLSManager;
import org.jetbrains.annotations.NotNull;
import util.Command;

import static editor.settings.ProjectSettingsStateComponent.ProjectSettingsState;

@Service
@State(name = "ProjectSettingsState",
        storages = { @Storage(value = "ZigPluginSettings.xml", roamingType = RoamingType.DISABLED) })
public final class ProjectSettingsStateComponent implements PersistentStateComponent<ProjectSettingsState> {

    public static class ProjectSettingsState {

        @NotNull
        public String zigExecutableLocation;
        @NotNull
        public String zlsExecutableLocation;
        public boolean isInfoMessagesSuppressed;

        public ProjectSettingsState() {
            // attempt to find the paths of the executables as default values
            zigExecutableLocation = Command.getCommandLocationOrEmpty("zig");
            zlsExecutableLocation = Command.getCommandLocationOrEmpty("zls");
            isInfoMessagesSuppressed = true;
        }

    }

    @NotNull
    public static ProjectSettingsStateComponent getInstance(final Project project) {
        return project.getService(ProjectSettingsStateComponent.class);
    }

    @NotNull
    private final Project project;

    @NotNull
    public ProjectSettingsState projectSettingsState = new ProjectSettingsState();

    public ProjectSettingsStateComponent(@NotNull final Project project) {
        this.project = project;
    }

    @NotNull
    @Override
    public ProjectSettingsState getState() {
        return projectSettingsState;
    }

    @Override
    public void loadState(@NotNull final ProjectSettingsState projectSettingsState) {
        this.projectSettingsState = projectSettingsState;
    }

    public void applyZLSExecutableLocationChange() throws ConfigurationException {
        ZLSManager.setZLSExecutableLocation(project, projectSettingsState.zlsExecutableLocation);
    }

}
