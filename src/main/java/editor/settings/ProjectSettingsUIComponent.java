package editor.settings;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.TextFieldWithBrowseButton;
import com.intellij.ui.components.*;
import com.intellij.util.ui.FormBuilder;
import javax.swing.*;
import org.jetbrains.annotations.NotNull;
import util.*;

public class ProjectSettingsUIComponent {

    private final JPanel rootComponent;
    private final TextFieldWithBrowseButton zigExecutableLocationTextField;
    private final TextFieldWithBrowseButton zlsExecutableLocationTextField;
    private final JBCheckBox isZLSInfoMessagesSuppressedCheckbox;

    public ProjectSettingsUIComponent(final Project project) {
        zigExecutableLocationTextField = UIUtils.createNonEmptyTextFieldWithBrowseButton(project, "Zig executable location");
        zlsExecutableLocationTextField = UIUtils.createTextFieldWithBrowseButton(project);
        isZLSInfoMessagesSuppressedCheckbox = new JBCheckBox("Suppress ZLS info messages", true);

        rootComponent = FormBuilder.createFormBuilder()
                .addLabeledComponent(new JBLabel("Zig executable location:"), zigExecutableLocationTextField)
                .addTooltip("Location of Zig executable file. Required.")
                .addLabeledComponent(new JBLabel("ZLS executable location:"), zlsExecutableLocationTextField)
                .addTooltip("Location of Zig Language Server executable file. Optional. Some plugin features will not" +
                        " be available if not provided.")
                .addComponent(isZLSInfoMessagesSuppressedCheckbox)
                .addTooltip("When enabled, no notifications for info or log messages from the Zig Language Server " +
                        "will be displayed.")
                .addComponentFillVertically(new JPanel(), 0)
                .getPanel();
    }

    @NotNull
    public JPanel getRootComponent() {
        return rootComponent;
    }

    @NotNull
    public String getZigExecutableLocation() {
        return zigExecutableLocationTextField.getText();
    }

    public void setZigExecutableLocationText(final String zigExecutableLocation) {
        zigExecutableLocationTextField.setText(zigExecutableLocation);
    }

    @NotNull
    public String getZLSExecutableLocation() {
        return zlsExecutableLocationTextField.getText();
    }

    public void setZLSExecutableLocationText(final String zlsExecutableLocation) {
        zlsExecutableLocationTextField.setText(zlsExecutableLocation);
    }

    public boolean isZLSInfoMessagesSuppressed() {
        return isZLSInfoMessagesSuppressedCheckbox.isSelected();
    }

    public void setZLSInfoMessagesSuppressed(final boolean isInfoMessagesSuppressed) {
        isZLSInfoMessagesSuppressedCheckbox.setSelected(isInfoMessagesSuppressed);
    }

    public JComponent getPreferredFocusedComponent() {
        return zigExecutableLocationTextField;
    }

}
