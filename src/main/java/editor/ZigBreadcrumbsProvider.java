package editor;

import com.intellij.lang.Language;
import com.intellij.psi.PsiElement;
import com.intellij.ui.breadcrumbs.BreadcrumbsProvider;
import language.ZigLanguage;
import language.psi.*;
import org.jetbrains.annotations.NotNull;

public class ZigBreadcrumbsProvider implements BreadcrumbsProvider {

    private static final Language[] LANGUAGES = new Language[] { ZigLanguage.INSTANCE };

    private static PsiElement getThirdChild(final PsiElement element) {
        final PsiElement fnConstOrVarKeyword = element.getFirstChild();
        if (fnConstOrVarKeyword == null) {
            return null;
        }
        final PsiElement whitespace = fnConstOrVarKeyword.getNextSibling();
        if (whitespace == null) {
            return null;
        }
        return whitespace.getNextSibling();
    }

    private static boolean acceptTopLevelDeclaration(final PsiElement element) {
        final PsiElement identifier = getThirdChild(element);
        return identifier != null && identifier.getNode().getElementType() == ZigTypes.IDENTIFIER;
    }

    private static String getTopLevelDeclarationElementInfo(final PsiElement element) {
        return element.getFirstChild().getNextSibling().getNextSibling().getText();
    }

    @Override
    public Language[] getLanguages() {
        return LANGUAGES;
    }

    @Override
    public boolean acceptElement(@NotNull final PsiElement element) {
        if (element instanceof ZigTestDeclaration) {
            return getThirdChild(element) != null;
        } else if (element instanceof ZigTopLevelComptimeBlock) {
            return true;
        } else if (element instanceof ZigTopLevelVariableDeclaration) {
            return acceptTopLevelDeclaration(((ZigTopLevelVariableDeclaration) element).getVariableDeclaration());
        } else if (element instanceof ZigFunctionDeclaration) {
            return acceptTopLevelDeclaration(((ZigFunctionDeclaration) element).getFunctionDeclarationPrototype());
        } else if (element instanceof ZigContainerField) {
            final PsiElement comptimeKeywordOrIdentifier = element.getFirstChild();
            if (comptimeKeywordOrIdentifier == null) {
                return false;
            }
            if (comptimeKeywordOrIdentifier.getNode().getElementType() == ZigTypes.IDENTIFIER) {
                return true;
            }
            final PsiElement whitespace = comptimeKeywordOrIdentifier.getNextSibling();
            if (whitespace == null) {
                return false;
            }
            final PsiElement identifierOrOther = whitespace.getNextSibling();
            return identifierOrOther != null && identifierOrOther.getNode().getElementType() == ZigTypes.IDENTIFIER;
        }
        return false;
    }

    @Override
    public @NotNull String getElementInfo(@NotNull final PsiElement element) {
        // Note: this function assumes that the psi tree has not changed since it was validated in an
        // acceptElement(PsiElement) call
        if (element instanceof ZigTestDeclaration) {
            return "test " + element.getFirstChild().getNextSibling().getNextSibling().getText();
        } else if (element instanceof ZigTopLevelComptimeBlock) {
            return "comptime { ... }";
        } else if (element instanceof ZigTopLevelVariableDeclaration) {
            return getTopLevelDeclarationElementInfo(((ZigTopLevelVariableDeclaration) element).getVariableDeclaration());
        } else if (element instanceof ZigFunctionDeclaration) {
            return getTopLevelDeclarationElementInfo(((ZigFunctionDeclaration) element).getFunctionDeclarationPrototype()) + "()";
        } else if (element instanceof ZigContainerField) {
            // return container field's identifier
            final PsiElement comptimeKeywordOrIdentifier = element.getFirstChild();
            return comptimeKeywordOrIdentifier.getNode().getElementType() == ZigTypes.COMPTIME_KEYWORD
                    ? comptimeKeywordOrIdentifier.getNextSibling().getNextSibling().getText() :
                    comptimeKeywordOrIdentifier.getText();
        }
        throw new IllegalStateException("Element of non-accepted class given to breadcrumb provider!");
    }

}
