package editor.runconfiguration;

import com.intellij.execution.configurations.*;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;

/**
 * A simple {@link ConfigurationFactory} implementation that creates {@link ZigRunConfiguration}s
 */
public class ZigRunConfigurationFactory extends ConfigurationFactory {

    /**
     * The ID used for serialization of {@code RunConfiguration}s created by this factory
     */
    private static final String ZIG_RUN_CONFIGURATION_FACTORY_ID = "ZIG_RUN_CONFIGURATION_FACTORY_ID";

    /**
     * Creates a {@code ZigRunConfigurationFactory} with the given run configuration type.
     *
     * @param zigRunConfigurationType a {@code ZigRunConfigurationType} instance
     */
    public ZigRunConfigurationFactory(final ZigRunConfigurationType zigRunConfigurationType) {
        super(zigRunConfigurationType);
    }

    @Override
    public @NotNull RunConfiguration createTemplateConfiguration(@NotNull final Project project) {
        return new ZigRunConfiguration(project, this);
    }

    @Override
    public @NotNull String getId() {
        return ZIG_RUN_CONFIGURATION_FACTORY_ID;
    }

}
