package editor.runconfiguration;

import com.intellij.openapi.options.SettingsEditor;
import com.intellij.openapi.ui.TextFieldWithBrowseButton;
import java.awt.GridLayout;
import java.nio.file.Paths;
import javax.swing.*;
import org.jetbrains.annotations.NotNull;

/**
 * A Swing GUI implementation that allows the user access to configure the two options for a {@link ZigRunConfiguration}
 * (the project build file and if the resulting executable should be run).
 */
public class ZigRunConfigurationEditor extends SettingsEditor<ZigRunConfiguration> {

    private TextFieldWithBrowseButton buildFileTextField;

    @Override
    protected void resetEditorFrom(@NotNull final ZigRunConfiguration runConfiguration) {
        buildFileTextField.setText(ZigRunConfiguration.getDefaultProjectBuildFilePath(runConfiguration.getProject()).toString());
    }

    @Override
    protected void applyEditorTo(@NotNull final ZigRunConfiguration runConfiguration) {
        runConfiguration.buildFilePath = Paths.get(buildFileTextField.getText());
    }

    @Override
    protected @NotNull JComponent createEditor() {
        final JPanel rootPanel = new JPanel(new GridLayout(2, 2));

        rootPanel.add(new JLabel("Build file:"));
        rootPanel.add(buildFileTextField = new TextFieldWithBrowseButton());

        return rootPanel;
    }

}
