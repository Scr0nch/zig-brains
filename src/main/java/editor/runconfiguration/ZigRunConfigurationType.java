package editor.runconfiguration;

import com.intellij.execution.configurations.ConfigurationTypeBase;
import editor.ZigIcons;

/**
 * Registers the one-and-only Zig build type and currently the only associated RunConfigurationFactory implementation,
 * {@link ZigRunConfigurationFactory}, which creates {@link ZigRunConfiguration}s
 */
public class ZigRunConfigurationType extends ConfigurationTypeBase {

    public static final ZigRunConfigurationType INSTANCE = new ZigRunConfigurationType();

    /**
     * Creates a ConfigurationType with Zig's ID, name, description, icon, and {@code RunConfigurationFactory}
     * implementation, {@link ZigRunConfigurationFactory}.
     */
    public ZigRunConfigurationType() {
        super("ZIG_RUN_CONFIGURATION_ID", "Zig", "Build and/or Run Zig Executable", ZigIcons.LANGUAGE_ICON);
        addFactory(new ZigRunConfigurationFactory(this));
    }

    public ZigRunConfigurationFactory getFactory() {
        return (ZigRunConfigurationFactory) getConfigurationFactories()[0];
    }

}
