package editor.runconfiguration;

import com.intellij.execution.*;
import com.intellij.execution.configurations.*;
import com.intellij.execution.process.*;
import com.intellij.execution.runners.ProgramRunner;
import com.intellij.execution.ui.ConsoleView;
import java.io.File;
import org.jetbrains.annotations.*;

/**
 * Handles the execution of a {@link ZigRunConfiguration}. On execution, opens a console in the project window and
 * attaches its output to it. Depending on the run configuration's settings, the project is built or built and the
 * resulting executable is executed.
 */
public class ZigRunProfileState implements RunProfileState {

    /**
     * The command needed to build a Zig project with the Zig build system. Use {@code zig --help} for more
     * information.
     */
    private static final String[] BUILD_COMMAND = new String[] { "zig", "build" };

    /**
     * The command needed to build and run the executable created by the Zig build system. Use {@code zig --help} for
     * more information.
     */
    private static final String[] RUN_COMMAND = new String[] { "zig", "build", "run" };

    private final ConsoleView consoleView;
    private final ZigRunConfiguration runConfiguration;

    /**
     * Creates a {@code ZigRunProfileState} with the specified console to output to and run configuration to execute.
     *
     * @param consoleView      the console where the build and executable output should be directed
     * @param runConfiguration the run configuration to run
     */
    public ZigRunProfileState(final ConsoleView consoleView, final ZigRunConfiguration runConfiguration) {
        this.consoleView = consoleView;
        this.runConfiguration = runConfiguration;
    }

    @Override
    public @Nullable ExecutionResult execute(final Executor executor, @NotNull final ProgramRunner<?> runner) throws ExecutionException {
        // BUILD_COMMAND or RUN_COMMAND must be run inside the build file's parent directory
        final File buildFileParentDirectory = runConfiguration.buildFilePath.getParent().toFile();

        // assume zig is on PATH
        final OSProcessHandler processHandler =
                new OSProcessHandler(new GeneralCommandLine(runConfiguration.isBuildOnly ? BUILD_COMMAND :
                        RUN_COMMAND).withWorkDirectory(buildFileParentDirectory));

        consoleView.attachToProcess(processHandler);
        ProcessTerminatedListener.attach(processHandler);
        processHandler.startNotify();

        return new DefaultExecutionResult(consoleView, processHandler);
    }

}
