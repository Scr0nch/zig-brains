package editor.runconfiguration;

import com.intellij.execution.Executor;
import com.intellij.execution.configurations.*;
import com.intellij.execution.filters.TextConsoleBuilderFactory;
import com.intellij.execution.runners.ExecutionEnvironment;
import com.intellij.execution.ui.ConsoleView;
import com.intellij.openapi.options.SettingsEditor;
import com.intellij.openapi.project.*;
import com.intellij.openapi.util.InvalidDataException;
import com.intellij.psi.search.GlobalSearchScope;
import java.nio.file.*;
import org.jdom.Element;
import org.jetbrains.annotations.*;

/**
 * A {@code RunConfiguration} implementation that executes a projects {@code build.zig} file (configured with {@link
 * ZigRunConfiguration#buildFilePath}) and, depending on the {@link ZigRunConfiguration#isBuildOnly} value, runs the
 * resulting executable.
 */
public class ZigRunConfiguration extends LocatableConfigurationBase<Object> {

    /**
     * By default, a new Zig project (created with {@code zig init-exe} or {@code zig init-lib}) creates the {@code
     * build.zig} file in the project's root directory.
     */
    static final String DEFAULT_PROJECT_RELATIVE_BUILD_FILE_PATH = "build.zig";
    static final boolean DEFAULT_BUILD_ONLY = false;

    /**
     * Serialization name constant for {@link ZigRunConfiguration#buildFilePath}.
     */
    private static final String BUILD_FILE_PATH_NAME = "buildFilePath";

    /**
     * The path to the project's build file ({@code build.zig}), by default is {@link
     * ZigRunConfiguration#DEFAULT_PROJECT_RELATIVE_BUILD_FILE_PATH}.
     */
    Path buildFilePath;

    /**
     * Serialization name constant for {@link ZigRunConfiguration#isBuildOnly}.
     */
    private static final String IS_BUILD_ONLY_NAME = "isBuildOnly";

    /**
     * {@code true} if the project should only be built using {@code zig build}, {@code false} if the resulting
     * executable from the build process should be run after building using {@code zig build run}
     */
    boolean isBuildOnly;

    protected ZigRunConfiguration(final Project project, final ConfigurationFactory factory) {
        super(project, factory);

        buildFilePath = getDefaultProjectBuildFilePath(project);
        isBuildOnly = DEFAULT_BUILD_ONLY;
    }

    public void setBuildOnly(final boolean isBuildOnly) {
        this.isBuildOnly = isBuildOnly;
    }

    public void setBuildFilePath(final Path buildFilePath) {
        this.buildFilePath = buildFilePath;
    }

    public Path getBuildFilePath() {
        return buildFilePath;
    }

    @Override
    @NotNull
    public SettingsEditor<? extends RunConfiguration> getConfigurationEditor() {
        return new ZigRunConfigurationEditor();
    }

    @Override
    @Nullable
    public RunProfileState getState(@NotNull final Executor executor,
                                    @NotNull final ExecutionEnvironment environment) {
        final Project project = getProject();
        final ConsoleView consoleView = TextConsoleBuilderFactory.getInstance().createBuilder(project,
                GlobalSearchScope.projectScope(project)).getConsole();
        return new ZigRunProfileState(consoleView, this);
    }

    @Override
    public void writeExternal(@NotNull final Element element) {
        super.writeExternal(element);
        // Note: using the preferred API XmlSerializer is not possible in this class because it requires a default
        // constructor. Creating a default constructor would require inference of the required @NotNull parameters which
        // would likely be incorrect and wasteful.
        element.setAttribute(BUILD_FILE_PATH_NAME, String.valueOf(this.buildFilePath));
        element.setAttribute(IS_BUILD_ONLY_NAME, String.valueOf(this.isBuildOnly));
    }

    @Override
    public void readExternal(@NotNull final Element element) throws InvalidDataException {
        super.readExternal(element);

        final String buildFilePath = element.getAttributeValue(BUILD_FILE_PATH_NAME);
        if (buildFilePath != null) {
            this.buildFilePath = Paths.get(buildFilePath);
        }
        this.isBuildOnly = Boolean.parseBoolean(element.getAttributeValue("isBuildOnly"));
    }

    /**
     * Resolves the path to the given project's Zig build file assuming that the project is one created from either a
     * {@code zig init-exe} or {@code zig init-lib} command.
     *
     * @param project the project that contains the build file.
     * @return the path to the given project's Zig build file.
     * @throws IllegalArgumentException if the given project is the default project returned from {@link
     *                                  ProjectManager#getDefaultProject()}
     */
    @NotNull
    static Path getDefaultProjectBuildFilePath(final @NotNull Project project) {
        final String projectBasePath = project.getBasePath();
        if (projectBasePath == null) {
            throw new IllegalArgumentException("Argument \"project\" must not be the default project!");
        }
        return Paths.get(projectBasePath).resolve(DEFAULT_PROJECT_RELATIVE_BUILD_FILE_PATH);
    }

}
