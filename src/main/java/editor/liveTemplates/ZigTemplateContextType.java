package editor.liveTemplates;

import com.intellij.codeInsight.template.*;
import com.intellij.openapi.util.NlsContexts;
import com.intellij.psi.PsiElement;
import language.*;
import org.jetbrains.annotations.*;

class ZigTemplateContextType extends TemplateContextType {

    protected ZigTemplateContextType(@NotNull @NonNls final String id,
                                     @NlsContexts.Label @NotNull final String presentableName) {
        super(id, presentableName);
    }

    @Override
    public boolean isInContext(@NotNull final TemplateActionContext templateActionContext) {
        return templateActionContext.getFile().getFileType() == ZigFileType.INSTANCE &&
                !ZigParserDefinition.COMMENT_TOKEN_SET.contains(
                        getElementInContext(templateActionContext).getNode().getElementType());
    }

    protected static PsiElement getElementInContext(@NotNull final TemplateActionContext templateActionContext) {
        return templateActionContext.getFile().findElementAt(templateActionContext.getStartOffset());
    }

}
