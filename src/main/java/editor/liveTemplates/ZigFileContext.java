package editor.liveTemplates;

class ZigFileContext extends ZigTemplateContextType {

    protected ZigFileContext() {
        super("ZIG_FILE_CONTEXT_ID", "Zig");
    }

}
