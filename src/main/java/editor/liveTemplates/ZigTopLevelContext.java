package editor.liveTemplates;

import com.intellij.codeInsight.template.TemplateActionContext;
import com.intellij.psi.PsiElement;
import language.psi.*;
import org.jetbrains.annotations.NotNull;

class ZigTopLevelContext extends ZigTemplateContextType {

    protected ZigTopLevelContext() {
        super("ZIG_TOP_LEVEL_CONTEXT_ID", "Zig top level");
    }

    @Override
    public boolean isInContext(@NotNull TemplateActionContext templateActionContext) {
        if (!super.isInContext(templateActionContext)) {
            return false;
        }

        // FIXME: parent element is often a zig file when it should not be because the parser cannot recover from errors
        final PsiElement element = getElementInContext(templateActionContext);
        if (element == null) {
            return false;
        }

        final PsiElement parent = element.getParent();
        return parent instanceof ZigContainerField || parent instanceof ZigFile;
    }

}
