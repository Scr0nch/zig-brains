package editor;

import com.intellij.psi.*;
import com.intellij.spellchecker.tokenizer.*;
import language.psi.*;
import org.jetbrains.annotations.NotNull;

public class ZigSpellcheckingStrategy extends SpellcheckingStrategy {

    private static final Tokenizer<PsiElement> EMPTY_TOKENIZER = new Tokenizer<>() {

        @Override
        public void tokenize(@NotNull PsiElement element, TokenConsumer consumer) {}

    };

    @Override
    public @NotNull Tokenizer<PsiElement> getTokenizer(final PsiElement element) {
        if (element instanceof PsiComment || element instanceof ZigStringLiteral) {
            return TEXT_TOKENIZER;
        }
        if (element.getNode().getElementType() == ZigTypes.IDENTIFIER) {
            final PsiElement parent = element.getParent();
            if (parent instanceof ZigFunctionPrototype || parent instanceof ZigVariableDeclaration
                    || parent instanceof ZigContainerField || parent instanceof ZigAssemblyInputItem
                    || parent instanceof ZigAssemblyOutputItem || parent instanceof ZigBreakLabel
                    || parent instanceof ZigBlockLabel || parent instanceof ZigParameterDeclaration
                    || parent instanceof ZigPayload || parent instanceof ZigPointerPayload
                    || parent instanceof ZigPointerIndexPayload || parent instanceof ZigIdentifierList) {
                return TEXT_TOKENIZER;
            }
            if (parent instanceof ZigPrimaryTypeExpression) {
                final PsiElement errorKeyword = parent.getFirstChild();
                if (errorKeyword == null) {
                    return EMPTY_TOKENIZER;
                }
                if (errorKeyword.getNode().getElementType() == ZigTypes.ERROR_KEYWORD) {
                    return TEXT_TOKENIZER;
                }
                return EMPTY_TOKENIZER;
            }
        }
        return EMPTY_TOKENIZER;
    }

}
