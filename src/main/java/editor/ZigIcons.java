package editor;

import com.intellij.openapi.util.IconLoader;
import javax.swing.Icon;
import org.jetbrains.annotations.NotNull;

/**
 * A utility constants class that defines all of the icons located in the icons folder.
 */
public final class ZigIcons {

    public static final @NotNull Icon LANGUAGE_ICON = IconLoader.getIcon("/icons/languageIcon.svg", ZigIcons.class);
    public static final @NotNull Icon LANGUAGE_ICON_WITH_NAME =
            IconLoader.getIcon("/icons/languageIconWithName.svg", ZigIcons.class);
    public static final @NotNull Icon FILE_ICON = IconLoader.getIcon("/icons/fileIcon.svg", ZigIcons.class);

    private ZigIcons() {}

}
