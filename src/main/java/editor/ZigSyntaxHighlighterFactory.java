package editor;

import com.intellij.openapi.fileTypes.*;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.*;

/**
 * A simple {@code SyntaxHighlighterFactory} implementation that always returns a {@link ZigSyntaxHighlighter}
 * instance.
 */
public class ZigSyntaxHighlighterFactory extends SyntaxHighlighterFactory {

    @Override
    public @NotNull SyntaxHighlighter getSyntaxHighlighter(@Nullable final Project project,
                                                           @Nullable final VirtualFile virtualFile) {
        return new ZigSyntaxHighlighter();
    }

}
