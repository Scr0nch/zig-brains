package editor;

import com.intellij.codeInspection.ProblemHighlightType;
import com.intellij.lang.annotation.*;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.psi.PsiElement;
import com.intellij.psi.impl.source.tree.LeafPsiElement;
import com.intellij.psi.tree.IElementType;
import editor.actions.*;
import language.ZigParserDefinition;
import language.psi.*;
import org.jetbrains.annotations.NotNull;
import util.PsiUtils;

/**
 * An {@code Annotator} implementation that aids in highlighting identifiers and registering quick fixes and some errors
 * relevant in a Zig source code file.
 */
public class ZigAnnotator implements Annotator {

    private static void highlightElement(@NotNull final PsiElement element,
                                         @NotNull final AnnotationHolder annotationHolder,
                                         @NotNull final TextAttributesKey highlight) {
        annotationHolder.newSilentAnnotation(HighlightSeverity.INFORMATION).range(element).textAttributes(highlight).create();
    }

    private static boolean isConstant(@NotNull final PsiElement elementParent) {
        return elementParent.getFirstChild().getNode().getElementType() == ZigTypes.CONST_KEYWORD;
    }

    @Override
    public void annotate(@NotNull final PsiElement element, @NotNull final AnnotationHolder holder) {
        if (element instanceof LeafPsiElement) {
            final IElementType elementType = ((LeafPsiElement) element).getElementType();
            if (elementType == ZigTypes.IDENTIFIER) {
                final PsiElement parent = element.getParent();
                if (parent instanceof ZigFunctionDeclarationPrototype) {
                    // element is a function identifier
                    highlightElement(element, holder, ZigSyntaxHighlighter.FUNCTION_DECLARATION);
                } else if (element.getNextSibling() instanceof ZigFunctionCallArguments) {
                    // element is a function call
                    highlightElement(element, holder, ZigSyntaxHighlighter.FUNCTION_CALL);
                } else if (parent instanceof ZigVariableDeclaration) {
                    // an element is a structure identifier if the first child (of the getChildren() call, NOT
                    // getFirstChild())'s
                    // first child is a ZigContainerDeclaration
                    // Note: it appears that in the generated/given PsiElement#getChildren() implementation, leaf
                    // elements are not included
                    final PsiElement[] children = parent.getChildren();
                    if (children.length > 0 && children[0].getFirstChild() instanceof ZigContainerDeclaration) {
                        highlightElement(element, holder, ZigSyntaxHighlighter.STRUCTURE);
                        return;
                    }

                    // highlight the element depending on the type of its parent element. If its parent is a
                    // ZigStatement, it musts reside within a block and is a local variable. Otherwise, if it is inside
                    // a ZigTopLevelDeclaration, it is a global variable or a structure variable/constant.
                    final PsiElement parentParent = parent.getParent();
                    if (parentParent instanceof ZigStatement) {
                        highlightElement(element, holder, isConstant(parent) ? ZigSyntaxHighlighter.LOCAL_CONSTANT :
                                ZigSyntaxHighlighter.LOCAL_VARIABLE);
                        return;
                    } else if (parentParent instanceof ZigTopLevelVariableDeclaration) {
                        // parent 1                  parent 2                  parent 3               parent 4
                        // ZigVariableDeclaration <- ZigTopLevelVariableDeclaration <- ZigFile = Global Variable
                        //                                                          \- ZigContainerDeclaration
                        //                                                                 = Structure Variable
                        if (parentParent.getParent() instanceof ZigContainerDeclaration) {
                            highlightElement(element, holder, isConstant(parent) ?
                                    ZigSyntaxHighlighter.STRUCTURE_CONSTANT : ZigSyntaxHighlighter.STRUCTURE_FIELD);
                            return;
                        }
                        highlightElement(element, holder, ZigSyntaxHighlighter.GLOBAL_VARIABLE);
                        return;
                    }

                    // this should never happen assuming that the file was parsed correctly
                    holder.newAnnotation(HighlightSeverity.ERROR, "Invalid variable declaration!").range(element).create();
                } else if (parent instanceof ZigParameterDeclaration) {
                    highlightElement(element, holder, ZigSyntaxHighlighter.PARAMETER);
                }
            } else if (elementType == ZigTypes.BUILTIN_IDENTIFIER) {
                // check that the built in function exists; remove the "@" symbol for comparison (the first character)
                final String builtinFunctionName = element.getText().substring(1);
                if (!ZigParserDefinition.BUILTIN_FUNCTION_IDENTIFIERS.contains(builtinFunctionName)) {
                    holder.newAnnotation(HighlightSeverity.ERROR, "Invalid builtin function")
                            .highlightType(ProblemHighlightType.LIKE_UNKNOWN_SYMBOL).range(element)
                            .withFix(new ZigConvertBuiltinFunctionIntentionAction(builtinFunctionName, element)).create();
                }
            }
        } else if (element instanceof ZigIfPrefix) {
            final PsiElement parent = element.getParent();
            final boolean isStatement = parent instanceof ZigIfStatement;
            // ifPrefix ::= "if" whitespace? "(" whitespace? expression
            final PsiElement thirdChild = element.getFirstChild().getNextSibling().getNextSibling();
            final PsiElement expression =
                    PsiUtils.getNextSiblingIfWhitespace(thirdChild.getNode().getElementType() == ZigTypes.LEFT_PARENTHESIS ?
                            thirdChild.getNextSibling() : thirdChild);

            // the primaryTypeExpression contains the PsiElement with an element type that can be TRUE_KEYWORD or
            // FALSE_KEYWORD
            final IElementType elementType = expression.getFirstChild().getNode().getElementType();
            // add a warning with a quick fix if the if statement contains only a boolean literal
            // TODO attempt to evaluate if the if expression is a constant
            if (elementType == ZigTypes.TRUE_KEYWORD) {
                holder.newAnnotation(HighlightSeverity.WARNING, "Condition is always true").range(expression)
                        .highlightType(ProblemHighlightType.WARNING)
                        .withFix(new ZigUnwrapIfIntentionAction(parent, true, isStatement)).create();
            } else if (elementType == ZigTypes.FALSE_KEYWORD) {
                final AnnotationBuilder builder = holder.newAnnotation(HighlightSeverity.WARNING, "Condition is " +
                        "always false").range(expression)
                        .highlightType(ProblemHighlightType.WARNING);
                if (isStatement || (PsiUtils.getPreviousNonWhitespaceSibling(parent).getNode().getElementType() == ZigTypes.ELSE_KEYWORD
                        || PsiUtils.getNextNonWhitespaceSibling(PsiUtils.getNextNonWhitespaceSibling(parent.getFirstChild())) != null)) {
                    // all false if statements and expressions can be unwrapped except expressions that are not part of
                    // an if-else chain
                    builder.withFix(new ZigUnwrapIfIntentionAction(parent, false, isStatement)).create();
                } else {
                    builder.create();
                }
            }
        }
    }

}
