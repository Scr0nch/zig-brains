package editor;

import com.intellij.lang.ASTNode;
import com.intellij.lang.folding.*;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.project.DumbAware;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiTreeUtil;
import java.util.*;
import language.psi.ZigBlock;
import org.jetbrains.annotations.*;

public class ZigFoldingBuilder extends FoldingBuilderEx implements DumbAware {

    @NotNull
    @Override
    public FoldingDescriptor @NotNull [] buildFoldRegions(@NotNull final PsiElement root,
                                                          @NotNull final Document document,
                                                          final boolean quick) {
        if (quick) {
            // don't slow down startup with finding blocks to fold; this method will be called again after startup is
            // complete so complex processing can be done.
            return new FoldingDescriptor[0];
        }

        // create a folding descriptor for each block
        final ZigBlock[] blockElements =
                PsiTreeUtil.collectElementsOfType(root, ZigBlock.class).toArray(new ZigBlock[0]);
        final List<FoldingDescriptor> foldingDescriptors = new ArrayList<>(blockElements.length);

        for (int i = 0, length = blockElements.length; i < length; ++i) {
            final ZigBlock blockElement = blockElements[i];
            if (blockElement.getStatementList().isEmpty()) {
                // don't allow the block to be folded if there is nothing in it
                continue;
            }

            // each folding descriptor's element is the block, but it is the block's statements that are folded
            final PsiElement firstStatement = blockElement.getFirstChild().getNextSibling();
            final PsiElement lastStatement = blockElement.getLastChild().getPrevSibling();
            foldingDescriptors.add(new FoldingDescriptor(blockElement, new TextRange(
                    firstStatement.getTextRange().getStartOffset(),
                    lastStatement.getTextRange().getEndOffset())));
        }

        return foldingDescriptors.toArray(new FoldingDescriptor[0]);
    }

    @Override
    public @Nullable String getPlaceholderText(@NotNull final ASTNode node) {
        return "...";
    }

    @Override
    public boolean isCollapsedByDefault(@NotNull final ASTNode node) {
        return false;
    }

}
