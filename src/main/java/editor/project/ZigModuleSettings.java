package editor.project;

class ZigModuleSettings {

    public final String zigExecutableLocation;
    public final String zlsExecutableLocation;
    public final boolean isProjectExecutable;

    public ZigModuleSettings(final String zigExecutableLocation, final String zlsExecutableLocation,
                             final boolean isProjectExecutable) {
        this.zigExecutableLocation = zigExecutableLocation;
        this.zlsExecutableLocation = zlsExecutableLocation;
        this.isProjectExecutable = isProjectExecutable;
    }

}
