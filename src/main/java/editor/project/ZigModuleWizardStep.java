package editor.project;

import org.jetbrains.annotations.NotNull;

class ZigModuleWizardStep extends AbstractZigModuleWizardStep {

    @NotNull
    private final ZigModuleBuilder builder;

    public ZigModuleWizardStep(@NotNull final ZigModuleBuilder builder) {
        this.builder = builder;
    }

    @Override
    public void updateDataModel() {
        builder.setModuleSettings(new ZigModuleSettings(zigExecutableLocationTextField.getText().trim(),
                zlsExecutableLocationTextField.getText().trim(), isExecutableButton.isSelected()));
    }

}
