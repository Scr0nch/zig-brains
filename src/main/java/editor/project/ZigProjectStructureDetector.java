package editor.project;

import com.intellij.ide.util.importProject.*;
import com.intellij.ide.util.projectWizard.ModuleWizardStep;
import com.intellij.ide.util.projectWizard.importSources.*;
import java.io.File;
import java.nio.file.*;
import java.util.*;
import javax.swing.Icon;
import org.jetbrains.annotations.NotNull;

class ZigProjectStructureDetector extends ProjectStructureDetector {

    @Override
    public @NotNull DirectoryProcessingResult detectRoots(@NotNull final File currentDirectory,
                                                          @NotNull final File @NotNull [] children,
                                                          @NotNull final File baseDirectory,
                                                          @NotNull final List<DetectedProjectRoot> result) {
        boolean isBuildFileFound = false;
        for (int i = 0, length = children.length; i < length; ++i) {
            final File child = children[i];
            if (child.getName().equals("build.zig")) {
                // the root file should contain a valid zig project
                result.add(new ZigDetectedProjectRoot(currentDirectory));
                isBuildFileFound = true;
            } else if (child.getName().equals("src") && child.isDirectory()) {
                result.add(new ZigDetectedSourceRoot(children[i]));
            }
        }
        return isBuildFileFound ? DirectoryProcessingResult.SKIP_CHILDREN : DirectoryProcessingResult.PROCESS_CHILDREN;
    }

    @Override
    public void setupProjectStructure(@NotNull final Collection<DetectedProjectRoot> roots,
                                      @NotNull final ProjectDescriptor projectDescriptor,
                                      @NotNull final ProjectFromSourcesBuilder builder) {
        List<ModuleDescriptor> modules = projectDescriptor.getModules();
        if (modules.isEmpty()) {
            final Iterator<DetectedProjectRoot> rootIterator = roots.iterator();
            File rootDirectory = null;
            Path sourceDirectory = null;
            while (rootIterator.hasNext()) {
                final DetectedProjectRoot root = rootIterator.next();
                final File directory = root.getDirectory();
                switch (root.getRootTypeName()) {
                    case ZigDetectedProjectRoot.ROOT_TYPE_NAME:
                        rootDirectory = directory;
                        break;
                    case ZigDetectedSourceRoot.ROOT_TYPE_NAME:
                        sourceDirectory = directory.toPath();
                        break;
                    default:
                        throw new IllegalStateException("Detected project root of unknown type found!");
                }
            }

            if (rootDirectory == null) {
                throw new IllegalStateException("No root directory found in detected project roots!");
            }

            final DetectedSourceRoot sourceRoot = sourceDirectory != null && Files.isDirectory(sourceDirectory) ?
                    new ZigDetectedSourceRoot(sourceDirectory.toFile()) : null;

            modules = new ArrayList<>();
            modules.add(new ModuleDescriptor(rootDirectory, ZigModuleType.getInstance(), sourceRoot));
            projectDescriptor.setModules(modules);
        }
    }

    @Override
    public List<ModuleWizardStep> createWizardSteps(final ProjectFromSourcesBuilder builder,
                                                    final ProjectDescriptor projectDescriptor, final Icon stepIcon) {
        return Collections.singletonList(
                new ZigDetectedModuleWizardStep(builder.getContext().getProjectBuilder(), projectDescriptor));
    }

    @Override
    public String getDetectorId() {
        return "Zig";
    }

}
