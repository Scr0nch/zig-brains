package editor.project;

import com.intellij.ide.util.projectWizard.importSources.DetectedSourceRoot;
import java.io.File;
import org.jetbrains.annotations.*;

class ZigDetectedSourceRoot extends DetectedSourceRoot {

    public static final String ROOT_TYPE_NAME = "Zig sources";

    public ZigDetectedSourceRoot(final File directory) {
        super(directory, null);
    }

    @Override
    public @NotNull @Nls(capitalization = Nls.Capitalization.Sentence) String getRootTypeName() {
        return ROOT_TYPE_NAME;
    }

}
