package editor.project;

import com.intellij.execution.*;
import com.intellij.ide.util.projectWizard.*;
import com.intellij.openapi.Disposable;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.*;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.*;
import com.intellij.openapi.vfs.VirtualFile;
import editor.runconfiguration.ZigRunConfigurationType;
import editor.settings.ProjectSettingsStateComponent;
import java.io.*;
import java.nio.file.Paths;
import org.jetbrains.annotations.*;
import util.Command;

class ZigModuleBuilder extends ModuleBuilder implements ModuleBuilderListener {

    private static final String ZIG_INIT_BUILD_ZIG_OVERWRITE_MESSAGE =
            "error: existing build.zig file would be overwritten\n";

    @Nullable
    private ZigModuleSettings moduleSettings;

    public ZigModuleBuilder() {
        addListener(this);
    }

    @Override
    public ModuleType<?> getModuleType() {
        return ZigModuleType.getInstance();
    }

    @Override
    public @Nullable ModuleWizardStep getCustomOptionsStep(final WizardContext context,
                                                           final Disposable parentDisposable) {
        return new ZigModuleWizardStep(this);
    }

    @Override
    public void setupRootModel(@NotNull final ModifiableRootModel rootModel) throws ConfigurationException {
        if (moduleSettings == null) {
            throw new IllegalStateException("Module settings cannot be null when setting up the root model!");
        }

        final ContentEntry contentRoot = doAddContentEntry(rootModel);
        if (contentRoot == null) {
            throw new ConfigurationException("Failed to create content root entry!");
        }

        final VirtualFile rootFile = contentRoot.getFile();
        if (rootFile == null) {
            throw new ConfigurationException("Failed to get the root file of the module's content root!");
        }

        // initialize zig project using its builtin command line utility; this has to be done before the src folder
        // created by this utility is marked as a source root
        final Command.Output output;
        try {
            output = Command.execute(new String[] {
                            Paths.get(moduleSettings.zigExecutableLocation).toAbsolutePath().toString(),
                            moduleSettings.isProjectExecutable ? "init-exe" : "init-lib" },
                    1000, new File(rootFile.getPath()));
        } catch (final IOException | InterruptedException e) {
            final ConfigurationException exception = new ConfigurationException("Failed to create zig project!");
            exception.initCause(e);
            throw exception;
        }

        // verify that the project was created successfully
        if (output != null && (ZIG_INIT_BUILD_ZIG_OVERWRITE_MESSAGE.equals(output.standardError))) {
            throw new ConfigurationException("Zig build file already exists at the given project location!");
        }

        final String errorMessage = completeRootModelSetup(rootModel);
        if (errorMessage != null) {
            throw new ConfigurationException(errorMessage);
        }
    }

    static String completeRootModelSetup(@NotNull final ModifiableRootModel rootModel) {
        // don't use the default SDK (in IntelliJ, a JDK)
        rootModel.setSdk(null);

        // mark the source directory as source files
        final ContentEntry[] contentEntries = rootModel.getContentEntries();
        if (contentEntries.length == 0) {
            return "Module has no content entries during root model setup!";
        }
        contentEntries[0].addSourceFolder(contentEntries[0].getUrl() + File.separator + "src", false);

        return null;
    }

    void setModuleSettings(@Nullable final ZigModuleSettings moduleSettings) {
        this.moduleSettings = moduleSettings;
    }

    @Override
    public void moduleCreated(@NotNull final Module module) {
        if (moduleSettings == null) {
            throw new IllegalStateException("ModuleSettings cannot be null when creating a module!");
        }

        // update the virtual file system to the changes made by zig
        final VirtualFile[] contentRoots = ModuleRootManager.getInstance(module).getContentRoots();
        if (contentRoots.length <= 0) {
            throw new IllegalStateException("Module has no content roots after it has been created!");
        }
        final VirtualFile rootFile = contentRoots[0];

        // ensure that the virtual file system is up to date with changes made during module creation
        rootFile.refresh(false, true);

        completeModuleInitialization(module, moduleSettings, rootFile);
    }

    static void completeModuleInitialization(final @NotNull Module module,
                                             final @NotNull ZigModuleSettings moduleSettings,
                                             final @NotNull VirtualFile rootFile) {
        // initialize the project's settings to those chosen in the user interface
        final ProjectSettingsStateComponent stateComponent =
                ProjectSettingsStateComponent.getInstance(module.getProject());
        stateComponent.projectSettingsState.zigExecutableLocation = moduleSettings.zigExecutableLocation;
        stateComponent.projectSettingsState.zlsExecutableLocation = moduleSettings.zlsExecutableLocation;

        final Project project = module.getProject();

        // create and register the default run configuration
        final RunManager runManager = RunManager.getInstance(project);
        final RunnerAndConfigurationSettings defaultRunConfiguration =
                runManager.createConfiguration(project.getName(), ZigRunConfigurationType.class);
        runManager.addConfiguration(defaultRunConfiguration);
        runManager.setSelectedConfiguration(defaultRunConfiguration);

        // open the autogenerated project files in the editor
        final VirtualFile projectBuildFile = rootFile.findChild("build.zig");
        if (projectBuildFile != null) {
            ApplicationManager.getApplication().invokeLater(() -> FileEditorManager.getInstance(project).openFile(projectBuildFile, false));
        }

        final VirtualFile projectSourceDirectory = rootFile.findChild("src");
        if (projectSourceDirectory != null) {
            final VirtualFile projectMainFile = projectSourceDirectory.findChild("main.zig");
            if (projectMainFile != null) {
                ApplicationManager.getApplication().invokeLater(() -> FileEditorManager.getInstance(project).openFile(projectMainFile, true));
            }
        }
    }

}
