package editor.project;

import com.intellij.ide.util.projectWizard.importSources.DetectedProjectRoot;
import java.io.File;
import org.jetbrains.annotations.*;

class ZigDetectedProjectRoot extends DetectedProjectRoot {

    public static final String ROOT_TYPE_NAME = "Zig project";

    protected ZigDetectedProjectRoot(@NotNull final File directory) {
        super(directory);
    }

    @Override
    public @NotNull @Nls(capitalization = Nls.Capitalization.Sentence) String getRootTypeName() {
        return ROOT_TYPE_NAME;
    }

}
