package editor.project;

import com.intellij.ide.util.importProject.*;
import com.intellij.ide.util.projectWizard.*;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.roots.ModifiableRootModel;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.*;

/**
 * A module wizard step implementation that is used when creating a project or module from sources. The result of
 * importing a new project and creating a new project through {@code ZigDetectedModuleWizardStep} and {@link
 * ZigModuleBuilder} respectively are the same, given that the source files are the same.
 */
class ZigDetectedModuleWizardStep extends AbstractZigModuleWizardStep {

    @Nullable
    private final ProjectBuilder projectBuilder;

    @NotNull
    private final ProjectDescriptor projectDescriptor;

    public ZigDetectedModuleWizardStep(@Nullable final ProjectBuilder projectBuilder,
                                       @NotNull final ProjectDescriptor projectDescriptor) {
        this.projectBuilder = projectBuilder;
        this.projectDescriptor = projectDescriptor;
    }

    @Override
    public void updateDataModel() {
        final ZigModuleSettings moduleSettings = new ZigModuleSettings(zigExecutableLocationTextField.getText().trim(),
                zlsExecutableLocationTextField.getText().trim(), isExecutableButton.isSelected());

        // Note: update method of a module configuration updater will be called after its creation is finished

        if (projectBuilder instanceof ZigModuleBuilder) {
            // Note: this branch was not triggered in testing, but it is what the intellij-rust plugin does
            ((ZigModuleBuilder) projectBuilder).setModuleSettings(moduleSettings);
            // since the builder will complete module initialization in its moduleCreated() implementation,
            // ZigSetupModuleConfigurationUpdater should be used so that it is not done twice
            ((ZigModuleBuilder) projectBuilder).addModuleConfigurationUpdater(new ZigSetupModuleConfigurationUpdater());
        } else {
            final ModuleDescriptor moduleDescriptor = projectDescriptor.getModules().get(0);
            if (moduleDescriptor == null) {
                return;
            }

            moduleDescriptor.addConfigurationUpdater(new ZigModuleConfigurationUpdater(moduleSettings));
        }
    }

    private static class ZigSetupModuleConfigurationUpdater extends ModuleBuilder.ModuleConfigurationUpdater {

        @Override
        public void update(@NotNull final Module module, @NotNull final ModifiableRootModel rootModel) {
            final String errorMessage = ZigModuleBuilder.completeRootModelSetup(rootModel);
            if (errorMessage != null) {
                throw new IllegalStateException(errorMessage);
            }
        }

    }

    /**
     * An implementation of a module configuration updater that is similar to that of {@link ZigModuleBuilder} where
     * the {@link ZigModuleConfigurationUpdater#update(Module, ModifiableRootModel)} method does the work that both the
     * {@link ZigModuleBuilder#setupRootModel(ModifiableRootModel)} and {@link ZigModuleBuilder#moduleCreated(Module)}
     * methods do.
     */
    private static class ZigModuleConfigurationUpdater extends ZigSetupModuleConfigurationUpdater {

        @NotNull
        private final ZigModuleSettings moduleSettings;

        private ZigModuleConfigurationUpdater(@NotNull final ZigModuleSettings moduleSettings) {
            this.moduleSettings = moduleSettings;
        }

        @Override
        public void update(@NotNull final Module module, @NotNull final ModifiableRootModel rootModel) {
            super.update(module, rootModel);

            final VirtualFile[] contentRoots = rootModel.getContentRoots();
            if (contentRoots.length <= 0) {
                throw new IllegalStateException("Module has no content roots after it has been created!");
            }
            final VirtualFile rootFile = contentRoots[0];

            ZigModuleBuilder.completeModuleInitialization(module, moduleSettings, rootFile);
        }

    }

}
