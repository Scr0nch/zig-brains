package editor.project;

import com.intellij.openapi.module.*;
import editor.ZigIcons;
import javax.swing.Icon;
import org.jetbrains.annotations.*;

public class ZigModuleType extends ModuleType<ZigModuleBuilder> {

    private static final String ZIG_MODULE_TYPE_ID = "ZIG_MODULE_TYPE_ID";

    public static ZigModuleType getInstance() {
        return (ZigModuleType) ModuleTypeManager.getInstance().findByID(ZIG_MODULE_TYPE_ID);
    }

    protected ZigModuleType() {
        super(ZIG_MODULE_TYPE_ID);
    }

    @Override
    public @NotNull ZigModuleBuilder createModuleBuilder() {
        return new ZigModuleBuilder();
    }

    @Override
    public @NotNull @Nls(capitalization = Nls.Capitalization.Title) String getName() {
        return "Zig";
    }

    @Override
    public @NotNull @Nls(capitalization = Nls.Capitalization.Sentence) String getDescription() {
        return "Zig language";
    }

    @Override
    public @NotNull Icon getNodeIcon(final boolean isOpened) {
        return ZigIcons.LANGUAGE_ICON;
    }

}
