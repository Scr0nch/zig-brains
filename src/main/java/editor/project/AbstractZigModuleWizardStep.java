package editor.project;

import com.intellij.ide.util.projectWizard.ModuleWizardStep;
import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.ui.TextFieldWithBrowseButton;
import com.intellij.ui.components.*;
import com.intellij.util.ui.FormBuilder;
import editor.settings.ProjectSettingsStateComponent;
import editor.settings.ProjectSettingsStateComponent.ProjectSettingsState;
import java.awt.FlowLayout;
import javax.swing.*;
import util.*;

/**
 * An abstract implementation of ModuleWizardStep that can create the user interface and validate user input. Regardless
 * of whether this project is being created directly or from sources, the same UI is used.
 */
abstract class AbstractZigModuleWizardStep extends ModuleWizardStep {

    private JComponent rootComponent;
    protected TextFieldWithBrowseButton zigExecutableLocationTextField;
    protected TextFieldWithBrowseButton zlsExecutableLocationTextField;
    protected JBRadioButton isExecutableButton;

    @Override
    public JComponent getComponent() {
        if (rootComponent != null) {
            return rootComponent;
        }

        final ProjectSettingsState projectSettingsState =
                ProjectSettingsStateComponent.getInstance(ProjectManager.getInstance().getDefaultProject()).projectSettingsState;
        zigExecutableLocationTextField = UIUtils.createNonEmptyTextFieldWithBrowseButton(null, "Zig executable location");
        zlsExecutableLocationTextField = UIUtils.createTextFieldWithBrowseButton(null);
        zigExecutableLocationTextField.setText(projectSettingsState.zigExecutableLocation);
        zlsExecutableLocationTextField.setText(projectSettingsState.zlsExecutableLocation);

        isExecutableButton = new JBRadioButton("Executable", true);
        final JBRadioButton isLibraryButton = new JBRadioButton("Library");
        final JPanel buttonGroupPanel = new JPanel();
        buttonGroupPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
        buttonGroupPanel.add(isExecutableButton);
        buttonGroupPanel.add(isLibraryButton);
        final ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(isExecutableButton);
        buttonGroup.add(isLibraryButton);

        return rootComponent = FormBuilder.createFormBuilder()
                .addLabeledComponent(new JBLabel("Zig executable location:"), zigExecutableLocationTextField)
                .addTooltip("Location of Zig executable file. Required. May be changed later in project settings.")
                .addLabeledComponent(new JBLabel("ZLS executable location:"), zlsExecutableLocationTextField)
                .addTooltip("Location of Zig Language Server executable file. Optional. Some plugin features will not" +
                        " be available if not provided. May be changed later in project settings.")
                .addLabeledComponent(new JBLabel("Project type:"), buttonGroupPanel)
                .addComponentFillVertically(new JPanel(), 0)
                .getPanel();
    }

    @Override
    public boolean validate() throws ConfigurationException {
        // Note: the trim call is necessary because otherwise Files#exists() will fail on an otherwise-valid path
        final String errorMessage =
                Command.getErrorMessageIfNotExecutableFile(zigExecutableLocationTextField.getText().trim(), false,
                        "Zig executable");
        if (errorMessage != null) {
            throw new ConfigurationException(errorMessage);
        }

        return true;
    }

    @Override
    public void disposeUIResources() {
        rootComponent = null;
    }

}
