package editor;

import com.intellij.formatting.*;
import com.intellij.lang.ASTNode;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiFile;
import com.intellij.psi.codeStyle.CodeStyleSettings;
import language.ZigLanguage;
import language.psi.ZigTypes;
import org.jetbrains.annotations.*;

class ZigFormattingModelBuilder implements FormattingModelBuilder {

    static SpacingBuilder createSpaceBuilder(final CodeStyleSettings codeStyleSettings) {
        return new SpacingBuilder(codeStyleSettings, ZigLanguage.INSTANCE)
                .between(ZigTypes.STATEMENT, ZigTypes.STATEMENT).lineBreakInCode()
                .between(ZigTypes.BLOCK_EXPRESSION, ZigTypes.ELSE_KEYWORD).spaces(1)
                .between(ZigTypes.LABELED_BLOCK_EXPRESSION, ZigTypes.ELSE_KEYWORD).spaces(1);
    }

    @Override
    public @NotNull FormattingModel createModel(@NotNull final FormattingContext formattingContext) {
        final CodeStyleSettings codeStyleSettings = formattingContext.getCodeStyleSettings();
        return FormattingModelProvider.createFormattingModelForPsiFile(
                formattingContext.getContainingFile(),
                new ZigFormattingBlock(formattingContext.getNode(), Wrap.createWrap(WrapType.NONE, false),
                        Alignment.createAlignment(), createSpaceBuilder(codeStyleSettings), Indent.getNoneIndent()),
                codeStyleSettings);
    }

    @Override
    public @Nullable TextRange getRangeAffectingIndent(final PsiFile file, final int offset,
                                                       final ASTNode elementAtOffset) {
        return null;
    }

}
