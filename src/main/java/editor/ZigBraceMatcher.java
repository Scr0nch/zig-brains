package editor;

import com.intellij.lang.*;
import com.intellij.psi.*;
import com.intellij.psi.tree.IElementType;
import language.psi.*;
import org.jetbrains.annotations.*;

public class ZigBraceMatcher implements PairedBraceMatcher {

    private static final BracePair[] BRACE_PAIRS = new BracePair[] {
            new BracePair(ZigTypes.LEFT_BRACE, ZigTypes.RIGHT_BRACE, false),
            new BracePair(ZigTypes.LEFT_BRACKET, ZigTypes.RIGHT_BRACKET, false),
            new BracePair(ZigTypes.LEFT_PARENTHESIS, ZigTypes.RIGHT_PARENTHESIS, false)
    };

    @Override
    public @NotNull BracePair @NotNull [] getPairs() {
        return BRACE_PAIRS;
    }

    @Override
    public boolean isPairedBracesAllowedBeforeType(@NotNull final IElementType leftBraceType,
                                                   @Nullable final IElementType contextType) {
        return true;
    }

    @Override
    public int getCodeConstructStart(final PsiFile file, final int openingBraceOffset) {
        final PsiElement element = file.findElementAt(openingBraceOffset);
        if (element == null || element instanceof PsiFile) {
            return openingBraceOffset;
        }

        // instead of determining the correct PsiElement parent for each possible brace type and parent class
        // explicitly, traverse up the psi tree until a top level declaration, statement, or expression parent is found.
        // That parent should be the correct start of the code construct for most cases
        PsiElement parent = element.getParent();
        while (!(parent instanceof ZigFile || parent instanceof ZigStatement || parent instanceof ZigExpression)) {
            final PsiElement nextParent = parent.getParent();
            if (nextParent == null) {
                break;
            }

            parent = parent.getParent();
        }

        return parent.getTextOffset();
    }

}
