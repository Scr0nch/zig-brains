package editor;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.*;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import java.util.*;
import language.ZigLexerAdapter;
import language.psi.*;
import org.jetbrains.annotations.NotNull;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

/**
 * A syntax highlighter implementation that maps all of the tokens output by the lexer to colors and defines constants
 * for the colors highlighted by a {@link ZigAnnotator}.
 */
public class ZigSyntaxHighlighter extends SyntaxHighlighterBase {

    // a list of constants representing the categories of highlighted tokens in this highlighter
    // this list matches that in {@link DefaultLanguageHighlighterColors}
    static final TextAttributesKey IDENTIFIER = createTextAttributesKey("ZIG_IDENTIFIER",
            DefaultLanguageHighlighterColors.IDENTIFIER);
    static final TextAttributesKey BUILTIN_IDENTIFIER = createTextAttributesKey("ZIG_BUILTIN_IDENTIFIER",
            DefaultLanguageHighlighterColors.PREDEFINED_SYMBOL);
    static final TextAttributesKey NUMBER = createTextAttributesKey("ZIG_NUMBER",
            DefaultLanguageHighlighterColors.NUMBER);
    static final TextAttributesKey KEYWORD = createTextAttributesKey("ZIG_KEYWORD",
            DefaultLanguageHighlighterColors.KEYWORD);
    static final TextAttributesKey STRING = createTextAttributesKey("ZIG_STRING",
            DefaultLanguageHighlighterColors.STRING);
    static final TextAttributesKey DOCUMENTATION_COMMENT = createTextAttributesKey("ZIG_DOCUMENTATION_COMMENT",
            DefaultLanguageHighlighterColors.DOC_COMMENT);
    static final TextAttributesKey TOP_LEVEL_DOCUMENTATION_COMMENT = createTextAttributesKey(
            "ZIG_TOP_LEVEL_DOCUMENTATION_COMMENT", ZigSyntaxHighlighter.DOCUMENTATION_COMMENT);
    static final TextAttributesKey LINE_COMMENT = createTextAttributesKey("ZIG_LINE_COMMENT",
            DefaultLanguageHighlighterColors.LINE_COMMENT);
    static final TextAttributesKey OPERATOR_SIGN = createTextAttributesKey("ZIG_OPERATOR_SIGN",
            DefaultLanguageHighlighterColors.OPERATION_SIGN);
    static final TextAttributesKey BRACES = createTextAttributesKey("ZIG_BRACES",
            DefaultLanguageHighlighterColors.BRACES);
    static final TextAttributesKey DOT = createTextAttributesKey("ZIG_DOT", DefaultLanguageHighlighterColors.DOT);
    static final TextAttributesKey SEMICOLON = createTextAttributesKey("ZIG_SEMICOLON",
            DefaultLanguageHighlighterColors.SEMICOLON);
    static final TextAttributesKey COMMA = createTextAttributesKey("ZIG_COMMA", DefaultLanguageHighlighterColors.COMMA);
    static final TextAttributesKey PARENTHESIS = createTextAttributesKey("ZIG_PARENTHESIS",
            DefaultLanguageHighlighterColors.PARENTHESES);
    static final TextAttributesKey BRACKETS = createTextAttributesKey("ZIG_BRACKETS",
            DefaultLanguageHighlighterColors.BRACKETS);
    static final TextAttributesKey BAD_CHARACTER = createTextAttributesKey("ZIG_BAD_CHARACTER",
            HighlighterColors.BAD_CHARACTER);

    // the below text attributes are added through a {@link ZigAnnotator} instance because they require extra processing
    // in addition to token information
    static final TextAttributesKey FUNCTION_DECLARATION = createTextAttributesKey("ZIG_FUNCTION_DECLARATION",
            DefaultLanguageHighlighterColors.FUNCTION_DECLARATION);
    static final TextAttributesKey FUNCTION_CALL = createTextAttributesKey("ZIG_FUNCTION_CALL",
            DefaultLanguageHighlighterColors.FUNCTION_CALL);
    static final TextAttributesKey LOCAL_VARIABLE = createTextAttributesKey("ZIG_LOCAL_VARIABLE",
            DefaultLanguageHighlighterColors.REASSIGNED_LOCAL_VARIABLE);
    static final TextAttributesKey LOCAL_CONSTANT = createTextAttributesKey("ZIG_LOCAL_CONSTANT",
            DefaultLanguageHighlighterColors.LOCAL_VARIABLE);
    static final TextAttributesKey GLOBAL_VARIABLE = createTextAttributesKey("ZIG_GLOBAL_VARIABLE",
            DefaultLanguageHighlighterColors.GLOBAL_VARIABLE);
    static final TextAttributesKey STRUCTURE = createTextAttributesKey("ZIG_STRUCTURE",
            DefaultLanguageHighlighterColors.CLASS_NAME);
    static final TextAttributesKey STRUCTURE_FIELD = createTextAttributesKey("ZIG_STRUCTURE_FIELD",
            DefaultLanguageHighlighterColors.INSTANCE_FIELD);
    static final TextAttributesKey STRUCTURE_CONSTANT = createTextAttributesKey("ZIG_STRUCTURE_CONSTANT",
            DefaultLanguageHighlighterColors.STATIC_FIELD);
    static final TextAttributesKey PARAMETER = createTextAttributesKey("ZIG_PARAMETER",
            DefaultLanguageHighlighterColors.PARAMETER);

    private static Map<IElementType, TextAttributesKey[]> getTokenHighlights() {
        // Regex used to extract the keyword names and fields from ZigTypes.class
        // Find: ([12][0-9][0-9] )(IElementType )((\w+)_KEYWORD)(.*)
        // Replace: tokenHighlights.put(ZigTypes.$3, _);
        final Map<IElementType, TextAttributesKey[]> tokenHighlights = new HashMap<>();

        tokenHighlights.put(ZigTypes.IDENTIFIER, new TextAttributesKey[] { IDENTIFIER });
        tokenHighlights.put(ZigTypes.BUILTIN_IDENTIFIER, new TextAttributesKey[] { BUILTIN_IDENTIFIER });

        final TextAttributesKey[] NUMBER_ARRAY = new TextAttributesKey[] { NUMBER };
        tokenHighlights.put(ZigTypes.INTEGER_LITERAL, NUMBER_ARRAY);
        tokenHighlights.put(ZigTypes.FLOAT_LITERAL, NUMBER_ARRAY);

        final TextAttributesKey[] KEYWORD_ARRAY = new TextAttributesKey[] { KEYWORD };
        tokenHighlights.put(ZigTypes.ALIGN_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.ALLOWZERO_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.AND_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.ANYFRAME_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.ASSEMBLY_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.ASYNC_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.AWAIT_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.BREAK_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.CATCH_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.COMPTIME_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.CONST_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.CONTINUE_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.DEFER_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.ELSE_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.ENUM_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.ERRDEFER_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.ERROR_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.EXPORT_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.EXTERN_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.FALSE_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.FN_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.FOR_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.IF_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.INLINE_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.LINKSECTION_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.NOALIAS_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.NOINLINE_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.NOSUSPEND_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.NULL_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.ORELSE_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.OR_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.PACKED_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.PUB_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.RESUME_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.RETURN_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.STRUCT_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.SUSPEND_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.SWITCH_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.TEST_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.THREADLOCAL_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.TRUE_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.TRY_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.UNDEFINED_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.UNION_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.UNREACHABLE_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.USINGNAMESPACE_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.VAR_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.VOLATILE_KEYWORD, KEYWORD_ARRAY);
        tokenHighlights.put(ZigTypes.WHILE_KEYWORD, KEYWORD_ARRAY);

        final TextAttributesKey[] STRING_ARRAY = new TextAttributesKey[] { STRING };
        tokenHighlights.put(ZigTypes.CHARACTER_LITERAL, STRING_ARRAY);
        tokenHighlights.put(ZigTypes.SINGLE_STRING_LITERAL, STRING_ARRAY);
        tokenHighlights.put(ZigTypes.LINE_STRING_LITERAL, STRING_ARRAY);

        tokenHighlights.put(ZigTypes.TOP_LEVEL_DOCUMENTATION_COMMENT,
                new TextAttributesKey[] { TOP_LEVEL_DOCUMENTATION_COMMENT });
        tokenHighlights.put(ZigTypes.DOCUMENTATION_COMMENT, new TextAttributesKey[] { DOCUMENTATION_COMMENT });
        tokenHighlights.put(ZigTokenType.LINE_COMMENT, new TextAttributesKey[] { LINE_COMMENT });

        final TextAttributesKey[] OPERATION_SIGN_ARRAY = new TextAttributesKey[] { OPERATOR_SIGN };
        tokenHighlights.put(ZigTypes.AMPERSAND_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.BITWISE_AND_ASSIGN_SYMBOL, OPERATION_SIGN_ARRAY);
        // Note: the asterisk and double asterisk symbols can be a part of types but also an operator and would need to
        // be highlighted differently in each case so do not include them
        tokenHighlights.put(ZigTypes.MULTIPLY_ASSIGN_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.MULTIPLY_OVERFLOW_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.MULTIPLY_OVERFLOW_ASSIGN_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.CARET_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.EXPONENTIATE_ASSIGN_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.COLON_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.COMMA_SYMBOL, new TextAttributesKey[] { COMMA });
        tokenHighlights.put(ZigTypes.DOT_SYMBOL, new TextAttributesKey[] { DOT });
        tokenHighlights.put(ZigTypes.SLICE_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.RANGE_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.DEREFERENCE_POINTER_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.UNWRAP_OPTIONAL_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.ASSIGN_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.EQUAL_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.SWITCH_PRONG_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.EXCLAMATIONMARK_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.NOT_EQUAL_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.LESS_THAN_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.LEFT_BIT_SHIFT_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.LEFT_BIT_SHIFT_ASSIGN_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.LESS_THAN_OR_EQUAL_SYMBOL, OPERATION_SIGN_ARRAY);

        final TextAttributesKey[] BRACES_ARRAY = new TextAttributesKey[] { BRACES };
        final TextAttributesKey[] BRACKETS_ARRAY = new TextAttributesKey[] { BRACKETS };
        final TextAttributesKey[] PARENTHESIS_ARRAY = new TextAttributesKey[] { PARENTHESIS };
        tokenHighlights.put(ZigTypes.LEFT_BRACE, BRACES_ARRAY);
        tokenHighlights.put(ZigTypes.LEFT_BRACKET, BRACKETS_ARRAY);
        tokenHighlights.put(ZigTypes.LEFT_PARENTHESIS, PARENTHESIS_ARRAY);

        tokenHighlights.put(ZigTypes.MINUS_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.SUBTRACT_ASSIGN_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.MINUS_PERCENT_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.SUBTRACT_OVERFLOW_ASSIGN_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.MINUS_RIGHT_ARROW, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.MODULUS_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.MODULUS_ASSIGN_SYMBOL, OPERATION_SIGN_ARRAY);
        //tokenHighlights.put(ZigTypes.PIPE_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.MERGE_ERROR_SET_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.BITWISE_OR_ASSIGN_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.ADD_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.CONCATENATE_ARRAY_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.ADD_ASSIGN_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.ADD_OVERFLOW_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.ADD_OVERFLOW_ASSIGN_SYMBOL, OPERATION_SIGN_ARRAY);
        //tokenHighlights.put(ZigTypes.C_POINTER_SYMBOL;
        //tokenHighlights.put(ZigTypes.OPTIONAL_SYMBOL;
        tokenHighlights.put(ZigTypes.GREATER_THAN_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.RIGHT_BIT_SHIFT_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.RIGHT_BIT_SHIFT_ASSIGN_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.GREATER_THAN_OR_EQUAL_SYMBOL, OPERATION_SIGN_ARRAY);

        tokenHighlights.put(ZigTypes.RIGHT_BRACE, BRACES_ARRAY);
        tokenHighlights.put(ZigTypes.RIGHT_BRACKET, BRACKETS_ARRAY);
        tokenHighlights.put(ZigTypes.RIGHT_PARENTHESIS, PARENTHESIS_ARRAY);

        tokenHighlights.put(ZigTypes.SEMICOLON_SYMBOL, new TextAttributesKey[] { SEMICOLON });

        tokenHighlights.put(ZigTypes.DIVIDE_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.DIVIDE_ASSIGN_SYMBOL, OPERATION_SIGN_ARRAY);
        tokenHighlights.put(ZigTypes.BITWISE_NOT_SYMBOL, OPERATION_SIGN_ARRAY);

        tokenHighlights.put(TokenType.BAD_CHARACTER, new TextAttributesKey[] { BAD_CHARACTER });

        return tokenHighlights;
    }

    private static final Map<IElementType, TextAttributesKey[]> TOKEN_HIGHLIGHTS = getTokenHighlights();

    @NotNull
    @Override
    public Lexer getHighlightingLexer() {
        return new ZigLexerAdapter();
    }

    @NotNull
    @Override
    public TextAttributesKey @NotNull [] getTokenHighlights(final IElementType tokenType) {
        return TOKEN_HIGHLIGHTS.getOrDefault(tokenType, TextAttributesKey.EMPTY_ARRAY);
    }

}
