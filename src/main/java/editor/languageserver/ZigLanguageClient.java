package editor.languageserver;

import com.intellij.openapi.project.Project;
import editor.Notifier;
import editor.settings.ProjectSettingsStateComponent;
import org.eclipse.lsp4j.*;
import org.wso2.lsp4intellij.client.*;

/**
 * A language client that displays notifications instead of dialogs when the language server sends a message so that
 * the user is not interrupted with unnecessary dialogs that must be acknowledged. Additionally, log and info level
 * messages may be discarded depending on the user's saved preference.
 */
public class ZigLanguageClient extends DefaultLanguageClient {

    public ZigLanguageClient(final ClientContext context) {
        super(context);
    }

    @Override
    public void showMessage(final MessageParams messageParams) {
        final MessageType messageType = messageParams.getType();
        final String message = messageParams.getMessage();
        final Project project = getContext().getProject();

        switch (messageType) {
            case Log:
            case Info:
                if (ProjectSettingsStateComponent.getInstance(project).projectSettingsState.isInfoMessagesSuppressed) {
                    break;
                }
                Notifier.displayZLSInfoMessage(project, message);
                break;
            case Warning:
                Notifier.displayWarning(project, message);
                break;
            case Error:
                Notifier.displayError(project, message);
        }
    }

}
