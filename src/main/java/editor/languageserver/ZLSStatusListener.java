package editor.languageserver;

/**
 * A functional interface representing a listener that receives status updates whenever the Zig Language Server is set
 * up or torn down.
 */
@FunctionalInterface
public interface ZLSStatusListener {

    /**
     * Handles a Zig Language Server status changed event.
     *
     * @param zlsStatus {@code true} if the ZLS has been set up, {@code false} is it has been torn down.
     */
    void zlsStatusChanged(boolean zlsStatus);

}
