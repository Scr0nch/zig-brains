package editor.languageserver;

import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.event.DocumentListener;
import org.eclipse.lsp4j.ServerCapabilities;
import org.eclipse.lsp4j.services.*;
import org.wso2.lsp4intellij.client.ClientContext;
import org.wso2.lsp4intellij.client.languageserver.ServerOptions;
import org.wso2.lsp4intellij.client.languageserver.requestmanager.*;
import org.wso2.lsp4intellij.client.languageserver.wrapper.LanguageServerWrapper;
import org.wso2.lsp4intellij.editor.EditorEventManager;
import org.wso2.lsp4intellij.extensions.LSPExtensionManager;
import org.wso2.lsp4intellij.listeners.*;

public class ZigLSPExtensionManager implements LSPExtensionManager {

    @Override
    public <T extends DefaultRequestManager> T getExtendedRequestManagerFor(final LanguageServerWrapper wrapper,
                                                                            final LanguageServer server,
                                                                            final LanguageClient client,
                                                                            final ServerCapabilities serverCapabilities) {
        return null;
    }

    @Override
    public <T extends EditorEventManager> T getExtendedEditorEventManagerFor(final Editor editor,
                                                                             final DocumentListener documentListener,
                                                                             final EditorMouseListenerImpl mouseListener,
                                                                             final EditorMouseMotionListenerImpl mouseMotionListener,
                                                                             final LSPCaretListenerImpl caretListener,
                                                                             final RequestManager requestManager,
                                                                             final ServerOptions serverOptions,
                                                                             final LanguageServerWrapper wrapper) {
        return null;
    }

    @Override
    public Class<? extends LanguageServer> getExtendedServerInterface() {
        // Note: this must be non-null for the extended client to be used. Use the default implementation's value.
        return LanguageServer.class;
    }

    @Override
    public LanguageClient getExtendedClientFor(final ClientContext context) {
        return new ZigLanguageClient(context);
    }

}
