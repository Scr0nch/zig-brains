package editor.languageserver;

import com.intellij.icons.AllIcons;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.fileEditor.*;
import com.intellij.openapi.options.ShowSettingsUtil;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.*;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.ui.*;
import editor.settings.ProjectSettingsConfigurable;
import language.ZigFileType;
import org.jetbrains.annotations.NotNull;

public class ZLSStatusNotifier implements FileEditorManagerListener {

    private static final Key<ZLSStatusListener> ZLS_MANAGER_LISTENER_USER_DATA_KEY = new Key<>("zlsManagerUserDataKey");

    public void fileOpenedSync(@NotNull final FileEditorManager fileEditorManager, @NotNull final VirtualFile file,
                               @NotNull final Pair<FileEditor[], FileEditorProvider[]> editors) {
        if (!ZigFileType.INSTANCE.getDefaultExtension().equals(file.getExtension())) {
            // only display the warning notification panel on zig files
            return;
        }

        final FileEditor fileEditor = fileEditorManager.getSelectedEditor(file);
        if (fileEditor == null) {
            return;
        }

        final EditorNotificationPanel notificationPanel = new EditorNotificationPanel(LightColors.YELLOW);
        notificationPanel.setText("The Zig Language Server has not been set up. Some plugin features will not be " +
                "available.");
        notificationPanel.icon(AllIcons.General.Warning);

        boolean[] isNotificationPanelAdded = new boolean[] { false };
        final ZLSStatusListener listener = zlsStatus -> ApplicationManager.getApplication().invokeLater(() -> {
            if (zlsStatus) {
                if (!isNotificationPanelAdded[0]) {
                    // don't remove a component from the editor that is not added; doing so throws an exception
                    return;
                }
                fileEditorManager.removeTopComponent(fileEditor, notificationPanel);
                isNotificationPanelAdded[0] = false;
            } else {
                fileEditorManager.addTopComponent(fileEditor, notificationPanel);
                isNotificationPanelAdded[0] = true;
            }
        });
        // add or remove the notification panel depending on whether the the ZLS is set up
        ZLSManager.addZLSStatusListener(listener);

        // Note: user data instead of a field is used to store the listener because the listener documentation states
        // that listeners cannot store any state
        file.putUserData(ZLS_MANAGER_LISTENER_USER_DATA_KEY, listener);

        final Project project = fileEditorManager.getProject();
        notificationPanel.createActionLabel("Configure ZLS executable location", () -> {
            // ignore the return value of this method because it is possible for the user to click cancel after
            // applying valid settings
            ShowSettingsUtil.getInstance().editConfigurable(project, "",
                    new ProjectSettingsConfigurable(project), true);

            if (ZLSManager.isZLSSetUp()) {
                ZLSManager.removeZLSStatusListener(listener);
                fileEditorManager.removeTopComponent(fileEditor, notificationPanel);
                isNotificationPanelAdded[0] = false;
            }
        });

        notificationPanel.createActionLabel("Close", () -> {
            fileEditorManager.removeTopComponent(fileEditor, notificationPanel);
            isNotificationPanelAdded[0] = false;
        });
    }

    @Override
    public void fileClosed(@NotNull final FileEditorManager source, @NotNull final VirtualFile file) {
        ZLSManager.removeZLSStatusListener(file.getUserData(ZLS_MANAGER_LISTENER_USER_DATA_KEY));
    }

}
