package editor.languageserver;

import com.intellij.openapi.options.ConfigurationException;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.startup.StartupActivity;
import editor.Notifier;
import editor.settings.ProjectSettingsStateComponent;
import java.util.*;
import org.jetbrains.annotations.*;
import org.wso2.lsp4intellij.IntellijLanguageClient;
import org.wso2.lsp4intellij.client.languageserver.serverdefinition.*;
import org.wso2.lsp4intellij.client.languageserver.wrapper.LanguageServerWrapper;
import org.wso2.lsp4intellij.utils.FileUtils;
import util.Command;

public class ZLSManager implements StartupActivity.DumbAware {

    /**
     * Represents the status of the Zig Language Server, {@code true} if the ZLS has been set up, {@code false
     * otherwise}.
     */
    private static boolean isSetUp;

    /**
     * The list of the registered ZLS status listeners.
     */
    private static final List<ZLSStatusListener> listeners = new ArrayList<>();

    @Override
    public void runActivity(@NotNull final Project project) {
        IntellijLanguageClient.addExtensionManager("zig", new ZigLSPExtensionManager());

        final String zlsExecutableLocation =
                ProjectSettingsStateComponent.getInstance(project).getState().zlsExecutableLocation;

        try {
            setZLSExecutableLocation(project, zlsExecutableLocation);
        } catch (final ConfigurationException ignored) {
            // user saved invalid settings; ignore exceptions but create a notification to alert the user
            Notifier.displayError(project,
                    "Failed to start a Zig Language Server at path \"" + zlsExecutableLocation + "\"!");
        }
    }

    public static void setZLSExecutableLocation(final Project project, final String zlsExecutableLocation) throws ConfigurationException {
        // validate the given zls executable location but don't throw any exceptions until the old server has been
        // removed
        @Nullable ConfigurationException exception = null;
        final String errorMessage = Command.getErrorMessageIfNotExecutableFile(zlsExecutableLocation, true,
                "ZLS executable");
        if (errorMessage != null) {
            exception = new ConfigurationException(errorMessage);
        }

        final LanguageServerDefinition newServerDefinition = new RawCommandServerDefinition("zig",
                new String[] { zlsExecutableLocation });

        // remove the old server wrapper registered with the given project for the Zig language if it is different
        final Set<LanguageServerWrapper> languageServerWrappers = IntellijLanguageClient.getAllServerWrappersFor(
                FileUtils.projectToUri(project));
        final Iterator<LanguageServerWrapper> iterator = languageServerWrappers.iterator();
        boolean foundCorrectServer = false;
        while (iterator.hasNext()) {
            final LanguageServerWrapper languageServerWrapper = iterator.next();
            final LanguageServerDefinition serverDefinition = languageServerWrapper.getServerDefinition();

            if (!"zig".equals(serverDefinition.ext)) {
                continue;
            } else if (!foundCorrectServer && newServerDefinition.equals(serverDefinition) && exception == null) {
                // don't remove a server that is properly-configured unless another one has already been found.
                // the exception should be empty even if the server definitions are equal because it is possible that
                // it has become invalid since the old server was started
                foundCorrectServer = true;
                continue;
            }

            languageServerWrapper.removeServerWrapper();
        }

        if (exception != null) {
            setSetUp(false);
            throw exception;
        }

        if (foundCorrectServer || zlsExecutableLocation.isBlank()) {
            // don't replace the server if a properly-configured one is already registered or if none was provided
            setSetUp(true);
            return;
        }

        IntellijLanguageClient.addServerDefinition(newServerDefinition, project);
        setSetUp(true);
    }

    /**
     * Queries the status of the Zig Language Server.
     *
     * @return {@code true} if the ZLS is set up, {@code false} otherwise
     */
    public static boolean isZLSSetUp() {
        return isSetUp;
    }

    /**
     * Registers the given status listener. It will receive status updates whenever the Zig Language Server is set up
     * or torn down.
     *
     * @param zlsStatusListener the status listener to add
     */
    public static void addZLSStatusListener(final ZLSStatusListener zlsStatusListener) {
        listeners.add(zlsStatusListener);
    }

    /**
     * Removes the given status listener so that it will no longer receive status updates.
     *
     * @param zlsStatusListener the status listener to remove
     */
    public static void removeZLSStatusListener(final ZLSStatusListener zlsStatusListener) {
        listeners.remove(zlsStatusListener);
    }

    /**
     * Sets whether the Zig Language Server has been set up. Afterwards, all of the registered status listeners are
     * notified if the given value is different from the current value.
     *
     * @param isSetUp {@code true} if the ZLS has been set up, {@code false} otherwise
     */
    private static void setSetUp(final boolean isSetUp) {
        final boolean oldIsSetUp = ZLSManager.isSetUp;
        ZLSManager.isSetUp = isSetUp;

        if (oldIsSetUp != isSetUp) {
            listeners.forEach(listener -> listener.zlsStatusChanged(isSetUp));
        }
    }

}
