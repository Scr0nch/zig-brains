package editor;

import com.intellij.formatting.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.*;
import com.intellij.psi.formatter.common.AbstractBlock;
import com.intellij.psi.impl.source.tree.ElementType;
import com.intellij.psi.tree.IElementType;
import java.util.*;
import language.psi.*;
import org.jetbrains.annotations.*;

public class ZigFormattingBlock extends AbstractBlock {

    private final SpacingBuilder spacingBuilder;
    private final Indent indent;

    protected ZigFormattingBlock(@NotNull final ASTNode node, @Nullable final Wrap wrap,
                                 @Nullable final Alignment alignment, final SpacingBuilder spacingBuilder,
                                 final Indent indent) {
        super(node, wrap, alignment);
        this.spacingBuilder = spacingBuilder;
        this.indent = indent;
    }

    @Override
    protected List<Block> buildChildren() {
        final ASTNode[] children = myNode.getChildren(null);
        final int length = children.length;
        final List<Block> blocks = new ArrayList<>(length);

        for (int i = 0; i < length; ++i) {
            final ASTNode child = children[i];

            // from intellij-rust's ASTNode?.isWhiteSpaceOrEmpty() implementation
            if (child == null || child.getTextLength() == 0 || child.getElementType() == TokenType.WHITE_SPACE) {
                continue;
            }

            final WrapType wrapType = calculateWrapType(myNode, child);

            blocks.add(new ZigFormattingBlock(child, Wrap.createWrap(wrapType, true),
                    null, spacingBuilder, calculateIndent(myNode, child)));
        }

        return blocks;
    }

    @Override
    public @NotNull ChildAttributes getChildAttributes(final int newChildIndex) {
        return new ChildAttributes(calculateIndent(myNode, myNode.getChildren(null)[newChildIndex]),
                super.getChildAttributes(newChildIndex).getAlignment());
    }

    @Override
    public @Nullable Spacing getSpacing(@Nullable final Block child1, @NotNull final Block child2) {
        return spacingBuilder.getSpacing(this, child1, child2);
    }

    @Override
    public Indent getIndent() {
        return indent;
    }

    @Override
    public boolean isLeaf() {
        return myNode.getFirstChildNode() == null;
    }

    private static WrapType calculateWrapType(final ASTNode parent, final ASTNode node) {
        return WrapType.NORMAL;
    }

    private static ASTNode getTreeNextNonWhitespace(final ASTNode node) {
        ASTNode next = node.getTreeNext();
        while (next != null && next.getElementType() == ElementType.WHITE_SPACE) {
            next = next.getTreeNext();
        }
        return next;
    }

    private static Indent calculateIndent(final ASTNode parent, final ASTNode child) {
        return isInBlock(parent, child) ? Indent.getNormalIndent() : Indent.getNoneIndent();
    }

    private static boolean isInBlock(final ASTNode parent, final ASTNode child) {
        final IElementType parentElementType = parent.getElementType();
        final IElementType elementType = child.getElementType();

        if (parentElementType == ZigTypes.BLOCK_EXPRESSION
                || parentElementType == ZigTypes.INITIALIZER_LIST
                || parentElementType == ZigTypes.ERROR_SET_DECLARATION
                || parentElementType == ZigTypes.SWITCH_EXPRESSION) {
            return elementType != ZigTypes.LEFT_BRACE && elementType != ZigTypes.RIGHT_BRACE;
        } else if (parentElementType == ZigTypes.CONTAINER_DECLARATION) {
            return elementType != ZigTypes.EXTERN_KEYWORD && elementType != ZigTypes.PACKED_KEYWORD
                    && elementType != ZigTypes.CONTAINER_DECLARATION_TYPE && elementType != ZigTypes.LEFT_BRACE
                    && elementType != ZigTypes.RIGHT_BRACE;
        }

        return false;
    }

}
