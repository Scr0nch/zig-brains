package editor.actions;

import com.intellij.codeInsight.intention.IntentionAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.intellij.util.IncorrectOperationException;
import language.psi.ZigPsiFactory;
import org.jetbrains.annotations.*;

/**
 * A quick fix implementation that converts an invalid builtin function to a valid user-defined function call by
 * removing the '@' symbol at the beginning of the builtin function identifier.
 */
public class ZigConvertBuiltinFunctionIntentionAction implements IntentionAction {

    /**
     * The result of a {@link PsiElement#getText()} call on the
     * {@link ZigConvertBuiltinFunctionIntentionAction#functionIdentifierElement}.
     * Taking this as a parameter saves a {@code getText()} call which the documentation says is expensive.
     */
    private final String functionIdentifierText;

    /**
     * The {@code PsiElement} that represents the function identifier that is to be changed.
     */
    private final PsiElement functionIdentifierElement;

    /**
     * Creates a {@code ZigConvertBuiltinFunctionIntentionAction} that, when executed, will change the given function
     * identifier element with the given function identifier text into a normal function call.
     *
     * @param functionIdentifierText    the result of a {@code PsiElement#getTtext()} call on {@code
     *                                  functionIdentifierElement}
     * @param functionIdentifierElement the {@code PsiElement} that represents the function identifier that is to be
     *                                  changed
     */
    public ZigConvertBuiltinFunctionIntentionAction(final String functionIdentifierText,
                                                    final PsiElement functionIdentifierElement) {
        this.functionIdentifierText = functionIdentifierText;
        this.functionIdentifierElement = functionIdentifierElement;
    }

    @Override
    public @Nls(capitalization = Nls.Capitalization.Sentence) @NotNull String getText() {
        return "Convert to normal function call";
    }

    @Override
    public @NotNull @Nls(capitalization = Nls.Capitalization.Sentence) String getFamilyName() {
        return "Zig";
    }

    @Override
    public boolean isAvailable(@NotNull final Project project, final Editor editor, final PsiFile file) {
        return true;
    }

    @Override
    public void invoke(@NotNull final Project project, final Editor editor, final PsiFile file) throws IncorrectOperationException {
        if (!functionIdentifierElement.isValid()) {
            return;
        }

        // convert the builtin function call to a normal function call by replacing the builtin function call with a
        // new function call
        functionIdentifierElement.getParent().replace(ZigPsiFactory.createFunctionCall(project,
                functionIdentifierText, functionIdentifierElement.getNextSibling().getText()));
    }

    @Override
    public boolean startInWriteAction() {
        return true;
    }

}
