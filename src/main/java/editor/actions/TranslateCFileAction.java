package editor.actions;

import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.fileEditor.OpenFileDescriptor;
import com.intellij.openapi.progress.*;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.*;
import java.io.IOException;
import java.nio.file.Paths;
import org.jetbrains.annotations.NotNull;
import util.Command;

/**
 * An action that, when performed, translates the selected file from C to Zig using {@code zig translate-c}.
 */
public class TranslateCFileAction extends AnAction {

    /**
     * Initializes a {@code TranslateCFileAction} with a text, description, and icon relevant to Zig.
     */
    public TranslateCFileAction() {
        super("Translate C File", "Translate a C source file to Zig", null);
    }

    @Override
    public void actionPerformed(@NotNull final AnActionEvent e) {
        final Project project = e.getProject();
        if (project == null) {
            return;
        }

        final VirtualFile cFile = CommonDataKeys.VIRTUAL_FILE.getData(e.getDataContext());
        if (cFile == null) {
            return;
        }

        final String cFilePath = Paths.get(cFile.getPath()).toAbsolutePath().toString();

        // execute "zig translate-c path/to/c/file"
        final Command.Output commandOutput;
        try {
            // Note: this task cannot be cancelled because it is a different process and will not check for cancellation
            // while it is running
            commandOutput = ProgressManager.getInstance().run(
                    new Task.WithResult<Command.Output, Exception>(project, "Translate C Source File to Zig", false) {

                        @Override
                        protected Command.Output compute(@NotNull ProgressIndicator indicator) throws Exception {
                            return Command.execute(new String[] { "zig", "translate-c", cFilePath }, 10_000);
                        }

                    });
        } catch (final Exception exception) {
            // Note: Exception is the first common superclass of IOException and InterruptedException which
            // Command#execute throws
            Messages.showErrorDialog(project, exception.getMessage(), "Error Running Translate-C");
            return;
        }

        // ensure that no errors were generated before writing command output to the file
        if (commandOutput.standardError != null && !commandOutput.standardError.isBlank()) {
            Messages.showErrorDialog(project, commandOutput.standardError, "Error Translating C File");
            return;
        }

        // ensure that there is output to write to the file
        if (commandOutput.standardOutput.isBlank()) {
            Messages.showErrorDialog(project, "No output generated from command:\n\"zig translate-c " + cFilePath,
                    "Error Translating C File");
            return;
        }

        // write command output to a Zig file with the same name as the C file
        final String[] errorMessage = new String[1];
        final VirtualFile[] zigFile = new VirtualFile[1];
        ApplicationManager.getApplication().runWriteAction(() -> {
            try {
                zigFile[0] = cFile.getParent().findOrCreateChildData(this, cFile.getNameWithoutExtension() + ".zig");
                VfsUtil.saveText(zigFile[0], commandOutput.standardOutput);
            } catch (final IOException ioException) {
                errorMessage[0] = ioException.getMessage();
                // attempt to delete the file if an exception is thrown in VfsUtil#saveText to avoid clutter
                if (zigFile[0] != null) {
                    try {
                        zigFile[0].delete(this);
                    } catch (IOException ignored) {}
                }
            }
        });

        // if an error was generated, show an error message
        if (errorMessage[0] != null) {
            Messages.showErrorDialog(project, errorMessage[0], "Error While Creating Zig Source File");
            return;
        }

        // navigate to the new file
        new OpenFileDescriptor(project, zigFile[0]).navigate(true);
    }

}
