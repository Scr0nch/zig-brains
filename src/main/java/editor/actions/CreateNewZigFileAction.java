package editor.actions;

import com.intellij.ide.actions.CreateFileAction;
import editor.ZigIcons;

/**
 * A simple extension of {@link CreateFileAction} that describes the Zig create file menu item.
 */
public class CreateNewZigFileAction extends CreateFileAction {

    /**
     * Creates a {@link CreateFileAction} with a relevant text, description, and icon to Zig.
     */
    public CreateNewZigFileAction() {
        super("Zig File", "Create new Zig file", ZigIcons.FILE_ICON);
    }

}
