package editor.actions;

import com.intellij.codeInsight.intention.IntentionAction;
import com.intellij.codeInspection.util.*;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.intellij.util.IncorrectOperationException;
import language.psi.*;
import org.jetbrains.annotations.NotNull;
import util.PsiUtils;

/**
 * A quick fix implementation that, when executed, unwraps an if statement or expression depending on its constant
 * condition by removing branches that are impossible to be reached.
 */
public class ZigUnwrapIfIntentionAction implements IntentionAction {

    public static final String UNWRAP_IF_STATEMENT_TEXT = "Unwrap if statement";
    public static final String UNWRAP_IF_EXPRESSION_TEXT = "Unwrap if expression";

    /**
     * The parent {@code PsiElement} of the {@link ZigIfPrefix} of the if statement or expression.
     */
    private final PsiElement parent;

    /**
     * {@code true} if the condition of the if statement or expression is always {@code true}, {@code false} otherwise.
     */
    private final boolean isConditionTrue;

    private final boolean isStatement;

    /**
     * Creates a new {@code ZigUnwrapIfIntentionAction} with the given parent element of the {@link ZigIfPrefix} of the
     * if statement or expression with a constant condition and a flag if the given if statement or expression is either
     * always {@code true} or {@code false}.
     *
     * @param parent          the parent {@code PsiElement} of the {@link ZigIfPrefix} of the if statement or
     *                        expression.
     * @param isConditionTrue {@code true} if the relevant if statement or expression is always {@code true}, {@code
     *                        false} otherwise
     * @param isStatement     {@code true} if the given element is part of an if statement, {@code false} otherwise
     */
    public ZigUnwrapIfIntentionAction(final PsiElement parent, final boolean isConditionTrue,
                                      final boolean isStatement) {
        this.parent = parent;
        this.isConditionTrue = isConditionTrue;
        this.isStatement = isStatement;
    }

    @Override
    public @NotNull @IntentionName String getText() {
        return isStatement ? UNWRAP_IF_STATEMENT_TEXT : UNWRAP_IF_EXPRESSION_TEXT;
    }

    @Override
    public @NotNull @IntentionFamilyName String getFamilyName() {
        return getText();
    }

    @Override
    public boolean isAvailable(@NotNull final Project project, final Editor editor, final PsiFile file) {
        return true;
    }

    @Override
    public void invoke(@NotNull final Project project, final Editor editor, final PsiFile file) throws IncorrectOperationException {
        if (!parent.isValid()) {
            return;
        }

        if (parent instanceof ZigIfStatement) {
            final PsiElement possibleElseKeyword = PsiUtils.getPreviousNonWhitespaceSibling(parent.getParent());
            final PsiElement previousElseKeyword = possibleElseKeyword != null &&
                    possibleElseKeyword.getNode().getElementType() == ZigTypes.ELSE_KEYWORD ? possibleElseKeyword : null;

            if (isConditionTrue) {
                unwrapAlwaysTrueIfStatement(project, editor, parent, previousElseKeyword);
            } else {
                unwrapAlwaysFalseIfStatement(editor, parent, previousElseKeyword);
            }
        } else if (parent instanceof ZigIfExpression) {
            if (isConditionTrue) {
                unwrapAlwaysTrueIfExpression(editor, parent);
            } else {
                final PsiElement possibleElseKeyword = PsiUtils.getPreviousNonWhitespaceSibling(parent);
                final PsiElement elseKeyword = possibleElseKeyword.getNode().getElementType() == ZigTypes.ELSE_KEYWORD ? possibleElseKeyword : null;
                unwrapAlwaysFalseIfExpression(parent, elseKeyword);
            }
        }

        // FIXME whitespace is not always correct after element removal
    }

    private static PsiElement[] getRootParents(final PsiElement element) {
        PsiElement rootParent = element;
        PsiElement lastParent = element;
        while (rootParent instanceof ZigStatement || rootParent instanceof ZigIfStatement) {
            lastParent = rootParent;
            rootParent = rootParent.getParent();
        }

        return new PsiElement[] { rootParent, lastParent };
    }

    private static void unwrapAlwaysTrueIfStatement(final Project project, final Editor editor, final PsiElement parent,
                                                    final PsiElement previousElseKeyword) {
        // delete prefix
        parent.getFirstChild().delete();

        PsiElement labelableBlockOrExpression = parent.getFirstChild();

        // delete any other branches to this if statement because they would never be executed
        final PsiElement elseKeywordWhitespaceOrSemicolon = labelableBlockOrExpression.getNextSibling();
        deleteElseBranches:
        if (elseKeywordWhitespaceOrSemicolon != null) {
            final PsiElement lastChild = parent.getLastChild();
            final PsiElement lastChildToRemove;
            if (lastChild.getNode().getElementType() == ZigTypes.SEMICOLON_SYMBOL) {
                if (PsiManager.getInstance(project).areElementsEquivalent(elseKeywordWhitespaceOrSemicolon,
                        lastChild)) {
                    // there is nothing to remove
                    break deleteElseBranches;
                }
                lastChildToRemove = lastChild.getPrevSibling();
            } else {
                lastChildToRemove = lastChild;
            }
            parent.deleteChildRange(elseKeywordWhitespaceOrSemicolon, lastChildToRemove);
        }

        if (previousElseKeyword != null) {
            previousElseKeyword.delete();
        }

        // find the parent of the if statement to add the unwrapped block or expression to
        final PsiElement[] parents = getRootParents(parent);
        final PsiElement rootParent = parents[0];
        final PsiElement lastParent = parents[1];

        // FIXME labeled block expressions should not be unwrapped
        if (labelableBlockOrExpression instanceof ZigLabeledBlockExpression
                || labelableBlockOrExpression instanceof ZigBlockExpression) {
            // the actual block expression is the last (second) child of a labeled block expression
            final PsiElement blockExpression = labelableBlockOrExpression instanceof ZigLabeledBlockExpression ?
                    labelableBlockOrExpression.getLastChild() : labelableBlockOrExpression;

            // remove left and right braces
            blockExpression.getFirstChild().delete();
            blockExpression.getLastChild().delete();

            // add all children of block to the if statement's parent because they will always be executed
            final PsiElement blockExpressionFirstChild = blockExpression.getFirstChild();
            if (blockExpressionFirstChild != null) {
                rootParent.addRangeAfter(blockExpression.getFirstChild(),
                        blockExpression.getLastChild(), lastParent);
            }
        } else {
            // labelableBlockOrExpression is an expression
            rootParent.addAfter(PsiUtils.createStatement(project, (ZigExpression) labelableBlockOrExpression),
                    lastParent);
        }

        // delete the block and its redundant children
        labelableBlockOrExpression.delete();
        parent.delete();

        // move the caret to the first unwrapped statement, which is the next sibling after the lastParent
        editor.getCaretModel().moveToOffset(PsiUtils.getNextNonWhitespaceSibling(lastParent).getTextOffset());
    }

    private static void unwrapAlwaysFalseIfStatement(final Editor editor, final PsiElement parent,
                                                     final PsiElement previousElseKeyword) {
        // delete the if prefix and labelable block or expression
        parent.getFirstChild().delete();
        parent.getFirstChild().delete();

        {
            final PsiElement possibleSemicolon = parent.getFirstChild();
            if (possibleSemicolon != null && possibleSemicolon.getNode().getElementType() == ZigTypes.SEMICOLON_SYMBOL) {
                // this case is reached when the if prefix is followed by an expression and not a labelable block;
                // the only remaining element is a semicolon. By deleting it, the parent will have no remaining children
                // left and the fast track below will be taken.
                possibleSemicolon.delete();
            }
        }

        // any other children beyond the already-deleted children are chained else-if and else conditions
        // delete the else keyword on the next branch if there is an if keyword after that or if the parent is an
        // expression and not a statement
        final PsiElement elseKeywordOrWhitespace = parent.getFirstChild();
        if (elseKeywordOrWhitespace == null) {
            // this is the last if statement if a chain exists
            if (previousElseKeyword != null) {
                // a chain exists, delete the will-be-hanging else keyword
                previousElseKeyword.delete();
            }
            parent.delete();
            return;
        }

        final PsiElement elseKeyword = elseKeywordOrWhitespace instanceof PsiWhiteSpace ?
                elseKeywordOrWhitespace.getNextSibling() : elseKeywordOrWhitespace;

        PsiElement statement = PsiUtils.getNextNonWhitespaceSibling(elseKeyword);
        if (statement instanceof ZigPayload) {
            statement = PsiUtils.getNextNonWhitespaceSibling(statement);
        }

        final PsiElement possibleIfStatement = statement.getFirstChild();

        final PsiElement parentParent = parent.getParent();
        if (possibleIfStatement instanceof ZigIfStatement) {
            if (previousElseKeyword == null) {
                elseKeyword.delete();
            }

            // move the next if statement up to the root parent
            final PsiElement[] parents = getRootParents(parentParent);
            final PsiElement rootParent = parents[0];
            final PsiElement lastParent = parents[1];
            rootParent.addAfter(possibleIfStatement, lastParent);
            parent.delete();
        } else if (possibleIfStatement instanceof ZigLabeledBlockOrLoopStatement) {
            final PsiElement blockLabelLoopOrExpression = possibleIfStatement.getFirstChild();

            // preserve the block if it has a label
            if (blockLabelLoopOrExpression instanceof ZigBlockLabel
                    || blockLabelLoopOrExpression instanceof ZigLoopStatement) {
                elseKeyword.delete();
                // move the labelable block or loop next to the statement containing the current if statement
                parentParent.addAfter(possibleIfStatement, parent);
                possibleIfStatement.delete();
                parent.delete();
            } else if (previousElseKeyword == null) {
                elseKeyword.delete();
                // unwrap block and move its child statements next to the parent statement
                final PsiElement[] parents = getRootParents(parentParent);
                final PsiElement rootParent = parents[0];
                final PsiElement lastParent = parents[1];

                // delete left and right braces
                blockLabelLoopOrExpression.getFirstChild().delete();
                blockLabelLoopOrExpression.getLastChild().delete();

                PsiElement firstStatement = blockLabelLoopOrExpression.getFirstChild();
                if (firstStatement != null) {
                    firstStatement = rootParent.addRangeAfter(firstStatement,
                            blockLabelLoopOrExpression.getLastChild(), lastParent);
                }
                parent.delete();

                if (firstStatement != null) {
                    // move the caret to the first unwrapped statement
                    // Note: do this after parent is deleted because otherwise firstStatement's offset may change?
                    editor.getCaretModel().moveToOffset(firstStatement.getTextOffset());
                }
            } else {
                // move the else keyword and the following statement up one level of if nesting
                // if statement -> statement -> if statement (parent)
                final PsiElement parentParentParent = parentParent.getParent();
                parentParentParent.addRangeAfter(elseKeyword, statement, parentParent);
                parentParent.delete();
            }

            // delete the else keyword that comes before this if statement if one exists
            if (previousElseKeyword != null) {
                previousElseKeyword.delete();
            }
        }
    }

    private static void unwrapAlwaysTrueIfExpression(final Editor editor, final PsiElement parent) {
        // replace the if expression with the expression itself, which is the second child of the if expression
        final PsiElement expression = PsiUtils.getNextNonWhitespaceSibling(parent.getFirstChild());

        // set the cursor position to be at the beginning of the resulting unwrapped expression; from testing, this is
        // not the always the default result in some rare cases
        final PsiElement expressionCopy = parent.replace(expression);
        editor.getCaretModel().moveToOffset(expressionCopy.getTextOffset());
    }

    private static void unwrapAlwaysFalseIfExpression(final PsiElement parent, final PsiElement previousElseKeyword) {
        // delete the if prefix and expression
        parent.getFirstChild().delete();
        parent.getFirstChild().delete();

        final PsiElement possibleElseKeyword = parent.getFirstChild();

        if (possibleElseKeyword != null) {
            // a trailing else branch exists
            // delete the next branches else keyword, the previous else keyword will replace it if necessary
            possibleElseKeyword.delete();

            // delete the next branches payload if one exists
            final PsiElement payloadOrExpression = parent.getFirstChild();
            if (payloadOrExpression instanceof ZigPayload) {
                // move the next if expression's payload and expression up the chain
                final PsiElement parentParent = parent.getParent();
                final PsiElement addedPayload = parentParent.addAfter(payloadOrExpression, parent);
                parentParent.addAfter(PsiUtils.getNextNonWhitespaceSibling(payloadOrExpression.getNextSibling()), addedPayload);
                parent.delete();
            } else {
                // replace the if expression with the next one in the chain
                parent.replace(payloadOrExpression);
            }
        } else {
            // the parent element has been emptied of children
            if (previousElseKeyword != null) {
                // remove the now-dangling previous else keyword
                previousElseKeyword.delete();
                // delete the empty parent element
                parent.delete();
            } else {
                // valid Zig cannot be produced as there is no expression to replace the if expression with
                throw new IllegalStateException("If expression in unwrap intention action must be part of an if-else chain!");
            }
        }
    }

    @Override
    public boolean startInWriteAction() {
        return true;
    }

}
