package editor.actions;

import com.intellij.compiler.actions.*;
import com.intellij.execution.*;
import com.intellij.execution.executors.DefaultRunExecutor;
import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.project.*;
import editor.runconfiguration.*;
import org.jetbrains.annotations.NotNull;

public class ZigCompileDirtyAction extends CompileActionBase {

    /**
     * The default action triggered by the build button. Will use IntelliJ's complex compiler system to build a project.
     */
    private static final CompileDirtyAction DEFAULT_COMPILE_DIRTY_ACTION = new CompileDirtyAction();

    @Override
    public void actionPerformed(@NotNull final AnActionEvent e) {
        super.actionPerformed(e);

        final Project project = e.getProject();
        if (project == null) {
            // Neither this action nor the default action have anything to do
            return;
        }

        final RunnerAndConfigurationSettings selectedRunnerAndSettings =
                RunManager.getInstance(project).getSelectedConfiguration();
        if (selectedRunnerAndSettings == null) {
            // Allow the default action to attempt to compile this project. This action requires a Zig run configuration
            // to compile a project.
            DEFAULT_COMPILE_DIRTY_ACTION.actionPerformed(e);
            return;
        }

        if (!(selectedRunnerAndSettings.getConfiguration() instanceof ZigRunConfiguration)) {
            // The selected configuration is not one for Zig; defer to the default action
            DEFAULT_COMPILE_DIRTY_ACTION.actionPerformed(e);
            return;
        }
        final ZigRunConfiguration selectedRunConfiguration = (ZigRunConfiguration) selectedRunnerAndSettings.getConfiguration();

        final RunnerAndConfigurationSettings selectedConfigurationBuildOnly = RunManager.getInstance(project)
                .createConfiguration("Build: " + selectedRunnerAndSettings.getName(), ZigRunConfigurationType.INSTANCE.getFactory());
        final ZigRunConfiguration selectedRunConfigurationBuildOnly = (ZigRunConfiguration) selectedConfigurationBuildOnly.getConfiguration();
        selectedRunConfigurationBuildOnly.setBuildFilePath(selectedRunConfiguration.getBuildFilePath());
        selectedRunConfigurationBuildOnly.setBuildOnly(true);

        final Executor executor = DefaultRunExecutor.getRunExecutorInstance();
        ProgramRunnerUtil.executeConfiguration(selectedConfigurationBuildOnly, executor);
    }

    @Override
    protected void doAction(final DataContext dataContext, final Project project) {
        // Do nothing here; it is simpler to keep all of the logic inside of actionPerformed because whether the
        // default compile dirty action should be run has to be handled there. Any communication between this method
        // and actionPerformed would require instance fields.
    }

    @Override
    public void update(@NotNull final AnActionEvent e) {
        DEFAULT_COMPILE_DIRTY_ACTION.update(e);
    }

}
