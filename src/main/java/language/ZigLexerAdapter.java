package language;

import com.intellij.lexer.FlexAdapter;

/**
 * A simple {@code FlexAdapter} extension that adapts a {@link ZigLexer} with flex.
 */
public class ZigLexerAdapter extends FlexAdapter {

    /**
     * Creates a {@code FlexAdapter} with a {@link ZigLexer} instance.
     */
    public ZigLexerAdapter() {
        super(new ZigLexer(null));
    }

}
