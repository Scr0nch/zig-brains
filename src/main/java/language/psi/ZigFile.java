package language.psi;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import language.*;
import org.jetbrains.annotations.NotNull;

/**
 * A simple {@code PsiFileBase} extension that represents a Zig Psi file.
 */
public class ZigFile extends PsiFileBase {

    /**
     * Creates a {@code PsiFileBase} for a Zig language file with the given view provider.
     *
     * @param viewProvider the view provider for the file
     */
    public ZigFile(@NotNull final FileViewProvider viewProvider) {
        super(viewProvider, ZigLanguage.INSTANCE);
    }

    @Override
    public @NotNull FileType getFileType() {
        return ZigFileType.INSTANCE;
    }

}
