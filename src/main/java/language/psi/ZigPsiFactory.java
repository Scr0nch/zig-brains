package language.psi;

import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.util.LocalTimeCounter;
import language.ZigFileType;
import org.jetbrains.annotations.NotNull;

/**
 * A utility class that provides functions to create {@code PsiElement}s dynamically.
 */
public class ZigPsiFactory {

    /**
     * Gets a {@code PsiElement} of the given class that is in the given zig file.
     *
     * @param file  the file to search for elements in
     * @param clazz the class of the element to search for
     * @return the first element of the given class found or {@code null} if none are found
     */
    private static PsiElement getDescendantOfType(final ZigFile file, final Class<? extends PsiElement> clazz) {
        return PsiTreeUtil.findChildOfType(file, clazz, true);
    }

    private static ZigFile createFileImpl(@NotNull final Project project, @NotNull final String code,
                                          final boolean isEventSystemEnabled) {
        return (ZigFile) PsiFileFactory.getInstance(project).createFileFromText("dummy.zig", ZigFileType.INSTANCE,
                code, LocalTimeCounter.currentTime(), isEventSystemEnabled, true);
    }

    /**
     * Create a {@code ZigFile} with parsed psi and code that can be highlighted. Should only be used when the file
     * needs to be highlighted, use {@link ZigPsiFactory#createFile(Project, String)} otherwise.
     *
     * @param project the current project
     * @param code    the code to parse into psi
     * @return a file containing the parsed psi
     */
    public static ZigFile createEventSystemEnabledFile(final Project project, final String code) {
        return createFileImpl(project, code, true);
    }

    /**
     * Creates a {@code ZigFile} with parsed psi from the given code.
     *
     * @param project the current project
     * @param code    the code to parse into psi
     * @return a file containing the parsed psi
     */
    public static ZigFile createFile(final Project project, final String code) {
        return createFileImpl(project, code, false);
    }

    /**
     * Creates a {@code ZigFile} with a function call with the given name as the only code inside a comptime block and
     * extracts the {@code PsiElement} representing the function call from it.
     *
     * @param project           the current project
     * @param functionName      the name of the function call
     * @param functionArguments the arguments used in the function call surrounded by opening and closing parenthesis
     * @return a {@code PsiElement} that represents a Zig function call with the given name.
     */
    public static PsiElement createFunctionCall(final Project project, final String functionName,
                                                final String functionArguments) {
        return getDescendantOfType(createFile(project, "comptime{" + functionName + functionArguments + ";}"),
                ZigSuffixExpression.class);
    }

    public static PsiElement createStatement(final Project project, final String statement) {
        return getDescendantOfType(createFile(project, "comptime{" + statement + "}"), ZigStatement.class);
    }

    private ZigPsiFactory() {}

}
