package language.psi;

import com.intellij.psi.tree.IElementType;
import language.ZigLanguage;
import org.jetbrains.annotations.NotNull;

/**
 * A simple {@code IElementType} extension that represents a Zig token's possible types.
 */
public class ZigTokenType extends IElementType {

    // constants for tokens used outside of the lexer and parsed through regexes
    public static final ZigTokenType LINE_COMMENT = new ZigTokenType("LINE_COMMENT");

    /**
     * Creates a {@code IElementType} for the Zig language with the given debug name.
     *
     * @param debugName a debugger-friendly name for the token type
     */
    public ZigTokenType(@NotNull final String debugName) {
        super(debugName, ZigLanguage.INSTANCE);
    }

}
