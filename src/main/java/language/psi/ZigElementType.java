package language.psi;

import com.intellij.psi.tree.IElementType;
import language.ZigLanguage;
import org.jetbrains.annotations.NotNull;

/**
 * A simple {@code IElementType} extension that represents a Zig element type.
 */
public class ZigElementType extends IElementType {

    /**
     * Creates an {@code IElementType} for the Zig language with the given debug name.
     *
     * @param debugName a debugger-friendly name for the element
     */
    public ZigElementType(@NotNull final String debugName) {
        super(debugName, ZigLanguage.INSTANCE);
    }

}
