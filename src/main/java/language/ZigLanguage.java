package language;

import com.intellij.lang.Language;

/**
 * A simple singleton {@code Language} extension that represents the Zig language.
 */
public class ZigLanguage extends Language {

    public static final ZigLanguage INSTANCE = new ZigLanguage();

    private ZigLanguage() {
        super("Zig");
    }

}
