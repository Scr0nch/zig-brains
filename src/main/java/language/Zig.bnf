{
  // class definitions below fill in the example provided by the IntelliJ Language Plugin Tutorial in their BNF file.
  parserClass="language.parser.ZigParser"

  extends="com.intellij.extapi.psi.ASTWrapperPsiElement"

  psiClassPrefix="Zig"
  psiImplClassSuffix="Impl"
  psiPackage="language.psi"
  psiImplPackage="language.psi.impl"

  // allow left-hand recursion for expressions
  extends(".*Expression")=expression
  consumeTokenMethod(".*Expression")="consumeTokenFast"

  elementTypeHolderClass="language.psi.ZigTypes"
  elementTypeClass="language.psi.ZigElementType"
  tokenTypeClass="language.psi.ZigTokenType"
}

/* This BNF grammar file aims to be an as-direct-as-possible translation of the <a href="https://github.com/ziglang/zig-spec/grammar/grammar.y">official Zig grammar</a>.
 * The official YACC file contains both rules for a parser and lexer. This file only defines the rules for the parser.
 * Known deviations from the latest official YACC zig grammar:
 * Line Number? | Content? | Description
 * Name abbreviations/capitalization changed to be more descriptive.
 */

// top level
zigFile ::= topLevelDeclarations

private topLevelDeclarations ::= TOP_LEVEL_DOCUMENTATION_COMMENT? topLevelDeclaration* (containerField COMMA_SYMBOL)* containerField? topLevelDeclaration*

private topLevelDeclaration ::= testDeclaration
                            | topLevelComptimeBlock
                            | DOCUMENTATION_COMMENT? PUB_KEYWORD? publicizableTopLevelDeclaration

testDeclaration ::= DOCUMENTATION_COMMENT? TEST_KEYWORD SINGLE_STRING_LITERAL? blockExpression

topLevelComptimeBlock ::= DOCUMENTATION_COMMENT? COMPTIME_KEYWORD blockExpression

private publicizableTopLevelDeclaration ::= functionDeclaration
                                        | topLevelVariableDeclaration
                                        | importDeclaration

functionDeclaration ::= (EXPORT_KEYWORD | EXTERN_KEYWORD SINGLE_STRING_LITERAL? | (INLINE_KEYWORD | NOINLINE_KEYWORD))? functionDeclarationPrototype (SEMICOLON_SYMBOL | blockExpression)
functionDeclarationPrototype ::= FN_KEYWORD IDENTIFIER functionPrototypeSuffix
functionPrototype ::= FN_KEYWORD functionPrototypeSuffix
private functionPrototypeSuffix ::= LEFT_PARENTHESIS parameterDeclarationList RIGHT_PARENTHESIS byteAlignment? linkSection? callingConvectionSection? EXCLAMATIONMARK_SYMBOL? typeExpression

topLevelVariableDeclaration ::= (EXPORT_KEYWORD | EXTERN_KEYWORD SINGLE_STRING_LITERAL?)? THREADLOCAL_KEYWORD? variableDeclaration
variableDeclaration ::= (CONST_KEYWORD | VAR_KEYWORD) IDENTIFIER (COLON_SYMBOL typeExpression)? byteAlignment? linkSection? (ASSIGN_SYMBOL expression)? SEMICOLON_SYMBOL

importDeclaration ::= USINGNAMESPACE_KEYWORD? expression SEMICOLON_SYMBOL

containerField ::= DOCUMENTATION_COMMENT? COMPTIME_KEYWORD? IDENTIFIER (COLON_SYMBOL (ANYTYPE_KEYWORD | typeExpression) byteAlignment?)? (ASSIGN_SYMBOL expression)?

// block level
statement ::= COMPTIME_KEYWORD? variableDeclaration
          | (COMPTIME_KEYWORD | NOSUSPEND_KEYWORD | DEFER_KEYWORD) blockExpressionStatement
          | ERRDEFER_KEYWORD payload? blockExpressionStatement
          | SUSPEND_KEYWORD blockExpressionStatement
          | ifStatement
          | labeledBlockOrLoopStatement
          | switchExpression // no trailing semicolon required; the expression is also a statement
          | expression SEMICOLON_SYMBOL

ifStatement ::= ifPrefix labeledBlockExpression (ELSE_KEYWORD payload? statement)?
            | ifPrefix expression (SEMICOLON_SYMBOL | ELSE_KEYWORD payload? statement)

labeledBlockOrLoopStatement ::= blockLabel? (blockExpression | loopStatement)

loopStatement ::= INLINE_KEYWORD? (forStatement | whileStatement)

forStatement ::= forPrefix labeledBlockExpression (ELSE_KEYWORD statement)?
             | forPrefix expression (SEMICOLON_SYMBOL | ELSE_KEYWORD statement)

whileStatement ::= whilePrefix labeledBlockExpression (ELSE_KEYWORD payload? statement)?
               | whilePrefix expression (SEMICOLON_SYMBOL | ELSE_KEYWORD payload? statement)

// accept either a block of statements or an expression terminated with a semicolon (a single statement)
blockExpressionStatement ::= labeledBlockExpression
                         | expression SEMICOLON_SYMBOL

labeledBlockExpression ::= blockLabel? blockExpression

// expression level
expression ::= assemblyExpression
           | OperationExpression
           | KeywordExpression
           | prefixExpression
           | blockExpression
           | primaryExpression

private OperationExpression ::= assignExpression
                              | booleanOrExpression
                              | booleanAndExpression
                              | compareExpression
                              | bitwiseExpression
                              | bitShiftExpression
                              | additionExpression
                              | multiplyExpression

assignExpression ::= expression (assignOperation expression)+

booleanOrExpression ::= expression (OR_KEYWORD expression)+

booleanAndExpression ::= expression (AND_KEYWORD expression)+

compareExpression ::= expression (compareOperation expression)

bitwiseExpression ::= expression (bitwiseOperation expression)+

bitShiftExpression ::= expression (bitShiftOperation expression)+

additionExpression ::= expression (additionOperation expression)+

multiplyExpression ::= expression (multiplyOperation expression)+

private KeywordExpression ::= tryExpression
                          | breakExpression
                          | comptimeExpression
                          | nosuspendExpression
                          | continueExpression
                          | resumeExpression
                          | returnExpression

tryExpression ::= TRY_KEYWORD expression
breakExpression ::= BREAK_KEYWORD breakLabel? expression?
comptimeExpression ::= COMPTIME_KEYWORD expression
nosuspendExpression ::= NOSUSPEND_KEYWORD expression
continueExpression ::= CONTINUE_KEYWORD breakLabel?
resumeExpression ::= RESUME_KEYWORD expression
returnExpression ::= RETURN_KEYWORD expression?

prefixExpression ::= prefixOperation+ expression

// TODO why do these expressions have to be separate from the rest in the root expression rule?
primaryExpression ::= ControlFlowExpression
                  | curlySuffixExpression

private ControlFlowExpression ::= ifExpression
                              | labeledLoopExpression

labeledLoopExpression ::= blockLabel? loopExpression

ifExpression ::= ifPrefix expression (ELSE_KEYWORD payload? expression)?

blockExpression ::= LEFT_BRACE statement* RIGHT_BRACE

loopExpression ::= INLINE_KEYWORD? (forExpression | whileExpression)

forExpression ::= forPrefix expression (ELSE_KEYWORD expression)?

whileExpression ::= whilePrefix expression (ELSE_KEYWORD payload? expression)?

// TODO flatten PSI hierarchy; constants do not need to be children of a curly suffix expression
curlySuffixExpression ::= typeExpression initializerList?

initializerList ::= LEFT_BRACE fieldInitializer (COMMA_SYMBOL fieldInitializer)* COMMA_SYMBOL? RIGHT_BRACE 
                | LEFT_BRACE expression (COMMA_SYMBOL expression)* COMMA_SYMBOL? RIGHT_BRACE
                | LEFT_BRACE RIGHT_BRACE

typeExpression ::= prefixTypeOperation* errorUnionExpression

errorUnionExpression ::= suffixExpression (EXCLAMATIONMARK_SYMBOL typeExpression)?

suffixExpression ::= ASYNC_KEYWORD primaryTypeExpression suffixOperation* functionCallArguments
                 | primaryTypeExpression (suffixOperation | functionCallArguments)*

primaryTypeExpression ::= BUILTIN_IDENTIFIER functionCallArguments
                      | CHARACTER_LITERAL
                      | containerDeclaration
                      | DOT_SYMBOL IDENTIFIER
                      | DOT_SYMBOL initializerList
                      | errorSetDeclaration
                      | FLOAT_LITERAL
                      | functionPrototype
                      | groupedExpression
                      | labeledTypeExpression
                      | IDENTIFIER
                      | ifTypeExpression
                      | INTEGER_LITERAL
                      | COMPTIME_KEYWORD typeExpression
                      | ERROR_KEYWORD DOT_SYMBOL IDENTIFIER
                      | FALSE_KEYWORD
                      | NULL_KEYWORD
                      | ANYFRAME_KEYWORD
                      | TRUE_KEYWORD
                      | UNDEFINED_KEYWORD
                      | UNREACHABLE_KEYWORD
                      | stringLiteral
                      | switchExpression

stringLiteral ::= SINGLE_STRING_LITERAL | LINE_STRING_LITERAL

containerDeclaration ::= (EXTERN_KEYWORD | PACKED_KEYWORD)? containerDeclarationType LEFT_BRACE topLevelDeclarations RIGHT_BRACE
containerDeclarationType ::= STRUCT_KEYWORD
                         | OPAQUE_KEYWORD
                         | ENUM_KEYWORD (LEFT_PARENTHESIS expression RIGHT_PARENTHESIS)?
                         | UNION_KEYWORD (LEFT_PARENTHESIS (ENUM_KEYWORD (LEFT_PARENTHESIS expression RIGHT_PARENTHESIS)? | expression) RIGHT_PARENTHESIS)?

errorSetDeclaration ::= ERROR_KEYWORD LEFT_BRACE identifierList RIGHT_BRACE

groupedExpression ::= LEFT_PARENTHESIS expression RIGHT_PARENTHESIS

ifTypeExpression ::= ifPrefix typeExpression (ELSE_KEYWORD payload? typeExpression)?

labeledTypeExpression ::= blockLabel blockExpression
                      | blockLabel? loopTypeExpression

loopTypeExpression ::= INLINE_KEYWORD? (forTypeExpression | whileTypeExpression)

forTypeExpression ::= forPrefix typeExpression (ELSE_KEYWORD typeExpression)?

whileTypeExpression ::= whilePrefix typeExpression (ELSE_KEYWORD payload? typeExpression)?

switchExpression ::= SWITCH_KEYWORD LEFT_PARENTHESIS expression RIGHT_PARENTHESIS LEFT_BRACE switchProngList RIGHT_BRACE

// assembly
assemblyExpression ::= ASSEMBLY_KEYWORD VOLATILE_KEYWORD? LEFT_PARENTHESIS expression assemblyOutput? RIGHT_PARENTHESIS

assemblyOutput ::= COLON_SYMBOL assemblyOutputList assemblyInput?

assemblyOutputItem ::= LEFT_BRACKET IDENTIFIER RIGHT_BRACKET stringLiteral LEFT_PARENTHESIS (MINUS_RIGHT_ARROW typeExpression | IDENTIFIER) RIGHT_PARENTHESIS

assemblyInput ::= COLON_SYMBOL assemblyInputList assemblyClobbers?

assemblyInputItem ::= LEFT_BRACKET IDENTIFIER RIGHT_BRACKET stringLiteral LEFT_PARENTHESIS expression RIGHT_PARENTHESIS

assemblyClobbers ::= COLON_SYMBOL stringList

// helper grammar
breakLabel ::= COLON_SYMBOL IDENTIFIER

blockLabel ::= IDENTIFIER COLON_SYMBOL

fieldInitializer ::= DOT_SYMBOL IDENTIFIER ASSIGN_SYMBOL expression

whileContinueExpression ::= COLON_SYMBOL LEFT_PARENTHESIS expression RIGHT_PARENTHESIS

linkSection ::= LINKSECTION_KEYWORD LEFT_PARENTHESIS expression RIGHT_PARENTHESIS

// function specific
callingConvectionSection ::= CALLCONV_KEYWORD LEFT_PARENTHESIS expression RIGHT_PARENTHESIS

parameterDeclaration ::= DOCUMENTATION_COMMENT? (NOALIAS_KEYWORD | COMPTIME_KEYWORD)? (IDENTIFIER COLON_SYMBOL)? parameterType
                     | RANGE_SYMBOL

parameterType ::= ANYTYPE_KEYWORD
              | typeExpression

// control flow prefixes
ifPrefix ::= IF_KEYWORD LEFT_PARENTHESIS expression RIGHT_PARENTHESIS pointerPayload?

whilePrefix ::= WHILE_KEYWORD LEFT_PARENTHESIS expression RIGHT_PARENTHESIS pointerPayload? whileContinueExpression?

forPrefix ::= FOR_KEYWORD LEFT_PARENTHESIS expression RIGHT_PARENTHESIS pointerIndexPayload

// payloads
payload ::= PIPE_SYMBOL IDENTIFIER PIPE_SYMBOL

pointerPayload ::= PIPE_SYMBOL ASTERISK_SYMBOL? IDENTIFIER PIPE_SYMBOL

pointerIndexPayload ::= PIPE_SYMBOL ASTERISK_SYMBOL? IDENTIFIER (COMMA_SYMBOL IDENTIFIER)? PIPE_SYMBOL

// switch specific grammar
switchProng ::= switchCase SWITCH_PRONG_SYMBOL pointerPayload? expression

switchCase ::= switchItem (COMMA_SYMBOL switchItem)* COMMA_SYMBOL?
           | ELSE_KEYWORD

switchItem ::= expression (RANGE_SYMBOL expression)?

// operators
private assignOperation ::= MULTIPLY_ASSIGN_SYMBOL
                        | DIVIDE_ASSIGN_SYMBOL
                        | MODULUS_ASSIGN_SYMBOL
                        | ADD_ASSIGN_SYMBOL
                        | SUBTRACT_ASSIGN_SYMBOL
                        | LEFT_BIT_SHIFT_ASSIGN_SYMBOL
                        | RIGHT_BIT_SHIFT_ASSIGN_SYMBOL
                        | BITWISE_AND_ASSIGN_SYMBOL
                        | EXPONENTIATE_ASSIGN_SYMBOL
                        | BITWISE_OR_ASSIGN_SYMBOL
                        | MULTIPLY_OVERFLOW_ASSIGN_SYMBOL
                        | ADD_OVERFLOW_ASSIGN_SYMBOL
                        | SUBTRACT_OVERFLOW_ASSIGN_SYMBOL
                        | ASSIGN_SYMBOL

private compareOperation ::= EQUAL_SYMBOL
                         | NOT_EQUAL_SYMBOL
                         | LESS_THAN_SYMBOL
                         | GREATER_THAN_SYMBOL
                         | LESS_THAN_OR_EQUAL_SYMBOL
                         | GREATER_THAN_OR_EQUAL_SYMBOL

private bitwiseOperation ::= AMPERSAND_SYMBOL // and
                         | CARET_SYMBOL // xor
                         | PIPE_SYMBOL // or
                         | ORELSE_KEYWORD
                         | CATCH_KEYWORD payload?

private bitShiftOperation ::= LEFT_BIT_SHIFT_SYMBOL
                          | RIGHT_BIT_SHIFT_SYMBOL

private additionOperation ::= ADD_SYMBOL
                          | MINUS_SYMBOL // subtraction operator
                          | CONCATENATE_ARRAY_SYMBOL
                          | ADD_OVERFLOW_SYMBOL
                          | MINUS_PERCENT_SYMBOL // subtract overflow operator

private multiplyOperation ::= MERGE_ERROR_SET_SYMBOL
                          | ASTERISK_SYMBOL
                          | DIVIDE_SYMBOL
                          | MODULUS_SYMBOL
                          | DOUBLE_ASTERISK_SYMBOL // multiply array operator
                          | MULTIPLY_OVERFLOW_SYMBOL

private prefixOperation ::= EXCLAMATIONMARK_SYMBOL // logical not operator
                        | MINUS_SYMBOL // negate operator
                        | MINUS_PERCENT_SYMBOL // wrapping negation operator
                        | BITWISE_NOT_SYMBOL
                        | AMPERSAND_SYMBOL // address-of operator
                        | TRY_KEYWORD
                        | AWAIT_KEYWORD

private prefixTypeOperation ::= OPTIONAL_SYMBOL
                            | ANYFRAME_KEYWORD MINUS_RIGHT_ARROW
                            | sliceTypeStart (byteAlignment | CONST_KEYWORD | VOLATILE_KEYWORD | ALLOWZERO_KEYWORD)*
                            | pointerTypeStart (ALIGN_KEYWORD LEFT_PARENTHESIS expression (COLON_SYMBOL INTEGER_LITERAL COLON_SYMBOL INTEGER_LITERAL)? RIGHT_PARENTHESIS | CONST_KEYWORD | VOLATILE_KEYWORD | ALLOWZERO_KEYWORD)*
                            | arrayTypeStart

private suffixOperation ::= LEFT_BRACKET expression (SLICE_SYMBOL expression? sentinelTerminator?)? RIGHT_BRACKET
                        | DOT_SYMBOL IDENTIFIER
                        | DEREFERENCE_POINTER_SYMBOL
                        | UNWRAP_OPTIONAL_SYMBOL

// pointer specific
sliceTypeStart ::= LEFT_BRACKET sentinelTerminator? RIGHT_BRACKET

pointerTypeStart ::= ASTERISK_SYMBOL // pointer type symbol
                 | DOUBLE_ASTERISK_SYMBOL // double pointer type symbol
                 | unknownLengthPointer
                 | C_POINTER_SYMBOL

arrayTypeStart ::= LEFT_BRACKET expression sentinelTerminator? RIGHT_BRACKET

unknownLengthPointer ::= LEFT_BRACKET ASTERISK_SYMBOL sentinelTerminator? RIGHT_BRACKET

sentinelTerminator ::= COLON_SYMBOL expression

// alignment
byteAlignment ::= ALIGN_KEYWORD LEFT_PARENTHESIS expression RIGHT_PARENTHESIS

// lists
identifierList ::= (DOCUMENTATION_COMMENT? IDENTIFIER COMMA_SYMBOL)* (DOCUMENTATION_COMMENT? IDENTIFIER)?

switchProngList ::= (switchProng COMMA_SYMBOL)* switchProng?

assemblyOutputList ::= (assemblyOutputItem COMMA_SYMBOL)* assemblyOutputItem?

assemblyInputList ::= (assemblyInputItem COMMA_SYMBOL)* assemblyInputItem?

stringList ::= (stringLiteral COMMA_SYMBOL)* stringLiteral?

parameterDeclarationList ::= (parameterDeclaration COMMA_SYMBOL)* parameterDeclaration?

expressionList ::= (expression COMMA_SYMBOL)* expression?

functionCallArguments ::= LEFT_PARENTHESIS expressionList RIGHT_PARENTHESIS