package language;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import language.psi.*;
%%

/* This FLEX grammar file aims to be an as-direct-as-possible translation of the <a href="https://github.com/ziglang/zig-spec/grammar/grammar.y">official Zig grammar</a>.
 * The official YACC file contains both rules for a parser and lexer. This file only defines the rules for the lexer.
 * Known deviations from the latest official YACC zig grammar:
 * Whitespace is handled globaly instead of appending {skip} and {end_of_word} with the WHITESPACE macro. This seems to
 *     be the indended way to handle whitespace in BNF (the intellij-zig project does something similar) and it also
 *     reduces ram usage from 12GB to less than 512MB
 * Keywords are not ignored in identifiers because an identifier that is a keyword is better handled by an Annotator
 * Symbols do not exclude characters that must not come after them in their rules; e.g. the rule for "=" does not end
 *     with "![=]". This should not cause any issues with parsing.
 * Invalid characters are defined so that parsing would not fail if there is one in a Zig file.
 */

// class information follows the example set by the IntelliJ Language Plugin Tutorial
%class ZigLexer
%implements FlexLexer
%unicode
%function advance
%type IElementType
%eof{
%eof}

// macro definitions
// utility definitions
DECIMAL_DIGIT=[0-9]
DECIMAL_DIGIT_=(_? {DECIMAL_DIGIT})
DECIMAL_INT={DECIMAL_DIGIT}{DECIMAL_DIGIT_}*
HEXIDECIMAL_DIGIT=[a-fA-F0-9]
HEXIDECIMAL_DIGIT_=(_? {HEXIDECIMAL_DIGIT})
HEXIDECIMAL_INT={HEXIDECIMAL_DIGIT}{HEXIDECIMAL_DIGIT_}*

// this macro does not use the same regex as the source grammar file's mb_utf8_literal macro, but they should still
// function the same
UNICODE_CHARACTER=\P{M}\p{M}*+
ascii_char_not_nl_slash_squote=[\000-\011\013-\046\050-\133\135-\177]

ESCAPE_CHARACTER=(\\x{HEXIDECIMAL_DIGIT}{HEXIDECIMAL_DIGIT})|(\\u\{{HEXIDECIMAL_DIGIT}+\})|(\\[nr\\t'\"])
CHARACTER_CHARACTER={UNICODE_CHARACTER}|{ESCAPE_CHARACTER}|{ascii_char_not_nl_slash_squote}
STRING_CHARACTER={ESCAPE_CHARACTER}|[^\\\n\"]

// grammar definitions
INTEGER_LITERAL=(0b[01](_?[01])*)|(0o[0-7](_?[0-7])*)|(0x{HEXIDECIMAL_INT})|({DECIMAL_INT})
FLOAT_LITERAL=(0x{HEXIDECIMAL_INT}\.{HEXIDECIMAL_INT}([pP][-+]?{DECIMAL_INT})?)
             |({DECIMAL_INT}\.{DECIMAL_INT}([eE][-+]?{DECIMAL_INT})?)
             |(0x{HEXIDECIMAL_INT}[pP][-+]?{DECIMAL_INT})
             |({DECIMAL_INT}[eE][-+]?{DECIMAL_INT})

CHARACTER_LITERAL=\'{CHARACTER_CHARACTER}\'

SINGLE_STRING_LITERAL=\"{STRING_CHARACTER}*\"
LINE_STRING=(\\\\[^\n]*[ \n]*)+

TOP_LEVEL_DOCUMENTATION_COMMENT=(\/\/\!             [^\n]* [ \n]*)+
DOCUMENTATION_COMMENT=          (\/\/\/\n?)
                               |(\/\/\/     [^/\n]  [^\n]* [ \n]*)+
LINE_COMMENT=                   (\/\/\n?)
                               |(\/\/       [^!/\n] [^\n]*)
                               |(\/\/\/\/           [^\n]*)
WHITESPACE=[\s\n\f\r\t]+

BUILTIN_IDENTIFIER=@[A-Za-z_][A-Za-z0-9_]*
IDENTIFIER=([A-Za-z_][A-Za-z0-9_]*)|(@{SINGLE_STRING_LITERAL})

ANY=[^]

%%

// symbols
&       { return ZigTypes.AMPERSAND_SYMBOL;                }
&=      { return ZigTypes.BITWISE_AND_ASSIGN_SYMBOL;       }
\*      { return ZigTypes.ASTERISK_SYMBOL;                 }
\*\*    { return ZigTypes.DOUBLE_ASTERISK_SYMBOL;          }
\*=     { return ZigTypes.MULTIPLY_ASSIGN_SYMBOL;          }
\*%     { return ZigTypes.MULTIPLY_OVERFLOW_SYMBOL;        }
\*%=    { return ZigTypes.MULTIPLY_OVERFLOW_ASSIGN_SYMBOL; }
\^      { return ZigTypes.CARET_SYMBOL;                    }
\^=     { return ZigTypes.EXPONENTIATE_ASSIGN_SYMBOL;      }
:       { return ZigTypes.COLON_SYMBOL;                    }
,       { return ZigTypes.COMMA_SYMBOL;                    }
\.      { return ZigTypes.DOT_SYMBOL;                      }
\.\.    { return ZigTypes.SLICE_SYMBOL;                    }
\.\.\.  { return ZigTypes.RANGE_SYMBOL;                    }
\.\*    { return ZigTypes.DEREFERENCE_POINTER_SYMBOL;      }
\.\?    { return ZigTypes.UNWRAP_OPTIONAL_SYMBOL;          }
=       { return ZigTypes.ASSIGN_SYMBOL;                   }
==      { return ZigTypes.EQUAL_SYMBOL;                    }
=>      { return ZigTypes.SWITCH_PRONG_SYMBOL;             }
\!      { return ZigTypes.EXCLAMATIONMARK_SYMBOL;          }
\!=     { return ZigTypes.NOT_EQUAL_SYMBOL;                }
\<      { return ZigTypes.LESS_THAN_SYMBOL;                }
\<\<    { return ZigTypes.LEFT_BIT_SHIFT_SYMBOL;           }
\<\<=   { return ZigTypes.LEFT_BIT_SHIFT_ASSIGN_SYMBOL;    }
\<=     { return ZigTypes.LESS_THAN_OR_EQUAL_SYMBOL;       }
\{      { return ZigTypes.LEFT_BRACE;                      }
\[      { return ZigTypes.LEFT_BRACKET;                    }
\(      { return ZigTypes.LEFT_PARENTHESIS;                }
-       { return ZigTypes.MINUS_SYMBOL;                    }
-=      { return ZigTypes.SUBTRACT_ASSIGN_SYMBOL;          }
-%      { return ZigTypes.MINUS_PERCENT_SYMBOL;            }
-%=     { return ZigTypes.SUBTRACT_OVERFLOW_ASSIGN_SYMBOL; }
->      { return ZigTypes.MINUS_RIGHT_ARROW;               }
%       { return ZigTypes.MODULUS_SYMBOL;                  }
%=      { return ZigTypes.MODULUS_ASSIGN_SYMBOL;           }
\|      { return ZigTypes.PIPE_SYMBOL;                     }
\|\|    { return ZigTypes.MERGE_ERROR_SET_SYMBOL;          }
\|=     { return ZigTypes.BITWISE_OR_ASSIGN_SYMBOL;        }
\+      { return ZigTypes.ADD_SYMBOL;                      }
\+\+    { return ZigTypes.CONCATENATE_ARRAY_SYMBOL;        }
\+=     { return ZigTypes.ADD_ASSIGN_SYMBOL;               }
\+%     { return ZigTypes.ADD_OVERFLOW_SYMBOL;             }
\+%=    { return ZigTypes.ADD_OVERFLOW_ASSIGN_SYMBOL;      }
\[\*c\] { return ZigTypes.C_POINTER_SYMBOL;                }
\?      { return ZigTypes.OPTIONAL_SYMBOL;                 }
>       { return ZigTypes.GREATER_THAN_SYMBOL;             }
>>      { return ZigTypes.RIGHT_BIT_SHIFT_SYMBOL;          }
>>=     { return ZigTypes.RIGHT_BIT_SHIFT_ASSIGN_SYMBOL;   }
>=      { return ZigTypes.GREATER_THAN_OR_EQUAL_SYMBOL;    }
\}      { return ZigTypes.RIGHT_BRACE;                     }
\]      { return ZigTypes.RIGHT_BRACKET;                   }
\)      { return ZigTypes.RIGHT_PARENTHESIS;               }
;       { return ZigTypes.SEMICOLON_SYMBOL;                }
\/      { return ZigTypes.DIVIDE_SYMBOL;                   }
\/=     { return ZigTypes.DIVIDE_ASSIGN_SYMBOL;            }
\~      { return ZigTypes.BITWISE_NOT_SYMBOL;              }

// keywords
align          { return ZigTypes.ALIGN_KEYWORD;          }
allowzero      { return ZigTypes.ALLOWZERO_KEYWORD;      }
and            { return ZigTypes.AND_KEYWORD;            }
anyframe       { return ZigTypes.ANYFRAME_KEYWORD;       }
anytype        { return ZigTypes.ANYTYPE_KEYWORD;        }
asm            { return ZigTypes.ASSEMBLY_KEYWORD;       }
async          { return ZigTypes.ASYNC_KEYWORD;          }
await          { return ZigTypes.AWAIT_KEYWORD;          }
break          { return ZigTypes.BREAK_KEYWORD;          }
callconv       { return ZigTypes.CALLCONV_KEYWORD;       }
catch          { return ZigTypes.CATCH_KEYWORD;          }
comptime       { return ZigTypes.COMPTIME_KEYWORD;       }
const          { return ZigTypes.CONST_KEYWORD;          }
continue       { return ZigTypes.CONTINUE_KEYWORD;       }
defer          { return ZigTypes.DEFER_KEYWORD;          }
else           { return ZigTypes.ELSE_KEYWORD;           }
enum           { return ZigTypes.ENUM_KEYWORD;           }
errdefer       { return ZigTypes.ERRDEFER_KEYWORD;       }
error          { return ZigTypes.ERROR_KEYWORD;          }
export         { return ZigTypes.EXPORT_KEYWORD;         }
extern         { return ZigTypes.EXTERN_KEYWORD;         }
false          { return ZigTypes.FALSE_KEYWORD;          }
fn             { return ZigTypes.FN_KEYWORD;             }
for            { return ZigTypes.FOR_KEYWORD;            }
if             { return ZigTypes.IF_KEYWORD;             }
inline         { return ZigTypes.INLINE_KEYWORD;         }
noalias        { return ZigTypes.NOALIAS_KEYWORD;        }
nosuspend      { return ZigTypes.NOSUSPEND_KEYWORD;      }
noinline       { return ZigTypes.NOINLINE_KEYWORD;       }
null           { return ZigTypes.NULL_KEYWORD;           }
opaque         { return ZigTypes.OPAQUE_KEYWORD;         }
or             { return ZigTypes.OR_KEYWORD;             }
orelse         { return ZigTypes.ORELSE_KEYWORD;         }
packed         { return ZigTypes.PACKED_KEYWORD;         }
pub            { return ZigTypes.PUB_KEYWORD;            }
resume         { return ZigTypes.RESUME_KEYWORD;         }
return         { return ZigTypes.RETURN_KEYWORD;         }
linksection    { return ZigTypes.LINKSECTION_KEYWORD;    }
struct         { return ZigTypes.STRUCT_KEYWORD;         }
suspend        { return ZigTypes.SUSPEND_KEYWORD;        }
switch         { return ZigTypes.SWITCH_KEYWORD;         }
test           { return ZigTypes.TEST_KEYWORD;           }
threadlocal    { return ZigTypes.THREADLOCAL_KEYWORD;    }
true           { return ZigTypes.TRUE_KEYWORD;           }
try            { return ZigTypes.TRY_KEYWORD;            }
undefined      { return ZigTypes.UNDEFINED_KEYWORD;      }
union          { return ZigTypes.UNION_KEYWORD;          }
unreachable    { return ZigTypes.UNREACHABLE_KEYWORD;    }
usingnamespace { return ZigTypes.USINGNAMESPACE_KEYWORD; }
var            { return ZigTypes.VAR_KEYWORD;            }
volatile       { return ZigTypes.VOLATILE_KEYWORD;       }
while          { return ZigTypes.WHILE_KEYWORD;          }

// macros: literals, comments, whitespace, identifiers, and invalid characters
{INTEGER_LITERAL}              { return ZigTypes.INTEGER_LITERAL;       }
{FLOAT_LITERAL}                { return ZigTypes.FLOAT_LITERAL;         }
{CHARACTER_LITERAL}            { return ZigTypes.CHARACTER_LITERAL;     }
{SINGLE_STRING_LITERAL}        { return ZigTypes.SINGLE_STRING_LITERAL; }
{LINE_STRING}                  { return ZigTypes.LINE_STRING_LITERAL;   }

{TOP_LEVEL_DOCUMENTATION_COMMENT} { return ZigTypes.TOP_LEVEL_DOCUMENTATION_COMMENT; }
{DOCUMENTATION_COMMENT}           { return ZigTypes.DOCUMENTATION_COMMENT;           }
{LINE_COMMENT}                    { return ZigTokenType.LINE_COMMENT;                }
{WHITESPACE}                      { return TokenType.WHITE_SPACE;                    }

{BUILTIN_IDENTIFIER} { return ZigTypes.BUILTIN_IDENTIFIER; }
{IDENTIFIER}         { return ZigTypes.IDENTIFIER;         }

// any other characters encountered at this point must be invalid
{ANY} { return TokenType.BAD_CHARACTER; }