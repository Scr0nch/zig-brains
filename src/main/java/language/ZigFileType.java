package language;

import com.intellij.openapi.fileTypes.LanguageFileType;
import editor.ZigIcons;
import javax.swing.Icon;
import org.jetbrains.annotations.*;

/**
 * A simple singleton {@code LanguageFileType} extension that registers the Zig file type with the editor.
 */
public class ZigFileType extends LanguageFileType {

    public static final ZigFileType INSTANCE = new ZigFileType();

    private ZigFileType() {
        super(ZigLanguage.INSTANCE);
    }

    @Override
    public @NotNull String getName() {
        return "Zig File";
    }

    @Override
    public @NotNull @Nls(capitalization = Nls.Capitalization.Sentence) String getDescription() {
        return "Zig language source file";
    }

    @Override
    public @NotNull String getDefaultExtension() {
        return "zig";
    }

    @Override
    public @Nullable Icon getIcon() {
        return ZigIcons.FILE_ICON;
    }

}
