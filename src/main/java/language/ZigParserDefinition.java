package language;

import com.intellij.lang.*;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.intellij.psi.tree.*;
import java.util.Set;
import language.parser.ZigParser;
import language.psi.*;
import org.jetbrains.annotations.NotNull;

/**
 * A {@code ParserDefinition} implementation that registers the Zig parser with the editor and defines paring-related
 * constants.
 */
public class ZigParserDefinition implements ParserDefinition {

    public static final Set<String> BUILTIN_FUNCTION_IDENTIFIERS = Set.of(
            "addWithOverflow", "alignCast", "alignOf", "as", "asyncCall", "atomicLoad", "atomicRmw",
            "atomicStore", "bitCast", "bitOffsetOf", "boolToInt", "bitSizeOf", "breakpoint", "mulAdd",
            "byteSwap", "bitReverse", "byteOffsetOf", "call", "cDefine", "cImport", "cInclude", "clz",
            "cmpxchgStrong", "cmpxchgWeak", "compileError", "compileLog", "ctz", "cUndef", "divExact",
            "divFloor", "divTrunc", "embedFile", "enumToInt", "errorName", "errorReturnTrace", "errorToInt",
            "errSetCast", "export", "fence", "field", "fieldParentPtr", "floatCast", "floatToInt", "frame",
            "Frame", "frameAddress", "frameSize", "hasDecl", "hasField", "import", "intCast", "intToEnum",
            "intToError", "intToFloat", "intToPtr", "memcpy", "memset", "wasmMemorySize", "wasmMemoryGrow",
            "mod", "mulWithOverflow", "panic", "popCount", "ptrCast", "ptrToInt", "rem", "returnAddress",
            "setAlignStack", "setCold", "setEvalBranchQuota", "setFloatMode", "setRuntimeSafety", "shlExact",
            "shlWithOverflow", "shrExact", "shuffle", "sizeOf", "splat", "src", "sqrt", "sin", "cos", "exp",
            "exp2", "log", "log2", "log10", "fabs", "floor", "ceil", "trunc", "round", "subWithOverflow",
            "tagName", "TagType", "This", "truncate", "Type", "typeInfo", "typeName", "TypeOf", "unionInit",
            "Vector", "extern", "reduce" // when last checked, these builtins were not included in the documentation for
            // the master branch
    );

    private static final IFileElementType FILE_ELEMENT_TYPE = new IFileElementType(ZigLanguage.INSTANCE);
    public static final TokenSet COMMENT_TOKEN_SET = TokenSet.create(ZigTokenType.LINE_COMMENT,
            ZigTypes.DOCUMENTATION_COMMENT, ZigTypes.TOP_LEVEL_DOCUMENTATION_COMMENT);
    private static final TokenSet STRING_LITERAL_TOKEN_SET = TokenSet.create(ZigTypes.SINGLE_STRING_LITERAL,
            ZigTypes.LINE_STRING_LITERAL);

    @Override
    public @NotNull Lexer createLexer(final Project project) {
        return new ZigLexerAdapter();
    }

    @Override
    public @NotNull PsiParser createParser(final Project project) {
        return new ZigParser();
    }

    @Override
    public @NotNull IFileElementType getFileNodeType() {
        return FILE_ELEMENT_TYPE;
    }

    @Override
    public @NotNull TokenSet getCommentTokens() {
        return COMMENT_TOKEN_SET;
    }

    @Override
    public @NotNull TokenSet getStringLiteralElements() {
        return STRING_LITERAL_TOKEN_SET;
    }

    @Override
    public @NotNull PsiElement createElement(final ASTNode node) {
        return ZigTypes.Factory.createElement(node);
    }

    @Override
    public @NotNull PsiFile createFile(final @NotNull FileViewProvider viewProvider) {
        return new ZigFile(viewProvider);
    }

}
